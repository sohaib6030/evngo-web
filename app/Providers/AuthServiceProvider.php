<?php

namespace App\Providers;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerDashboardPolicies();
        $this->registerAdminsPolicies();
        $this->registerAdminmenuPolicies();
        $this->group();
        $this->event();
        $this->customer();
        $this->eventType();
        $this->groupType();
        $this->subscriptionPrice();
        Passport::routes();

        //
    }
    
    
     //subscriptionPrice
    public function subscriptionPrice(){
        
        Gate::define('subscriptionPrice-index', function($user){
            return $user->hasAccess(['subscriptionPrice-index']);
        });

        Gate::define('subscriptionPrice-store', function($user){
            return $user->hasAccess(['subscriptionPrice-store']);
        });

        
    }
    
      //event
    public function event(){
        
        Gate::define('event-index', function($user){
            return $user->hasAccess(['event-index']);
        });

        Gate::define('event-fetch', function($user){
            return $user->hasAccess(['event-fetch']);
        });

        // Gate::define('event-store', function($user){
        //     return $user->hasAccess(['event-store']);
        // });

        Gate::define('event-edit', function($user){
            return $user->hasAccess(['event-edit']);
        });
        Gate::define('event-show', function($user){
            return $user->hasAccess(['event-show']);
        });

        Gate::define('event-active', function($user){
            return $user->hasAccess(['event-active']);
        });
        Gate::define('event-disable', function($user){
            return $user->hasAccess(['event-disable']);
        });
    }


     //group
    public function group(){
        
        Gate::define('group-index', function($user){
            return $user->hasAccess(['group-index']);
        });

        Gate::define('group-fetch', function($user){
            return $user->hasAccess(['group-fetch']);
        });

        // Gate::define('group-store', function($user){
        //     return $user->hasAccess(['group-store']);
        // });

        Gate::define('group-edit', function($user){
            return $user->hasAccess(['group-edit']);
        });
        Gate::define('group-show', function($user){
            return $user->hasAccess(['group-show']);
        });

        Gate::define('group-active', function($user){
            return $user->hasAccess(['group-active']);
        });
        Gate::define('group-disable', function($user){
            return $user->hasAccess(['group-disable']);
        });
    }

    //customer
    public function customer(){
        
        Gate::define('customer-index', function($user){
            return $user->hasAccess(['customer-index']);
        });

        Gate::define('customer-fetch', function($user){
            return $user->hasAccess(['customer-fetch']);
        });

        // Gate::define('customer-store', function($user){
        //     return $user->hasAccess(['customer-store']);
        // });

        Gate::define('customer-edit', function($user){
            return $user->hasAccess(['customer-edit']);
        });
        Gate::define('customer-show', function($user){
            return $user->hasAccess(['customer-show']);
        });

        Gate::define('customer-active', function($user){
            return $user->hasAccess(['customer-active']);
        });
        Gate::define('customer-disable', function($user){
            return $user->hasAccess(['customer-disable']);
        });
    }


    //eventType
    public function eventType(){
        
        Gate::define('eventType-index', function($user){
            return $user->hasAccess(['eventType-index']);
        });

        Gate::define('eventType-fetch', function($user){
            return $user->hasAccess(['eventType-fetch']);
        });

        Gate::define('eventType-store', function($user){
            return $user->hasAccess(['eventType-store']);
        });

        Gate::define('eventType-edit', function($user){
            return $user->hasAccess(['eventType-edit']);
        });
        Gate::define('eventType-show', function($user){
            return $user->hasAccess(['eventType-show']);
        });

        Gate::define('eventType-active', function($user){
            return $user->hasAccess(['eventType-active']);
        });
        Gate::define('eventType-disable', function($user){
            return $user->hasAccess(['eventType-disable']);
        });
    }


    //groupType
    public function groupType(){
        
        Gate::define('groupType-index', function($user){
            return $user->hasAccess(['groupType-index']);
        });

        Gate::define('groupType-fetch', function($user){
            return $user->hasAccess(['groupType-fetch']);
        });

        Gate::define('groupType-store', function($user){
            return $user->hasAccess(['groupType-store']);
        });

        Gate::define('groupType-edit', function($user){
            return $user->hasAccess(['groupType-edit']);
        });
        Gate::define('groupType-show', function($user){
            return $user->hasAccess(['groupType-show']);
        });

        Gate::define('groupType-active', function($user){
            return $user->hasAccess(['groupType-active']);
        });
        Gate::define('groupType-disable', function($user){
            return $user->hasAccess(['groupType-disable']);
        });
    }




    public function registerAdminmenuPolicies(){
    
        Gate::define('menu-index', function($user){
            return $user->hasAccess(['menu-index']);
        });

    }


    
    //Dashboard
    public function registerDashboardPolicies(){
    
       /* Gate::define('stats-number', function($user){
            return $user->hasAccess(['stats-number']);
        });

       */

    }
    

    //Sub Admins
    public function registerAdminsPolicies(){
        Gate::define('admins-index', function($user){
            return $user->hasAccess(['admins-index']);
        });

        Gate::define('create-staff', function($user){
            return $user->hasAccess(['create-staff']);
        });

        Gate::define('edit-staff', function($user){
            return $user->hasAccess(['edit-staff']);
        });

        Gate::define('status-staff', function($user){
            return $user->hasAccess(['status-staff']);
        });

        Gate::define('show-staff', function($user){
            return $user->hasAccess(['show-staff']);
        });
        
        Gate::define('delete-staff', function($user){
            return $user->hasAccess(['delete-staff']);
        });


        Gate::define('staff-reset-password', function($user){
            return $user->hasAccess(['staff-reset-password']);
        });
    }

}
