<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class EventService extends Model
{
    protected $table = 'event_services';

    

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }

   

    
}

