<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;
use App\User;
class GroupMember extends Model
{

  protected $table = "group_members";
  
    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }
    
    public function requestedBy()
    {
        return $this->hasOne(User::class,'id','requested_by')->withDefault();
    }
    
    public function member()
    {
        return $this->hasOne(User::class,'id','user_id')->withDefault();
    }
    
    public function group()
    {
        return $this->hasOne(Group::class, 'id', 'group_id');
    }

}