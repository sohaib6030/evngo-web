<?php
/**
 * Created by Touqeer Hanif.
 * User: gc
 * Date: 04/21/2020
 * Time: 12:21 PM
 */

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use PayPal\Api\Plan;

class PlansDetail extends Model
{

    protected $table = "plan_details";

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }

    public function detail()
    {
        return $this->hasOne(Plans::class,'id','plan_id')->withDefault();
    }

}