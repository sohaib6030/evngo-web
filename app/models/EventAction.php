<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class EventAction extends Model
{
    protected $table = 'event_action';

    

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id')->withDefault();
    }

   

    
}

