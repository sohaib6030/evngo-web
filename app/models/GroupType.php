<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;
use App\User;
class GroupType extends Model
{

    protected $table = "group_types";
  
    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }
    
   
}