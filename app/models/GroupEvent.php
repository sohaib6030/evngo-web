<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;
use App\User;
class GroupEvent extends Model
{

    protected $table = "group_events";
  
    public function user()
    {
        return $this->hasOne(User::class,'id','shared_by')->withDefault();
    }

}