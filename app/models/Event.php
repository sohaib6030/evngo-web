<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Event extends Model
{
    protected $table = 'events';

    public function joinUsers()
    {
        return $this->hasMany(EventJoin::class, 'event_id', 'id')->where(['status'=>'Active','is_deleted'=>'0']);
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }
    
    public function serviceProvider()
    {
        return $this->hasOne(User::class,'id','service_provider_id')->withDefault();
    }
    
    public function eventType()
    {
        return $this->hasOne(EventType::class,'id','event_type_id')->withDefault();
    }
    
    public function eventLike()
    {
        return $this->hasMany(EventAction::class,'event_id','id')->where(['type'=>'Like']);
    }
    
    public function share()
    {
        return $this->hasMany(EventShare::class, 'event_id', 'id');
    }

    public function eventPaymentType()
    {
        return $this->hasOne(EventPaymentType::class,'id','event_payment_id')->withDefault();
    }

   
    public function joinEvent()
    {
        return $this->hasMany(EventJoin::class,'event_id','id')->where('status','Active')->limit('4');
    }
    
    
}
