<?php
/**
 * Created by sohaib ahmed.
 * User: gc
 * Date: 1/14/2020
 * Time: 6:55 PM
 */

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;
class EventTicket extends Model
{

    protected $table = "event_tickets";

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }

    public function eventTicketType()
    {
        return $this->hasOne(EventTicketType::class,'id','event_ticket_type_id')->withDefault();
    }


    public function eventTicketSold()
    {
        return $this->hasOne(EventTicketSold::class,'event_join_id','id')->withDefault();
    }

    public function eventTicketJoin()
    {
        return $this->hasOne(EventJoin::class,'event_id','event_id')->withDefault();
    }




}