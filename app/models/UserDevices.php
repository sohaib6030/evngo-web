<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 2/14/2020
 * Time: 8:13 PM
 */

namespace App\models;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserDevices extends Model
{
    protected $table = "userdevices";

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }
}