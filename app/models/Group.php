<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;
use App\User;
class Group extends Model
{

    protected $table = "groups";
  
    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }
    
    public function members()
    {
        return $this->hasMany(GroupMember::class, 'group_id', 'id')->where('status','Approved')->where('is_deleted',0);
    }

    public function joinmembers()
    {
        return $this->hasMany(GroupMember::class, 'group_id', 'id');
    }
    
    public function group_type()
    {
        return $this->hasOne(GroupType::class,'id','group_type_id')->withDefault();
    }
    
    public function group_new()
    {
        return $this->belongsTo(GroupMember::class, 'group_id');
    }

}