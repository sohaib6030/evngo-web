<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;


class UserType extends Model
{
    protected $table = 'user_types';
    
    public function user()
    {
        return $this->hasOne(User::class,'id','user_id')->withDefault();
    }
}
