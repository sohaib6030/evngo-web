<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class EventJoin extends Model
{
    protected $table = 'event_join';

    

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id')->withDefault();
    }
    
    
    public function createdBy()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }
    
    public function event()
    {
        return $this->hasOne(Event::class,'id','event_id')->withDefault();
    }

    public function eventticket()
    {
        return $this->hasOne(EventTicket::class,'event_id','event_id')->withDefault();
    }
}
