<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class EventType extends Model
{

    protected $table = "event_types";

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }


}