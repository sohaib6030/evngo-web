<?php
/**
 * Created by Touqeer Hanif.
 * User: gc
 * Date: 04/21/2020
 * Time: 12:21 PM
 */

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Cities extends Model
{

    protected $table = "cities";

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }


}