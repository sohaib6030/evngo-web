<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Service extends Model
{
    protected $table = 'services';

    

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }

   

    
}

