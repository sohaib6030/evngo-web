<?php
/**
 * Created by PhpStorm.
 * User: me
 * Date: 2/14/2020
 * Time: 8:13 PM
 */

namespace App\models;
use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;

class Notifications extends Model
{
    protected $table = "notification_log";

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }

}