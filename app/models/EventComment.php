<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class EventComment extends Model
{
    protected $table = 'event_comments';

    

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id')->withDefault();
    }
    
    
    public function created_by()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }

   

    
}

