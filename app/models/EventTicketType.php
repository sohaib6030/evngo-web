<?php
/**
 * Created by sohaib ahmed.
 * User: gc
 * Date: 1/14/2020
 * Time: 6:55 PM
 */

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class EventTicketType extends Model
{

    protected $table = "event_ticket_types";

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }


}