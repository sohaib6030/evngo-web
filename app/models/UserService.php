<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserService extends Model
{
    protected $table = 'user_services';

    

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }

   

    
}

