<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;
use App\User;
class EventShare extends Model
{

    protected $table = "event_shares";
  
    public function user()
    {
        return $this->hasOne(User::class,'id','created_by')->withDefault();
    }


    
    public function sharedWith()
    {
        return $this->hasOne(User::class,'id','user_id')->withDefault();
    }
    
    public function event()
    {
        return $this->hasOne(User::class,'id','event_id')->withDefault();
    }
    
    public function allevent()
    {
        return $this->hasOne(Event::class,'id','event_id')->where(['status'=>'Active','is_deleted'=>'0'])->withDefault();
    }

    public function getallevent()
    {
        return $this->hasOne(Event::class,'id','event_id')->where(['status'=>'Active','is_deleted'=>'0'])->withDefault();
        //orderBy('event_payment_id','desc')->
        //orderBy('event_date','asc')->orderBy('event_time','asc')->

    }

    public function tickets()
    {
        return $this->hasMany(EventTicket::class, 'event_id', 'event_id')->where(['status'=>'Active','is_deleted'=>'0']);
    }
    
    public function group()
    {
        return $this->hasOne(Group::class,'id','group_id')->withDefault();
    }
    
    public function eventLike()
    {
        return $this->hasMany(EventAction::class,'event_share_id','id')->where(['type'=>'Like']);
    }
    
    public function comment()
    {
        return $this->hasMany(EventComment::class,'event_share_id','id');
    }
    
    
   
}