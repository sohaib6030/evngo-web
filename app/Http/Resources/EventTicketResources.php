<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
class EventTicketResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

       /* return [

            'event_ticket_total'=> $this->event_ticket_total,
            'event_ticket_sold'=> $this->event_ticket_sold,
            'event_ticket_type_id'=> $this->event_ticket_type_id,
            'is_unlimited'=> $this->is_unlimited,
        ];  */
         return parent::toArray($request);
    }
}

