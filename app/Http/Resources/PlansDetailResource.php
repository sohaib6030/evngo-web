<?php
/**
 * Created by Touqeer Hanif.
 * User: gc
 * Date: 04/21/2020
 * Time: 12:27 PM
 */

namespace App\Http\Resources;
use App\models\UserType;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
class PlansDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $currentplandetails =  UserType::where('user_id',$this->user_id)->where('status','Active')->first();
        $expiry = "None";

        if (($currentplandetails) && ($this->status == "Active"))
        {
            $expiry = $currentplandetails->end_date;

        }

        return [
            'id'=> $this->id,

            'user_id'=> $this->user_id,
            'licences'=> $this->licences,
            'capacity'=> $this->capacity,
            'services'=> $this->services,
            'promotions'=> $this->promotions,
            'plan_id'=> $this->plan_id,
            'status'=> $this->status,

            'type'=> $this->detail->type,
            'tenure'=> $this->detail->tenure,
            'price'=> $this->detail->price,
            'Discount'=> $this->detail->Discount,
            'Saving'=> $this->detail->Saving,
            'MonthlyCost'=> $this->detail->MonthlyCost,

            'name'=>  $this->user->fname . ' ' . $this->user->lname,
            'email'=>  $this->user->email,
            'mobile'=>  $this->user->mobile,
            'address'=>  $this->user->address,

            'current_plan_expiry' => $expiry


        ];
        // return parent::toArray($request);
    }
}

