<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use App\models\UserType;
class FrontEventJoinResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        if($this->avatar=='placeholder.png'){
            $Avatarurl=URL::to('/').Storage::disk('local')->url('public/users/'.$this->user->avatar);
        }else{
            $Avatarurl=URL::to('/').Storage::disk('local')->url('public/users/'.$this->user->id.'/'.$this->user->avatar);
        }
       
       
        return [
            'join_id'       => $this->id,
            'event_id'      => $this->event_id,
            'id'            => $this->user->id,
            'Avatarurl'     => $Avatarurl,
            'city'          => $this->user->city ?? '',
            'country'       => $this->user->country ?? '',
            'dob'           => $this->user->dob ?? '',
            'fname'         => $this->user->fname ?? '',
            'lname'         => $this->user->lname ?? '',
            'gender'        => $this->user->gender ?? '',
            'email'         => $this->user->email ?? '',

        ];
    }
}
