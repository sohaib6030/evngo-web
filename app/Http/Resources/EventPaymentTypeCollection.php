<?php
/**
 * Created by PhpStorm.
 * User: gc
 * Date: 1/14/2020
 * Time: 7:04 PM
 */

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\models\EventPaymentType;

class EventPaymentTypeCollection extends  ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (EventPaymentType $EventPaymentType) {
            return (new EventPaymentTypeResources($EventPaymentType));
        });
        return parent::toArray($request);
    }
    public function with($request)
    {
        return [
            'success' => 1,
            'message' => 'Records found.'
        ];
    }
}