<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use App\models\UserType;
use App\models\EventAttendance;
class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        return [

            'id'           => $this->id,
            'type_id'      => $this->type_id,
            'user_id'     => $this->user_id,
            'event_id'     => $this->event_id,
            'title'        => $this->title,
            'description'    => $this->description,
            'user'         =>  new UserResource($this->user)

        ];
    }
}
