<?php
/**
 * Created by Touqeer Hanif.
 * User: gc
 * Date: 04/21/2020
 * Time: 12:27 PM
 */

namespace App\Http\Resources;
use App\models\PlansDetail;
use Illuminate\Http\Resources\Json\ResourceCollection;


class PlansDetailsCollection extends  ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (PlansDetail $Plans) {
            return (new PlansDetailResource($Plans));
        });
        return parent::toArray($request);
    }
    public function with($request)
    {
        return [
            'success' => 1,
            'message' => 'Records found.'
        ];
    }
}