<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
use Auth;
use App\models\EventAction;
use App\models\EventJoin;
use App\models\EventShare;
use App\models\EventTicket;
class EventVenueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'venue'=> $this->venue,
        ];
    }
}
