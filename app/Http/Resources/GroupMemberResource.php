<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
use App\models\UserType;
class GroupMemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        if($this->member->avatar=='placeholder.png'){
            $Avatarurl=URL::to('/').Storage::disk('local')->url('public/users/'.$this->member->avatar);
        }else{
            $Avatarurl=URL::to('/').Storage::disk('local')->url('public/users/'.$this->member->id.'/'.$this->member->avatar);
        }
        
        $vip = UserType::where('type','VIP')->where('user_id',$this->member->id)->first();
        if($vip){
            
           $VIP = 'Yes'; 
        }else{
           $VIP = 'No';
        }
        $promoter = UserType::where('type','Promoter')->where('user_id',$this->member->id)->first();
        if($promoter){
            
           $Promoter = 'Yes'; 
        }else{
           $Promoter = 'No';
        }
        $serviceProvider = UserType::where('type','ServiceProvider')->where('user_id',$this->member->id)->first();
        if($serviceProvider){
            
           $ServiceProvider = 'Yes'; 
        }else{
           $ServiceProvider = 'No';
        }
        
        return [
            'member_id'            => $this->id,
            'id'            => $this->member->id,
            'Avatarurl'     => $Avatarurl,
            'city'          => $this->member->city ?? '',
            'country'       => $this->member->country ?? '',
            'dob'           => $this->member->dob ?? '',
            'fname'         => $this->member->fname ?? '',
            'lname'         => $this->member->lname ?? '',
            'gender'        => $this->member->gender ?? '',
            'email'         => $this->member->email ?? '',
            'role_in_group'         => $this->role_in_group ?? '',
            'VIP'           => $VIP,
            'Promoter'      => $Promoter,
            'ServiceProvider'=> $ServiceProvider,
        ];
    }
}
