<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
use Auth;
use App\models\EventAction;
use App\models\EventJoin;
use App\models\EventTicket;
class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        
        $image = URL::to('/').Storage::disk('local')->url('public/event/'.$this->allevent->image);
        
       $totalLike = count($this->eventLike);
       $totalComment = count($this->comment);
      //  $totalLike = 0;
       // $totalComment = 0;
        if(isset($this->group_id) && $this->group_id!=""){
            
            if($this->group->image){
            $groupimage = URL::to('/').Storage::disk('local')->url('public/groups/'.$this->group->image);
            }else{
                $groupimage ='';// URL::to('/').Storage::disk('local')->url('public/placeholder.png');;
            }
            
            $group = [
                        'id'=> $this->group->id,
                        'name'=> $this->group->name,
                        'group_type_id'=> $this->group->group_type->title,
                        'description'=> $this->group->description,
                        'image'=> $groupimage,
                     ];
        }else{
            $group = null;
        }
        
        
        
        if(isset($this->user_id) && $this->user_id!=""){
            
            if($this->sharedWith->avatar=='placeholder.png'){
            $Avatarurl=URL::to('/').Storage::disk('local')->url('public/users/'.$this->sharedWith->avatar);
            }else{
                $Avatarurl=URL::to('/').Storage::disk('local')->url('public/users/'.$this->sharedWith->id.'/'.$this->sharedWith->avatar);
            }
            
            $sharedWith = [
                            'id'            => $this->sharedWith->id,
                            'Avatarurl'     => $Avatarurl,
                            'city'          => $this->sharedWith->city ?? '',
                            'country'       => $this->sharedWith->country ?? '',
                            'dob'           => $this->sharedWith->dob ?? '',
                            'fname'         => $this->sharedWith->fname ?? '',
                            'lname'         => $this->sharedWith->lname ?? '',
                            'gender'        => $this->sharedWith->gender ?? '',
                            'email'         => $this->sharedWith->email ?? '',
                         ];
        }else{
            $sharedWith = null;
        }
        $auth_user_id = Auth::user()->id;
        $likeData =  EventAction::where('user_id',$auth_user_id)->where('event_share_id',$this->id)->first();
        if($likeData){
            if($likeData->type=='Like'){
                $like = 'Yes';
            }else{
                $like = 'No';
            }
        }else{
                $like = 'No';
            }
            
            
        $EventJoin =  EventJoin::where('user_id',$auth_user_id)->where('event_id',$this->allevent->id)->first();
        if($EventJoin){
            
                $join = 'Yes';
            
        }else{
                $join = 'No';
            }


            // added by sohaib ahmed on 16  jan 2020
            $ticketdata = EventTicket::where('event_tickets.status','Active')
        ->where('event_tickets.is_deleted','0')
        ->where('event_tickets.event_id',$this->allevent->id)
        ->leftJoin('event_ticket_types', 'event_tickets.event_ticket_type_id', '=', 'event_ticket_types.id')
        ->select('event_tickets.*','event_ticket_types.title')
        ->orderBy('event_ticket_type_id', 'asc')->get();

            // $data = EventPaymentType::where('status','Active')->where('is_deleted','0')->orderBy('id','asc')->get();

        return [
            'id'=> $this->allevent->id,
            'event_id'=> $this->id,
            'event_show'=> $this->allevent->event_show,
            'event_type_id'=> $this->allevent->eventType->title,
            'service_provider_id'=> $this->allevent->serviceProvider->fname.' '.$this->allevent->serviceProvider->lname,
            'title'=> $this->allevent->title,
            'description'=> $this->allevent->description,
            'venue_address'=> $this->allevent->venue_address,
            'image' => $image,
            'event_date'=> $this->allevent->event_date,
            'event_payment_id'=> $this->allevent->event_payment_id,
            'event_payment_type_name'=> $this->allevent->eventPaymentType->title,
            'event_time'=> $this->allevent->event_time,
            'created_at'=> $this->created_at->diffForHumans(),
            'created_by'=> new UserResource($this->allevent->user),
            'tickets'=> $ticketdata,
            'shared_with'=> $sharedWith,
            'group'=> $group,
            'totalLike'=> $totalLike,
            'totalComment'=> $totalComment,
            'like'=> $like,
            'eventJoin'=> $join,

        ];

    }
}
