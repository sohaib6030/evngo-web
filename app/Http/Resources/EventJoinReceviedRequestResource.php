<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
class EventJoinReceviedRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        if($this->qr_code){
            $qrcode = asset($this->qr_code);
        }else{
            $qrcode='';
        }
        $message = $this->user->fname.' '.$this->user->lname.' request to join event ' . $this->event->title;
        return [
            'message'=>     $message,
            'id'           => $this->id,
            'event_id'     => $this->event_id,
            'attanded'     => $this->attanded,
            'created_at'   => $this->created_at->diffForHumans(),
            'qrcode'       => $qrcode,
            'user'         => new UserResource($this->user),
            
        ];
       // return parent::toArray($request);
    }
}
