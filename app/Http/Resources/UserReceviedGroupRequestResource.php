<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
use App\models\GroupMember;
class UserReceviedGroupRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        if(isset($this->group->image) && ($this->group->image!="")){
            $image = URL::to('/').Storage::disk('local')->url('public/groups/'.$this->group->image);
        }else{
            $image ='';// URL::to('/').Storage::disk('local')->url('public/placeholder.png');;
        }

        //$TotalRecords = GroupMember::where('status','Pending')->where('requested_by',$this->requested_by)->where('role_in_group','Member')->where('is_deleted','0')->orderBy('id', 'desc')->count();
        
        if($this->member->avatar=='placeholder.png'){
            $Avatarurl=URL::to('/').$this->member->avatar;
        }else{
            $Avatarurl=URL::to('/').$this->member->avatar;
        }
        
        if($this->type=='RequestGroup'){
            $message = $this->user->fname.' '.$this->user->lname.' requested you to  join  ' . $this->group->name ?? '';
            $fname = $this->user->fname ?? '';
            $lname = $this->user->lname ?? '';
            $city =  $this->user->city ?? '';
            $country =  $this->user->country ?? '';
            $dob =  $this->user->dob ?? '';
            $gender =  $this->user->gender ?? '';
            
        }else if($this->type=='RequestUser'){
            $message = $this->member->fname.' '.$this->member->lname.' want to join  ' . $this->group->name ?? '';
            $fname = $this->member->fname ?? '';
            $lname = $this->member->lname ?? '';
            $city =  $this->member->city ?? '';
            $country =  $this->member->country ?? '';
            $dob =  $this->member->dob ?? '';
            $gender =  $this->member->gender ?? '';
        }
        
        

        return [
            'message'=> $message,
            'member_id'=> $this->id,
            'type'=> $this->type,
            'created_at'=> $this->created_at->diffForHumans(),
            'Group'=>[
                'id'=> $this->group->id ?? '',
                'name'=> $this->group->name ?? '',
                'group_type_id'=> $this->group->group_type->title ?? '',
                'description'=> $this->group->description ?? '',
                'created_by'=> $this->group->user->fname ?? ''.' '.$this->group->user->lname ?? '',
                'image' => $image,
                ],
            'Userdetail'=>[
            'Avatarurl'     => $Avatarurl,
            'city'          => $city,
            'country'       => $country,
            'dob'           => $dob,
            'fname'         => $fname,
            'lname'         => $lname,
            'gender'        => $gender ?? '',
            ]    
            
            
        ];
       // return parent::toArray($request);
    }
}
