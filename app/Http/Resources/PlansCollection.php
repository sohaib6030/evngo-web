<?php
/**
 * Created by Touqeer Hanif.
 * User: gc
 * Date: 04/21/2020
 * Time: 12:27 PM
 */

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\models\Plans;

class PlansCollection extends  ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Plans $Plans) {
            return (new PlansResource($Plans));
        });
        return parent::toArray($request);
    }
    public function with($request)
    {
        return [
            'success' => 1,
            'message' => 'Records found.'
        ];
    }
}