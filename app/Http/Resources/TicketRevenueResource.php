<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
use Auth;
use App\models\EventAction;
use App\models\EventJoin;
use App\models\EventShare;
use App\models\EventTicket;
class TicketRevenueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {




        $image = URL::to('/').Storage::disk('local')->url('public/event/'. ($this->allevent->image ?? 'placeholder.png'));


/*
        $totalLike = count($this->eventLike);
        $totalComment = count($this->comment);

        $auth_user_id = Auth::user()->id;
        $likeData =  EventAction::where('user_id',$auth_user_id)->where('event_share_id',$this->id)->first();
        if($likeData){
            if($likeData->type=='Like'){
                $like = 'Yes';
            }else{
                $like = 'No';
            }
        }else{
            $like = 'No';
        }


        $totalJoinUser =  EventJoin::where('event_id',$this->allevent->id)->where('status','Active')->count();

        $EventJoin     =  EventJoin::where('user_id',$auth_user_id)->where('event_id',$this->allevent->id)->first();

        if($EventJoin){
            if($EventJoin->status=='Pending'){
                $join = 'Pending';
            }
            elseif($EventJoin->status=='Rejected'){
                $join = 'Rejected';
            }else{
                $join = 'Yes';
            }
        }else{
            $join = 'No';
        }

        // $joins = $this->allevent->joinEvent->toArray();
        //dd($join);
        // $array = array_slice($joins, 0, 4);
        $attandedEvent     =  EventJoin::where('event_id',$this->allevent->id)->where('attanded','Present')->count();
        $totalShareEvent  = EventShare::where('event_id',$this->allevent->id)->where('is_deleted','0')->where('type','User')->orderBy('id', 'desc')->count();
*/





        $TicketCount = $this->event_ticket_sold;
        $TicketList_ = [];
        $qrcode = URL::to('/').'/'.($this->eventJoin->qr_code ?? 'placeholder.png');
        $qrcodemd5 = $this->eventJoin->qr_code_md5;
        $eventticketref = $this->event_ticket_ref;
        for ($i=0; $i<$TicketCount; $i++)
        {
            $eventid = $this->eventJoin->event_id;
            $joinid = $this->eventJoin->id;
            $id = $this->eventTicketType->id;

            $ticketid = $eventid.$joinid.$id.$i+1;

            $arra = array (
                'TicketID'=> $ticketid,
                'qrcode'=> $qrcode,
                'qrcodemd5'=> $qrcodemd5,
                'event_ticket_ref'=>$eventticketref
            );
            //$TicketList_.array_push($arra);
            array_push($TicketList_, $arra);
        }





/*
        // added by sohaib ahmed on 16  jan 2020
        $ticketdata = EventTicket::where('event_tickets.status','Active')
            ->where('event_tickets.is_deleted','0')
            ->where('event_tickets.event_id',$this->allevent->id)
            ->leftJoin('event_ticket_types', 'event_tickets.event_ticket_type_id', '=', 'event_ticket_types.id')
            ->select('event_tickets.*','event_ticket_types.title')
            ->orderBy('event_ticket_type_id', 'asc')->get();
*/

        return [
            'event_id'=> $this->eventJoin->event_id,
            'event_name'=> $this->eventJoin->event->title,
            'image' => $image,
            'dated'=>date('yy-m-d H:i:s', strtotime($this->eventJoin->eventticket->created_at)),
            'tickettypeid'=>$this->eventTicketType->id,
            'tickettypename'=>$this->eventTicketType->title,
            'totalqty'=>$this->eventJoin->eventticket->event_ticket_total,
            'totalsold'=>$this->eventJoin->eventticket->event_ticket_sold,
            'price'=>$this->event_ticket_price,
            'nooftickets'=>$this->event_ticket_sold,
            'created_by'=> new UserResource($this->user),
            'Tickets'=>$TicketList_,
        ];


/*
        return [
            'id'=> $this->allevent->id,
            'event_id'=> $this->id,
            'event_show'=> $this->allevent->event_show,
            'event_type_id'=> $this->allevent->eventType->title,
            'service_provider_id'=> $this->allevent->serviceProvider->fname.' '.$this->allevent->serviceProvider->lname,
            'title'=> $this->allevent->title,
            'description'=> $this->allevent->description,
            'venue'=> $this->allevent->venue,
            'venue_address'=> $this->allevent->venue_address,
            'image' => $image,
            'event_date'=> $this->allevent->event_date,
            'event_payment_id'=> $this->allevent->event_payment_id,
            'event_payment_type_name'=> $this->allevent->eventPaymentType->title,
            'event_time'=> $this->allevent->event_time,
            'created_at'=> $this->created_at->diffForHumans(),
            'created_by'=> new UserResource($this->allevent->user),
            'totalLike'=> $totalLike,
            'totalComment'=> $totalComment,
            'like'=> $like,
            'eventJoin'=> $join,
            'totalJoinUser'=> $totalJoinUser,
            'totalAttandEvent'=> $attandedEvent,
            'totalShareEvent'=> $totalShareEvent,
            'joinEventUser'=> FrontEventJoinResource::collection($this->allevent->joinEvent),
            'tickets'=> $ticketdata,
            'group'=>  $this->group->name,
            'attendence'=>  $this->allevent->attendence,


        ];
*/
        // return parent::toArray($request);
    }
}
