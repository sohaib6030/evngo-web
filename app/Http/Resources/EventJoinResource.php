<?php
namespace App\Http\Resources;
use App\models\EventTicketSold;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use App\models\UserType;
use App\models\EventAttendance;
class EventJoinResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $type = 'Free Event';

        if($this->qr_code){
            $qrcode = asset($this->qr_code);
        }else{
            $qrcode='';
        }
        $totalCheckIn = EventAttendance::where('event_id',$this->event_id)->where('user_id',$this->user_id)->where('type','IN')->count();
        $totalCheckOut = EventAttendance::where('event_id',$this->event_id)->where('user_id',$this->user_id)->where('type','OUT')->count();

        $ticketdata = EventTicketSold::where('event_join_id',$this->id)
            ->leftJoin('event_ticket_types', 'event_tickets_sold.event_ticket_type_id', '=', 'event_ticket_types.id')
            ->select('event_ticket_types.title')
            ->orderBy('event_ticket_type_id', 'asc')->first();

        if ($ticketdata != null)
        {
            $type = $ticketdata->title;
        }

        return [
            'id'            => $this->id,
            'event_id'     => $this->event_id,
            'attanded'     => $this->attanded,
            'event'          => $this->event->title ?? '',
            'user'          => new UserResource($this->user),
            'qrcode'         => $qrcode,
            'totalCheckIn'         => $totalCheckIn,
            'totalCheckOut'         => $totalCheckOut,
            'ticketType'  =>   $type
            
        ];
    }
}
