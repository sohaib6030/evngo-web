<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
use Auth;
use App\models\EventAction;
use App\models\EventJoin;
use App\models\EventShare;
use App\models\EventTicket;
class FrontEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {




        $image = URL::to('/').Storage::disk('local')->url('public/event/'. ($this->allevent->image ?? 'placeholder.png'));



        $totalLike = count($this->eventLike);
        $totalComment = count($this->comment);

        $auth_user_id = Auth::user()->id ?? 0;
        $likeData =  EventAction::where('user_id',$auth_user_id)->where('event_share_id',$this->id)->first();
        if($likeData){
            if($likeData->type=='Like'){
                $like = 'Yes';
            }else{
                $like = 'No';
            }
        }else{
            $like = 'No';
        }


        $totalJoinUser =  EventJoin::where('event_id',$this->allevent->id)->where('status','Active')->count();

        $EventJoin     =  EventJoin::where('user_id',$auth_user_id)->where('event_id',$this->allevent->id)->first();

        if($EventJoin){
            if($EventJoin->status=='Pending'){
                $join = 'Pending';
            }
            elseif($EventJoin->status=='Rejected'){
                $join = 'Rejected';
            }else{
                $join = 'Yes';
            }
        }else{
            $join = 'No';
        }

        // $joins = $this->allevent->joinEvent->toArray();
        //dd($join);
        // $array = array_slice($joins, 0, 4);
        $attandedEvent     =  EventJoin::where('event_id',$this->allevent->id)->where('attanded','Present')->count();
        $totalShareEvent  = EventShare::where('event_id',$this->allevent->id)->where('is_deleted','0')->where('type','User')->orderBy('id', 'desc')->count();







        // added by sohaib ahmed on 16  jan 2020
        $ticketdata = EventTicket::where('event_tickets.status','Active')
            ->where('event_tickets.is_deleted','0')
            ->where('event_tickets.event_id',$this->allevent->id)
            ->leftJoin('event_ticket_types', 'event_tickets.event_ticket_type_id', '=', 'event_ticket_types.id')
            ->select('event_tickets.*','event_ticket_types.title')
            ->orderBy('event_ticket_type_id', 'asc')->get();



        return [
            'id'=> $this->allevent->id,
            'event_id'=> $this->id,
            'event_show'=> $this->allevent->event_show,
            'event_type_id'=> $this->allevent->eventType->title,
            'service_provider_id'=> $this->allevent->serviceProvider->fname.' '.$this->allevent->serviceProvider->lname,
            'title'=> $this->allevent->title,
            'description'=> $this->allevent->description,
            'venue'=> $this->allevent->venue,
            'venue_address'=> $this->allevent->venue_address,
            'image' => $image,
            'event_date'=> $this->allevent->event_date,
            'event_payment_id'=> $this->allevent->event_payment_id,
            'event_payment_type_name'=> $this->allevent->eventPaymentType->title,
            'event_time'=> $this->allevent->event_time,
            'created_at'=> $this->created_at->diffForHumans(),
            'created_by'=> new UserResource($this->allevent->user),
            'totalLike'=> $totalLike,
            'totalComment'=> $totalComment,
            'like'=> $like,
            'eventJoin'=> $join,
            'totalJoinUser'=> $totalJoinUser,
            'totalAttandEvent'=> $attandedEvent,
            'totalShareEvent'=> $totalShareEvent,
            'joinEventUser'=> FrontEventJoinResource::collection($this->allevent->joinEvent),
            'tickets'=> $ticketdata,
            'group'=>  $this->group->name,
            'attendence'=>  $this->allevent->attendence,
            'country'=>  $this->allevent->Country,
            'city'=>  $this->allevent->City,
            'shared_with'=>  $this->sharedWith->fname . ' ' . $this->sharedWith->lname,
        ];
        // return parent::toArray($request);
    }
}
