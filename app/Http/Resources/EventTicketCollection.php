<?php
/**
 * Created by PhpStorm.
 * User: gc
 * Date: 1/14/2020
 * Time: 7:04 PM
 */

namespace App\Http\Resources;
use App\models\EventTicket;
use Illuminate\Http\Resources\Json\ResourceCollection;


class EventTicketCollection extends  ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (EventTicket $EventTicket) {
            return (new EventTicketResources($EventTicket));
        });
        return parent::toArray($request);
    }
    public function with($request)
    {
        return [
            'success' => 1,
            'message' => 'Records found.'
        ];
    }
}