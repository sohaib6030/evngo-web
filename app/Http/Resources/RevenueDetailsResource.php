<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use App\models\UserType;
class RevenueDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->avatar=='placeholder.png'){
            $Avatarurl=URL::to('/').Storage::disk('local')->url('public/users/'.$this->avatar);
        }else{
            $Avatarurl=URL::to('/').Storage::disk('local')->url('public/users/'.$this->id.'/'.$this->avatar);
        }
        
        $vip = UserType::where('type','VIP')->where('user_id',$this->id)->first();
        if($vip){
            
           $VIP = 'Yes'; 
        }else{
           $VIP = 'No';
        }
        $promoter = UserType::where('type','Promoter')->where('user_id',$this->id)->first();
        if($promoter){
            
           $Promoter = 'Yes'; 
        }else{
           $Promoter = 'No';
        }
        $serviceProvider = UserType::where('type','ServiceProvider')->where('user_id',$this->id)->first();
        if($serviceProvider){
            
           $ServiceProvider = 'Yes'; 
        }else{
           $ServiceProvider = 'No';
        }
        return [
            'id'            => $this->id,
            'Avatarurl'     => $Avatarurl,
            'city'          => $this->city ?? '',
            'country'       => $this->country ?? '',
            'dob'           => $this->dob ?? '',
            'fname'         => $this->fname ?? '',
            'lname'         => $this->lname ?? '',
            'gender'        => $this->gender ?? '',
            'email'         => $this->email ?? '',
            'VIP'           => $VIP,
            'Promoter'      => $Promoter,
            'ServiceProvider'=> $ServiceProvider,
        ];
    }
}
