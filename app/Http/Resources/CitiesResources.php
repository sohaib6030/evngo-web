<?php
/**
 * Created by Touqeer Hanif.
 * User: gc
 * Date: 04/21/2020
 * Time: 12:27 PM
 */

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
class CitiesResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {




        return [
            'id'=> $this->id,
            'city'=> $this->Name,
            'countrycode'=> $this->CountryCode,
            'countryName'=> $this->CountryName,
            'countryshortcode'=> $this->Country_ShortCode,
        ];
        // return parent::toArray($request);
    }
}

