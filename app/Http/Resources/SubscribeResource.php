<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
use App\User;
use App\models\UserType;
class SubscribeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->avatar=='placeholder.png'){
            $Avatarurl=URL::to('/').$this->user->avatar;
        }else{
            $Avatarurl=URL::to('/').$this->user->avatar;
        }
        
        $vip = UserType::where('type','VIP')->where('user_id',$this->user->id)->first();
        if($vip){
            
           $VIP = 'Yes'; 
        }else{
           $VIP = 'No';
        }
        $promoter = UserType::where('type','Promoter')->where('user_id',$this->user->id)->first();
        if($promoter){
            
           $Promoter = 'Yes'; 
        }else{
           $Promoter = 'No';
        }
        $serviceProvider = UserType::where('type','ServiceProvider')->where('user_id',$this->user->id)->first();
        if($serviceProvider){
            
           $ServiceProvider = 'Yes'; 
        }else{
           $ServiceProvider = 'No';
        }
        
        return [
            'id'            => $this->user->id,
            'Avatarurl'     => $Avatarurl,
            'city'          => $this->user->city ?? '',
            'country'       => $this->user->country ?? '',
            'dob'           => $this->user->dob ?? '',
            'fname'         => $this->user->fname ?? '',
            'lname'         => $this->user->lname ?? '',
            'gender'        => $this->user->gender ?? '',
            'email'         => $this->user->email ?? '',
            'VIP'           => $VIP,
            'Promoter'      => $Promoter,
            'ServiceProvider'=> $ServiceProvider,
        ];
    }
}
