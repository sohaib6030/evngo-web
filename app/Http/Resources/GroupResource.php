<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
use Auth;
use App\models\GroupMember;
class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        if($this->image){
            $image = URL::to('/').Storage::disk('local')->url('public/groups/'.$this->image);
        }else{
            $image ='';// URL::to('/').Storage::disk('local')->url('public/placeholder.png');;
        }
        
        $user = Auth::user();
        
        $member = GroupMember::where('group_id',$this->id)->where('user_id',$user->id)->first();
        
        if($member){
                if($member->status=='Pending'){
                    
                    $status ='Pending';
                }else{
                    $status ='Joined';
                }  
            
        }else{
            
            $status = "Not Joined";
        }
        
        $totalMember = count($this->members);
       
        return [
            'id'=> $this->id,
            'name'=> $this->name,
            'group_type_id'=> $this->group_type->title,
            'description'=> $this->description,
            'created_by'=> new UserResource($this->user),
            'image' => $image,
            'status' => $status,
            'created_at'=> $this->created_at->diffForHumans(),
            'totalMember'=> $totalMember,
        ];
       // return parent::toArray($request);
    }
}
