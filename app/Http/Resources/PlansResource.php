<?php
/**
 * Created by Touqeer Hanif.
 * User: gc
 * Date: 04/21/2020
 * Time: 12:27 PM
 */

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
class PlansResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'type'=> $this->type,
            'tenure'=> $this->tenure,
            'price'=> $this->price,
            'Discount'=> $this->Discount,
            'Saving'=> $this->Saving,
            'MonthlyCost'=> $this->MonthlyCost,
            'status'=> $this->status,
        ];
        // return parent::toArray($request);
    }
}

