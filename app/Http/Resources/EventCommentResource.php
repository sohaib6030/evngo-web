<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use Storage;
use DateTime;
class EventCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
    
        return [
            'id'=> $this->id,
            'comment'=> $this->comment,
            'created_at'=> $this->created_at->diffForHumans(),
            'created_by'=> new UserResource($this->user),
            

            

        ];
       // return parent::toArray($request);
    }
}
