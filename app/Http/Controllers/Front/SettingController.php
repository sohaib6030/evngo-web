<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use App\models\Users;

class SettingController extends Controller
{
    public function profile(Request $request){
    	$user = Auth::user();
        if ($user->avatar == 'placeholder.png') {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $user->avatar);
        } else {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $user->id . '/' . $user->avatar);
        }
        $user['avatarurl'] = $Avatarurl;
        $user['email_verified_at'] = ($user['email_verified_at'] !== null) ? $user['email_verified_at'] : '';
        $user['longitude'] = ($user['longitude'] !== null) ? $user['longitude'] : '';
        $user['latitude'] = ($user['latitude'] !== null) ? $user['latitude'] : '';

    	return view('front.Pages.Setting.Profile',compact('user'));
    }

    public function profile_update(Request $request){

    	$user = Auth::user();
        $this->validate($request,[
            'fname'  => 'required',
            'lname'  => 'required',
            // 'email' => 'required|email|unique:users,email,' . $user->id,
        ],[
        	'fname.required' => 'First Name is Required',
        	'lname.required' => 'Last Name is Required',
        ]);
        $newmail = $request->get('email');
        $existsuser = Users::where('email',$newmail)->first();

        if( ($user->email != $newmail) && ($existsuser != null) )
            {
                return redirect()->back()->with('success','Error : Email Address Already Exists');
            }


        $user->fname   = $request->get('fname');
        $user->lname   = $request->get('lname');
        $user->gender  = $request->get('gender');
        $user->dob     = $request->get('year')."-".Carbon::parse($request->get('month'))->format('n')."-".$request->get('day');
        $user->city    = $request->get('city');
        $user->country = $request->get('country');
        if ($request->hasfile('avatar')) {
        	$file = $request->file('avatar');
            $avatar = Str::random(20) . ".png";//$file->getClientOriginalName();
            Storage::disk('local')->put('/public/users/' . $user->id . '/' . $avatar, File::get($file));
            $user->avatar = $avatar;
    
        }


         $user->save();       
         return redirect()->back()->with('success','Profile updated successfully');

    }
}
