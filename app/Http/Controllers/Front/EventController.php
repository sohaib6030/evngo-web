<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Resources\EventCollection;
use App\Http\Resources\EventCommentCollection;
use App\Http\Resources\EventCommentResource;
use App\Http\Resources\EventTypeCollection;
use App\Http\Resources\FrontEventResource;
use App\Http\Resources\FrontGroupResource;
use App\Http\Resources\GroupCollection;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\ServiceResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\User;
use App\models\Event;
use App\models\EventAction;
use App\models\EventComment;
use App\models\EventService;
use App\models\EventShare;
use App\models\EventType;
use App\models\Group;
use App\models\Service;
use App\models\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use URL;
use Validator;
use function GuzzleHttp\json_encode;


class EventController extends Controller
{

    public $successStatus = 200;

    public function events(){
    	return view('front.Pages.Events');
    }
    public function event(){
        return view('front.Pages.Event');
    }
    public function my_events(){
        return view('front.Pages.MyEvents');
    }

    public function all_events_api(Request $request){
    	$user = Auth::user();
        
        // $data = Event::where('status','Active')
        // ->where('is_deleted','0')
        // ->orderBy('id', 'desc')
        // ->where('status','Active')
        // ->paginate(10);

        $data = EventShare::where('status','Active')->where('is_deleted','0')->orderBy('id', 'desc')->paginate(3);
        // $data = EventShare::whereHas('event', function ($query) use ($user) {
        //         $query->where('status','Active');
        //         $query->where('is_deleted','0');
        // })
        // ->where('is_deleted',0)->orderBy('id', 'desc')->paginate(10);
       // dd($data);
       
        if(count($data)>0){
        	
            return new EventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function event_create(Request $request){
            Log::info($request->all());
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'event_type_id'  => 'required',
                'title'  => 'required',
                'description'  => 'required',
                'venue_address'  => 'required',
                'event_date'  => 'required',
                'event_time'  => 'required',
            ]);
            if ($validator->fails()) {
                $response_data = [
                    'success' => 0,
                    'message' => 'Validation error.',
                    'data' => $validator->errors()
                ];
                return response()->json($response_data,  $this->successStatus);
            }
            if(isset($request->edit_id) && ($request->edit_id !="") && ($request->edit_id !='0') )
            {
                //echo "yes" ; exit;
                //dd($request->all());
                
                $data = Event::findOrFail($request->edit_id);
                $data->created_by  = $user->id;
                $data->event_type_id  = $request->event_type_id;
                $data->event_payment_id  = $request->event_payment_id;
                $data->service_provider_id  = $request->service_provider_id;
                $data->title  = $request->title;
                $data->description  = $request->description;
                $data->venue_address  = $request->venue_address;
                $data->event_date  = $request->event_date;
                $data->event_time  = $request->event_time;
                $data->status  = 'Active';
                if ($request->hasfile('image')) {
                    $file = $request->file('image');
                    $image = $file->getClientOriginalName();
                    Storage::disk('local')->put('/public/event/' . $image, File::get($file));
                    $data->image = $image;
                }    
                $data->save();
                
                if($data){
                    if(isset($request->services) && ($request->services !=""))
                    {
                       
                        $servicesUniq = json_decode($request->services);
                        foreach ($servicesUniq as $value) {
                         $findService = EventService::where('event_id',$data->id)->where('service_id',$value)->first();
                            if($findService){
                                $findService->event_id =   $data->id;
                                $findService->service_id =   $value;
                                $findService->status = 'Active';
                                $findService->created_by = $user->id;
                                $findService->save();
                            }else{
                                 $eventService = new EventService;
                                 $eventService->event_id = $data->id;
                                 $eventService->service_id = $value;
                                 $eventService->status = 'Active';
                                 $eventService->created_by = $user->id;
                                 $eventService->save();
                            }
                         
                       }
                    }   
                }
        
                $response_data = [
                    'success' => 1,
                    'message' => 'Event successfully created.',
                    'data' => $data
                ]; 
                
            }else{
               // echo "no" ; exit;
                $data = new Event;
                $data->created_by  = $user->id;
                $data->event_type_id  = $request->event_type_id;
                $data->event_payment_id  = $request->event_payment_id;
                $data->service_provider_id  = $request->service_provider_id;
                $data->title  = $request->title;
                $data->description  = $request->description;
                $data->venue_address  = $request->venue_address;
                $data->event_date  = $request->event_date;
                $data->event_time  = $request->event_time;
                $data->status  = 'Active';
                if ($request->hasfile('image')) {
                    $file = $request->file('image');
                    $image = $file->getClientOriginalName();
                    Storage::disk('local')->put('/public/event/'. $image, File::get($file));
                    $data->image = $image;
                }    
                $data->save();
                
                if($data){
                    if(isset($request->services) && ($request->services !=""))
                    {
                              
                        $servicesUniq = json_decode($request->services);
                        foreach ($servicesUniq as $value) {
                         $findService = EventService::where('event_id',$data->id)->where('service_id',$value)->first();
                            if($findService){
                                $findService->event_id =   $data->id;
                                $findService->service_id =   $value;
                                $findService->status = 'Active';
                                $findService->created_by = $user->id;
                                $findService->save();
                            }else{
                                 $eventService = new EventService;
                                 $eventService->event_id = $data->id;
                                 $eventService->service_id = $value;
                                 $eventService->status = 'Active';
                                 $eventService->created_by = $user->id;
                                 $eventService->save();
                            }
                         
                      }
                    }   
                }
                
                
                if($data){
                    
                       $EventShare = new EventShare;
                        $EventShare->event_id =   $data->id;
                        $EventShare->user_id =   $user->id;
                        $EventShare->type = 'User';
                        $EventShare->status = 'Active';
                        $EventShare->created_by = $user->id;
                        $EventShare->save();
                }
            
                $response_data = [
                    'success' => 1,
                    'message' => 'Event successfully created.',
                    'data' => $data
                ];
               
               
                
            }
                
            
            return response()->json($response_data, $this->successStatus);
    }



    // Event Types
    public function events_types(){
        $data = EventType::where('status','Active')->where('is_deleted','0')->orderBy('id','desc')->get();
        
        if(count($data)>0){
            
            return new EventTypeCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    // Event Types


    // Event View
    public function event_view(Request $request){
        
        $data = EventShare::where('status','Active')->where('is_deleted','0')->orderBy('id', 'desc');

        if($request->has('event') && is_numeric($request->query('event')) && $request->query('event') > 0){
            $data = $data->where('event_id',$request->query('event'));
        }

        $data = $data->paginate(1);
        //print_r($data->toArray());exit;
        if(count($data)>0){
            return new EventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    
    }
    // Event Types

    // Event Search
    public function searh(Request $request){
        
       return view('front.Pages.search');
    
    }

    public function search_data(Request $request)
    {
        $user = Auth::user();
        $search = $request->search;
        if(isset($search) && ($search !="") )
        {
          $event   = EventShare::with('allevent')->whereHas('allevent',function($q) use ($search) {
              
                                               $q->where('title', 'LIKE', '%' . $search . '%');
                                               
                                            })->where('status','Active')->where('is_deleted','0')->where('type','Admin')->where('event_show','Public')->get();
        
         $group = Group::where('name', 'LIKE', '%' . $search . '%')->where('is_deleted',0)->orderBy('id', 'desc')->get();

         $response_data=[
                'success' => 1,
                'message' => 'success.',
                'event' =>  FrontEventResource::collection($event),
                'group' => FrontGroupResource::collection($group),
            ];
            
            
            if(count($response_data)>0){
            
             return response()->json($response_data,  $this->successStatus);
            }
            else{
                $response_data=[
                    'success' => 0,
                    'message' => 'Data Not Found.'
                ];
                return response()->json($response_data,  $this->successStatus);
            }
            
        }else{
                $response_data=[
                    'success' => 0,
                    'message' => 'Data Not Found.'
                ];
                return response()->json($response_data,  $this->successStatus);
            }
    }

}
