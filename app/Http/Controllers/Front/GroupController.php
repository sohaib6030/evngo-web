<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Resources\GroupCollection;
use App\Http\Resources\UserResource;
use App\models\Group;
use App\models\GroupMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class GroupController extends Controller
{
    public function groups(){
    	return view('front.Pages.Groups');
    }

    public function group_view($group_id)
    {
        $user = Auth::user();
        //$data = Group::where('is_deleted',0)->where('created_by',$user->id)->orderBy('id', 'desc')->paginate(10);
        $data = Group::where('is_deleted',0)->where('id',$group_id)->first();

        if($data){
            if($data->image){
                $image = URL::to('/public').Storage::disk('local')->url('/groups/'.$data->image);
            }else{
                $image ='';
            }
        }else{
            $response_data = [
                'success' => 1,
                'message' => 'Event join successfully.',
                
            ];

            return redirect()->back();
        }
        
        $user = Auth::user();
        
        $member = GroupMember::where('group_id',$data->id)->where('user_id',$user->id)->first();
        
        if($member){
                if($member->status=='Pending'){
                    
                    $status ='Pending';
                }else{
                    $status ='Joined';
                }  
            
        }else{
            
            $status = "Not Joined";
        }
        
        $totalMember = count($data->members);
        if($data->user_id == Auth::user()->user_id){
             $my_group = true;
        }else{
             $my_group = false;
        }
       
        $response =  [
            'id'=> $data->id,
            'name'=> $data->name,
            'group_type_id'=> $data->group_type->title,
            'description'=> $data->description,
            'created_by'=> new UserResource($data->user),
            'image' => $image,
            'status' => $status,
            'created_at'=> $data->created_at->diffForHumans(),
            'my_group'=> $my_group,
            'totalMember'=> $totalMember,
        ];

        return view('front.Pages.Group-view',compact('response'));
        
    }

    public function requests(){
        return view('front.Pages.Requests');    
    }

    public function search(Request $request){
        if($request->query('s')){
            $user = Auth::user();
            return Group::whereHas('members', function ($query) use ($user) {
            $query->where('user_id',$user->id);
            $query->where('status','Approved');
        })->where('is_deleted',0)->orderBy('id', 'desc')->where('name','LIKE','%'.$request->query('s').'%')->paginate(10);
        }else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data);
        }
        
    }
}
