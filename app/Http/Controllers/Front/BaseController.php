<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helper;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use PHPMailer\PHPMailer\PHPMailer;


class BaseController extends Controller
{




    public function make_payment(){

        return view('front.payment');

    }





    // Landing Page start of the project
    public function landing_page(){

        return view('front.landing_page');

    }

    public function user_success(){

        return view('front.user_success');

    }
    public function user_failed(){

        return view('front.user_failed');

    }


    public function user_panel(){



        if(Auth::check() && Auth::user()->role_id == 2){
            return view('front.Pages.Events');
        }
        else{
            Auth::logout();
            return view('front.user_panel');
        }
    }



    public function user_panel_register(){

            Auth::logout();
            return view('front.user_panel_register');

    }



    // Attempt Login (post)
    public function attempt_login(Request $request){

        $this->validate($request,[
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $response_data = [];

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            if($user->role_id != 2){
                $response_data = [
                    'success' => 0,
                    'message' => 'Your login credentials are incorrect.'
                ];
            }else{

                $web_token = $user->createToken('web_token')->accessToken;
                session(['web_token' => $web_token]);
                // session(['auth_user' => new UserResource($user) ]);
                return redirect('/events');

            }

        }

        return redirect()->back()->with(['message' => $response_data]);

    }


    // User Regiser (post)
    public function user_register(Request $request){
        Session::flash('registration', true);
        $data = $this->validate($request,[
            'fname' => 'required|string|min:2|max:35',
            'lname' => 'required|string|min:2|max:35',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'day' => 'required|min:1|max:31',
            'month' => 'required|min:1|max:12',
            'year' => 'required|string',
            'gender' => 'required|string',
            'country' => 'required|string',
            'city' => 'required|string',
        ],[
            'required' => 'Required'
        ]);

        $user = new User;
        $user->status = '1';
        $user->role_id = '2';
        $code = rand(999, 99999);
        $user->verify_code = $code;
        $user->verify = 0;
        $user->fname = $data['fname'];
        $user->lname = $data['lname'];
        $user->name = $data['fname']." ".$data['lname'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->dob = $data['year']."-".$data['month']."-".$data['day'];
        $user->gender = $data['gender'];
        $user->country = $data['country'];
        $user->city = $data['city'];
        $user->save();

        //  Helper::sendVerificationEmail($user,$code);
        Helper::sendEmail($user,$code);
        // $this->sendEmail($data,$code);
        $response_data = [
            'success' => 1,
            'message' => $user->fname . ' ' . $user->lname .  ' your
registration has been successful please check you email and
verify your account (if you cannot find this email please check
your junk mail)',
        ];
        // Session::flash('email_confirmation', true);
        return redirect()->back()->with(['message' => $response_data]);


    }

    public function user_logout(Request $request){
        if($request->logout){
            Auth::logout();
            return redirect('/');
        }
    }

    public function verify_email($email,$code){
        // Session::flash('email_confirmation', true);
        $data =  User::where('email',$email)->first();
        if($data->verify_code==$code){
            $data->verify = '1';
            $data->save();
            return redirect('/');
        }else{

            return redirect('/');
        }
    }

    // password Reset
    public function reset_password($email,$token){

        $user = User::where('email',$email)->first();
        $check = Password::getRepository()->exists($user,$token);

        if($check){
            return self::reset_password_form($user,$token);
        }else{
            Session::flash('forget_password', true);
            $response_data = [
                'success' => 0,
                'message' => 'Invalid link.',
            ];
            return redirect('/user_panel')->with(['message' => $response_data]);

        }

    }
    //End password Reset 

    // password Form
    public function reset_password_form($user,$token){
        return view('front.auth.change_password')->with('user',$user)->with('token',$token);
    }
    //End password Form

    // set new password
    public function reset_new_password(Request $request){
        $data = $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|string|confirmed|min:6',
            'token' => 'required|string',
        ]);

        $user = User::where('email',$data['email'])->first();

        if($user){
            $check = Password::getRepository()->exists($user,$data['token']);
            if($check){

                $user->password = bcrypt($data['password']);
                $user->save();

                $response_data = [
                    'success' => 0,
                    'message' => 'Password has been changed.'
                ];

                return redirect('/')->with(['message' => $response_data]);

            }
        }



    }
    //End set new password


    // Forget Password
    public function user_forget_password(Request $request){
        Session::flash('forget_password', true);
        $this->validate($request,[
            'email' => 'required|email',
        ]);
        $response_data = [];

        $email = $request->email;
        $user =  User::where('email',$email)->first();
        if($user)
        {
            $token = Password::getRepository()->create($user);
            // $this->sendEmailForgetPassword($user,$token);
            Helper::sendEmailForgot($user,$token);
            // sendEmailForgot
            $response_data = [
                'success' => 1,
                'message' => 'Account password reset successfully.Please check your email account and follow the instruction!',
            ];


        }else{
            $response_data = [
                'success' => 0,
                'message' => 'This email address does not exist!'
            ];


        }

        return redirect()->back()->with(['message' => $response_data]);
    }

    private function sendEmailForgetPassword($user, $code)
    {

        $mail = Mail::send('emails.forgotPassword', [
            'user' => $user,
            'code' => $code
        ], function($message) use ($user){

            $message->to($user->email);
            $message->subject("Hello ".$user->fname.", reset your password");

        });

    }

    public function firebase_token_store(Request $request){

        if($request->firebase_token){
            $user = User::find(auth()->user()->id);
            $user->firebase_token = $request->firebase_token;
            $user->save();

        }

    }


}
