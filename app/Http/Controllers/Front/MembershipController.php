<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class MembershipController extends Controller
{
    public function subscribe(){
    	return view('front.membership.subscribe');
    }

    public function subscribe_type($type){
    	if($type){
    		$role = Role::find($type);
    		if($role){
    			$user = User::find(auth()->user()->id);
    			$user->role_id = $type;
    			$user->save();

    			$response_data = [
	            'success' => 1,
	            'message' => 'You have been upgrated.'
	       		 ];
	       		 return redirect('/')->with(['message' => $response_data]);

    		}else{
    			$response_data = [
	            'success' => 0,
	            'message' => 'Error'
	       		 ];
	       		 return redirect()->back()->with(['message' => $response_data]);
    		}
    	}
    }
}
