<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\GroupResource;
use App\Http\Resources\GroupCollection;
use App\Http\Resources\GroupMemberResource;
use App\Http\Resources\GroupMemberCollection;
use App\Http\Resources\GroupTypeResource;
use App\Http\Resources\GroupTypeCollection;
use App\Http\Resources\UserReceviedGroupRequestResource;
use App\Http\Resources\UserReceviedGroupRequestCollection;
use App\models\Event;
use App\models\Group;
use App\models\GroupMember;
use App\models\GroupEvent;
use App\models\GroupType;
use App\models\EventShare;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use URL;
use Validator;
use function GuzzleHttp\json_encode;
use App\Http\Resources\EventCollection;
use Illuminate\Support\Facades\Log;
use FCM;


class GroupController extends Controller
{
    public $successStatus = 200;
    
    
    
    public function groupEvents(Request $request)
    {
        $user = Auth::user();
        
        $group_id = $request->group_id;
        $data = EventShare::where('status','Active')
        ->where('group_id',$group_id)
        ->where('user_id',$user->id)
        ->where('is_deleted','0')
        ->orderBy('id', 'desc')->paginate(10);

        if(count($data)>0){
            return new EventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    
    
    public function userSendGroupRequest(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'group_id'  => 'required',
           // 'user_id'  => 'required',
            //'role_in_group'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        $alreadyMember = GroupMember::where('user_id',$user->id)
        ->where('group_id',$request->group_id)
       // ->where('status','Pending')
        ->where('is_deleted',0)
        ->orderBy('updated_at', 'desc')
        ->first();
        if($alreadyMember)
        {
            $response_data = [
                'success' => 1,
                'message' => 'Request Already Sent.'
            ];
        }else{
            $group = Group::findOrFail($request->group_id);
           
            $data = new GroupMember;
            $data->created_by     = $user->id;
            $data->group_id       = $request->group_id;
            $data->user_id        = $user->id;
            $data->requested_by        = $group->user->id;
            $data->role_in_group  = 'Member';
            $data->status         = 'Pending';
            $data->type         = 'RequestUser';
            $data->save();
            
            $recipients =[];
            if($data->group->user->firebase_token){
                $recipients[] = $data->group->user->firebase_token;
            }
            if($data->group->user->devices){
                foreach($data->group->user->devices as $device){
                    $recipients[] = $device->registerid;
                }
            }
                    
             //dd($recipients);       
            if(count($recipients) > 0){
                $result=fcm()->to($recipients) // $recipients must an array
                ->data([
                        'content_available' => true,
                        'title' => 'Evengo Group Request',
                        'body' => $data->member->fname.' '.$data->member->lname.' want to join  ' . $data->group->name,
                        'notification_type' => 'group_request_notification',
                  ]
                )
                    ->notification([
                        'sound' => 'default',
                        'content_available' => true,
                        'title' => 'Evengo Group Request',
                        'body' => $data->member->fname.' '.$data->member->lname.' want to join  ' . $data->group->name,
                        'channelid' => 'group_request_notification',
                        'notification_type' => 'group_request_notification',
                    ])
                // ->notification([
                //     'title' => $data->member->fname.' '.$data->member->lname.' want to join  ' . $data->group->name,
                //      'body' => '',
                // ])
                ->send();
                //dd($result);
            }

            $response_data = [
                'success' => 1,
                'message' => 'Your request  successfully send.',
                'data' => new GroupMemberResource($data)
            ]; 
            
        }
            
        
        return response()->json($response_data, $this->successStatus);

    
    }
    
     
    public function removeRequest(Request $request)
    {
        if($request->status=='Rejected'){    
          $data =  GroupMember::where('user_id', $request->member_id)
          ->where('group_id', $request->group_id)
          ->where('is_deleted','0')
          ->where('status' , 'Approved')
          ->firstOrFail(); 
          $data->is_deleted = '1';
          $data->save();
          $message = 'Member Removed From Group Successfully.';
        }
               if($data){
                $response_data = [
                    'success' => 1,
                    'message' => $message,
                ];
            }
             else{
            $response_data=[
                'success' => 0,
                'message' => 'Member Not Found.'
            ];
           
        }
         return response()->json($response_data,  $this->successStatus);
      
    }
    
    
    public function acceptRejectRequest(Request $request)
    {
      
      
      $user = Auth::user();
      if($request->status=='Approved'){    
          $data = GroupMember::findOrFail($request->member_id);
          $data->status = 'Approved';
          $data->save();
          $message = 'Request successfully Approved.';
      }else if($request->status=='Rejected'){    
          $data = GroupMember::findOrFail($request->member_id);
          $data->is_deleted = '1';
          $data->save();
          $message = 'Request successfully Rejected.';
      }
      if($data){
      $response_data = [
                'success' => 1,
                'message' => $message,
            ];
      }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
           
        }
         return response()->json($response_data,  $this->successStatus);

    }
    
    
    public function userReceviedGroupRequest(Request $request)
    {
        $user = Auth::user();
        $data = GroupMember::where('user_id',$user->id)->where('status','Pending')->where('type','RequestGroup')->orderBy('id', 'desc')->paginate(10);

        if(count($data)>0){
            return new UserReceviedGroupRequestCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    public function receviedRequest(Request $request)
    {
        $user = Auth::user();
        $data = GroupMember::where('status','Pending')->where('requested_by',$user->id)->where('role_in_group','Member')->where('is_deleted','0')->orderBy('id', 'desc')->paginate(10);

        if(count($data)>0){
            return new UserReceviedGroupRequestCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    public function groupTypes()
    {
        $data = GroupType::where('is_deleted',0)->orderBy('id', 'desc')->get();

        if(count($data)>0){
            return new GroupTypeCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    public function allUsers(Request $request)
    {
        $user_search = ($request->searchuser != null || $request->searchuser != "" ? $request->searchuser : "");
        $seachusers="";
        //Search User
        if($user_search == "")
        {
            $seachusers = user::where('status','1')->pluck('id');
        }
        else
        {
            $seachusers = user::where('status','1')->whereRaw("CONCAT(`fname`, ' ', `lname`) like ? ", '%' . $user_search . '%')->pluck('id');
        }

        $id = $request->group_id;
        $ids = GroupMember::where('group_id',$id)->pluck('user_id');
        $data = User::whereNotIN('id',$ids)
            ->where('role_id',2)
            ->where('status',1)
            ->whereIn('id',$seachusers)
            ->orderBy('id', 'desc')
            ->paginate(10);

        if(count($data)>0){
            return new UserCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    
    
    
    
    public function allGroups()
    {
        $user = Auth::user();

        $data = Group::whereHas('members', function ($query) use ($user) {
            $query->where('user_id','!=',$user->id);
            $query->where('status','Approved');
        })
        ->where('is_deleted',0)->orderBy('id', 'desc')->where('created_by','!=',$user->id)->paginate(10);


        if(count($data)>0){
            return new GroupCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    
    public function myGroups()
    {
        $user = Auth::user();
        $data = Group::where('is_deleted',0)->where('created_by',$user->id)->orderBy('id', 'desc')->paginate(10);

        if(count($data)>0){
            return new GroupCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    
    
    
    
    
    public function myJoinGroups()
    {
        
        $user = Auth::user();
        $data = Group::whereHas('members', function ($query) use ($user) {
            $query->where('user_id',$user->id);
            $query->where('status','Approved');
        })
        ->where('is_deleted',0)->orderBy('id', 'desc')->where('created_by','!=',$user->id)->paginate(10);
        
        if(count($data)>0){
            return new GroupCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

   

    public function store(Request $request)
    {
        
        Log::info($request->all());
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'group_type_id'  => 'required',
            'description'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        if(isset($request->edit_id) && ($request->edit_id !="") )
        {
            
            $data = Group::findOrFail($request->edit_id);
            $data->updated_by  = $user->id;
            $data->name  = $request->name;
            $data->group_type_id  = $request->group_type_id;
            $data->description  = $request->description;
            
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $image = $file->getClientOriginalName();
                Storage::disk('local')->put('/public/groups/' . $image, File::get($file));
                $data->image = $image;
            }    
            $data->save();
    
            $response_data = [
                'success' => 1,
                'message' => 'Group successfully updated.',
                //'data' => new GroupResource($data)
            ];
        }else{
           
            $data = new Group;
            $data->created_by  = $user->id;
            $data->name  = $request->name;
            $data->group_type_id  = $request->group_type_id;
            $data->description  = $request->description;
            $data->status  = 'Pending';
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $image = $file->getClientOriginalName();
                Storage::disk('local')->put('/public/groups/' . $image, File::get($file));
                $data->image = $image;
            }    
            $data->save();
            
            if($data){
                
                $member = new GroupMember;
                $member->created_by     = $user->id;
                $member->group_id       = $data->id;
                $member->user_id        = $user->id;
                $member->role_in_group  = 'Admin';
                $member->status         = 'Approved';
                $member->save();
            }
    
            $response_data = [
                'success' => 1,
                'message' => 'Group successfully created.',
               // 'data' => new GroupResource($data)
            ]; 
            
        }
            
        
        return response()->json($response_data, $this->successStatus);

    
    }
    
    
    public function groupDelete(Request $request)
    {
      $data = Group::findOrFail($request->group_id);
      $data->is_deleted = '1';
      $data->save();
      if($data){
      $response_data = [
                'success' => 1,
                'message' => 'Group successfully deleted.',
            ];
      }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
           
        }
         return response()->json($response_data,  $this->successStatus);

    }
    
    public function memberAddInGroup(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'group_id'  => 'required',
            'user_id'  => 'required',
            'role_in_group'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        $alreadyMember = GroupMember::where('user_id',$request->user_id)->where('group_id',$request->group_id)->first();
        if($alreadyMember)
        {
            $response_data = [
                'success' => 0,
                'message' => 'Already a member.'
            ];
        }else{
             $group = Group::findOrFail($request->group_id);
            $data = new GroupMember;
            $data->created_by     = $user->id;
            $data->group_id       = $request->group_id;
            $data->user_id        = $request->user_id;
            $data->requested_by        = $request->user_id;
            $data->role_in_group  = $request->role_in_group;
            $data->status         = 'Pending';
            $data->type         = 'RequestGroup';
            $data->save();
            
            $recipients =[];
            if($data->member->firebase_token){
                $recipients[] = $data->member->firebase_token;
            }
            if($data->member->devices){
                foreach($data->member->devices as $device){
                    $recipients[] = $device->registerid;
                }
            }
                    
                    
            if(count($recipients) > 0){
                $result=fcm()->to($recipients) // $recipients must an array
                ->data([
                        'content_available' => true,
                        'title' => 'Evengo Group Invite',
                        'body' => $data->user->fname.' '.$data->user->lname.' requested you to  join  ' . $data->group->name,
                        'notification_type' => 'group_request_notification',
                    ]
                )
                    ->notification([
                        'sound' => 'default',
                        'content_available' => true,
                        'title' => 'Evengo Group Invite',
                        'body' => $data->user->fname.' '.$data->user->lname.' requested you to  join  ' . $data->group->name,
                        'channelid' => 'group_request_notification',
                        'notification_type' => 'group_request_notification',
                        ])
                //->notification([
                //    'title' => $data->user->fname.' '.$data->user->lname.' requested you to  join  ' . $data->group->name,
                //     'body' => '',
                //])
                ->send();
            }
        }



        /*
        $response_data = [
            'success' => 1,
            'message' => 'Member successfully added.',
            'data' => new GroupMemberResource($data)
        ];
*/



        if($data){
            $response_data = [
                'success' => 1,
                'message' => 'Member successfully added.',
                'data' => new GroupMemberResource($data)
            ];
        }else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

        }


        return response()->json($response_data, $this->successStatus);

    
    }
    
    public function groupMembers(Request $request)
    {
        $user_search = ($request->searchuser != null || $request->searchuser != "" ? $request->searchuser : "");
        $seachusers="";

        //Search User
        if($user_search == "")
        {
            $seachusers = user::where('status','1')->pluck('id');
        }
        else
        {
            $seachusers = user::where('status','1')->whereRaw("CONCAT(`fname`, ' ', `lname`) like ? ", '%' . $user_search . '%')->pluck('id');
        }


        $data = GroupMember::where('group_id',$request->group_id)
            ->where('status','Approved')
            ->where('is_deleted',0)
            ->whereIn('user_id',$seachusers)
            ->orderBy('id', 'desc')
            ->paginate(10);

        if(count($data)>0){
            return new GroupMemberCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
   
    
    public function edit(Request $request)
    {
      $data = Event::findOrFail($request->event_id);

        if($data){
            return new EventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }

    }


    public function memberBlock(Request $request)
    {
      $data = GroupMember::findOrFail($request->member_id);
      $data->status = 'Block';
      $data->save();
      if($data){
      $response_data = [
                'success' => 1,
                'message' => 'Member successfully blocked.',
            ];
      }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
           
        }
         return response()->json($response_data,  $this->successStatus);

    }

  

}
