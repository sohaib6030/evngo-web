<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mail\MailchimpController;
use App\Http\Resources\PlansDetailResource;
use App\Http\Resources\PlansResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\SubscribeResource;
use App\models\PlansDetail;
use App\SpCategory;
use App\User;
use App\UserDetails;
use App\models\UserType;
use App\models\UserDevices;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use URL;
use Validator;
use Mail;
use function GuzzleHttp\json_encode;
use App\Helpers\Mail\PHPMailer;
use App\Helpers\Mail\SMTP;
use App\models\Users;
use App\models\Plans;
use DB;


class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = Auth::user();
            if($user->status==1 && $user->verify==1){
                //dd($user);
                $success['token'] =  $user->createToken('MyApp')->accessToken;

                // if ($user->avatar == 'placeholder.png') {
                //     $Avatarurl = URL::to('/') . Storage::disk('local')->url('app/public/users/' . $user->avatar);
                // } else {
                //     $Avatarurl = URL::to('/') . Storage::disk('local')->url('app/public/users/' . $user->id . '/' . $user->avatar);
                // }
                // $user['avatar'] = $Avatarurl;

                //Insert register device data begins

                $id = auth()->user()->id;
                $token = $success['token'];


                if (request('registerid')) {
                    $userdevice = \App\Userdevice::firstOrCreate(['registerid' => request('registerid'), 'user_id' => auth()->user()->id]);
                    //$this->addDevice($id, $token);
                }






                $response_data = [
                    'success' => 1,
                    'message' => 'Login success.',
                    'data' => $success,
                    'user' => new UserResource($user)
                ];



                return response()->json($response_data, $this->successStatus);
            }else {
                $response_data = [
                    'success' => 2,
                    'message' => 'Your account is not verified.Please check email and verifiy your account'
                ];
                return response()->json($response_data,  $this->successStatus);
            }

        } else {
            $response_data = [
                'success' => 0,
                'message' => 'Invalid Email or Password, please try again.'
            ];
            //return response()->json(['error'=>'Unauthorised'], 401);
            return response()->json($response_data,  $this->successStatus);
        }
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fname'         => 'required',
            'lname'         => 'required',
            'email'         => 'required|email|unique:users',
            'dob'           => 'required',
            'password'      => 'required',
            'c_password'    => 'required|same:password',
            'city'          => 'required',
            'country'       => 'required',
            'gender'        => 'required',
        ]);

        $email = '';
        if ($validator->fails()) {
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                foreach ($messages as $message) {
                    if ($field_name == 'email') {
                        $email = $message;
                    }

                }
            }
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'email' => ($email != '') ? $email : 'incorrect data provided',
                'user' => null,

            ];
            return response()->json($response_data);
        }


        $input = $request->all();
        //dd($input);
        $input['password'] = bcrypt($input['password']);
        $input['fname'] = $input['fname'];
        $input['status'] = '1';
        $input['role_id'] = '2';
        $user = User::create($input);
        $data = User::find($user->id);
        $data->fname = $input['fname'];
        $code = rand(999, 99999);
        $data->verify_code = $code;
        $data->verify = 0;
        $data->save();

        //Insert register device data begins
        if($request->registerid){
            $userdevice = \App\Userdevice::firstOrCreate(['registerid' => $request->registerid, 'user_id' => $data->id]);
        }

        // email for verification
        $this->sendEmail($data,$code);

        $success['token'] =  $user->createToken('MyApp')->accessToken;

        $response_data = [
            'success' => 1,
            'message' =>  $data->fname . ' ' . $data->lname .  ' your
registration has been successful please check you email and
verify your account (if you cannot find this email please check
your junk mail)',
            'email' => '',
            'user' => new UserResource($data),
        ];
        return response()->json($response_data, $this->successStatus);
    }




    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        if ($user->avatar == 'placeholder.png') {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $user->avatar);
        } else {
            $Avatarurl = URL::to('/') . Storage::disk('local')->url('public/users/' . $user->id . '/' . $user->avatar);
        }
        $user['avatarurl'] = $Avatarurl;
        $user['email_verified_at'] = ($user['email_verified_at'] !== null) ? $user['email_verified_at'] : '';
        $user['longitude'] = ($user['longitude'] !== null) ? $user['longitude'] : '';
        $user['latitude'] = ($user['latitude'] !== null) ? $user['latitude'] : '';
        $response_data = [
            'success' => 1,
            'message' => 'Profile data received.',
            'data' => $user
        ];
        return response()->json($response_data, $this->successStatus);
    }

    public function uploadAvatar(Request $request)
    {

        $user = Auth::user();
        if ($request->hasfile('avatar')) {
            $file = $request->file('avatar');
            $avatar = Str::random(20) . ".png";//$file->getClientOriginalName();
            Storage::disk('local')->put('/public/users/' . $user->id . '/' . $avatar, File::get($file));
            $user->avatar = $avatar;
            $user->save();
            $response_data = [
                'success' => 1,
                'message' => 'Profile picture uploaded successfully.',
                'user' => new UserResource($user)
            ];
            return response()->json($response_data, $this->successStatus);
        } else {
            $response_data = [
                'success' => 0,
                'message' => 'Unable to upload, please try again.'
            ];
            return response()->json(['error' => 'Nothing to upload'],  $this->successStatus);
        }
    }


    public function editProfile(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'fname'  => 'required',
            'lname'  => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }

        $newmail = $request->get('email');
        $existsuser = Users::where('email',$newmail)->first();

        if( ($user->email != $newmail) && ($existsuser != null) )
        {
            $response_data = [
                'success' => 0,
                'message' => 'Email Address Already Exists'

            ];
            return response()->json($response_data,  $this->successStatus);
        }



        $user->fname  = $request->get('fname');
        $user->lname  = $request->get('lname');
        $user->gender = $request->get('gender');
        $user->email = $request->get('email');
        $user->dob = $request->get('dob');
        $user->city = $request->get('city');
        $user->country = $request->get('country');
        $user->save();
        // if ($user->avatar == 'placeholder.png') {
        //     $Avatarurl = Storage::disk('local')->url('public/users/' . $user->avatar);
        // } else {
        //     $Avatarurl = Storage::disk('local')->url('public/users/' . $user->id . '/' . $user->avatar);
        // }
        // $user['avatar'] = $Avatarurl;
        $response_data = [
            'success' => 1,
            'message' => 'Profile updated successfully.',
            'user' => new UserResource(Auth::user())
        ];
        return response()->json($response_data, $this->successStatus);
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        //Check The Current Password Matched
        if (!Hash::check($request->get('currentPassword'), $user->password)) {
            $response_data = [
                'success' => 0,
                'message' => 'Current password not matched.',
            ];
            return response()->json($response_data,  $this->successStatus);
        }

        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        //bcrypt
        $user->password = Hash::make($request->get('password'));
        $date = date_create($request->get('date'));
        $format = date_format($date, "Y-m-d");
        $user->updated_at = strtotime($format);
        $user->save();
        $response_data = [
            'success' => 1,
            'message' => 'Password changed successfully.',
        ];
        return response()->json($response_data, $this->successStatus);
    }


    public function subscribe(Request $request)
    {

        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'type'  => 'required',
            'stripe_token'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }

        DB::table('user_types')
            ->where('user_id', $request->user_id)
            ->where('status', 'Active')
            ->update(['status' => "Expired"]);


        $data = new UserType;
        $data->user_id  = $user->id;
        $data->type  = $request->type;
        $data->stripe_token  = $request->stripe_token;
        $data->amount  = $request->amount;
        $data->start_date = date("Y-m-d");
        $data->end_date = date("Y-m-d", strtotime("+" . $request->days ?? 30 . "days"));
        $data->save();

        $response_data = [
            'success' => 1,
            'message' => 'Successfully subscribe.',
            'user' => new SubscribeResource($data)
        ];
        return response()->json($response_data, $this->successStatus);
    }

    public function verify($email, $code)
    {

        $data =  User::where('email',$email)->first();
        if($data->verify_code==$code){
            $data->verify = '1';
            $data->save();
            return redirect('/user_success');
        }else{
            return redirect('/user_failed');
        }

    }


    public function  testmail()
        {
        $to_name = 'EVNGO';
        $to_email = 'sohaib@digitalxconcept.com.au';
        $data = array('name'=>"Ogbonna Vitalis(sender_name)", "body" => "A test mail");
        Mail::send('emails.mails', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('This is the Test Email');
            $message->from('touqeer.hanif91@gmail.com','Testing Email');
        });

    }

    public function  getPlans(Request $request)
{
    $type = $request->type ?? "";

    if($type == "") {
        $data =  Plans::get();
    }
    else {
        $data =  Plans::where('type',$type)->get();
    }

    $response_data = [
        'success' => 1,
        'message' => 'success',
        'data' => PlansResource::collection($data)
    ];
    return response()->json($response_data,  $this->successStatus);
}


    // accepr request
    public function  acceptPlans(Request $request)
    {


        $mailstatus = "Rejected";
        if ($request->status ==  "Active")
        {
            // make expire all previuos plains
            DB::table('plan_details')
                ->where('user_id', $request->user_id)
                ->where('status', 'Active')
                ->update(['status' => "Expired"]);
            $mailstatus = "Approved";
        }

        $data = PlansDetail::where('id',$request->plan_id)->first();
        $data->status = $request->status;
        $data->save();
        $plan_selected = Plans::where('id',$data->plan_id)->first();
        $mail = new PHPMailer(true);

        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = 'smtp.mailgun.org';                    // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = 'postmaster@mg.evngo.com.au';                     // SMTP username
            $mail->Password = 'cb03cfba6fa72239728cdff564ea5cb8-ee13fadb-c84d309a';                              // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = 2525;// TCP port to connect to

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );




            $mail->setFrom('postmaster@mg.evngo.com.au', 'EVNGO');
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "New Subscription Plan  For " . $plan_selected->type;
            $mail->Body = "<h2>Dear " .  $data->user->fname . " " . $data->user->lname  . "</h2>
                           <p>New Subscription Plan  Have Been " . $mailstatus  . " With Following details:</p>
            <p>Name : " . $data->user->fname . " " . $data->user->lname . "</p>" .
                "<p>Contact : " . $data->user->mobile . "</p>" .
                "<p>Email : " . $data->user->email . "</p>" .
                "<p>Address : " . $data->user->address . "</p>";


            if ($plan_selected->type == "Promoter") {
                    $mail->Body = $mail->Body .
                    "<p>Services : " . $data->services . "</p>" .
                    "<p>Licences : " . $data->licences . "</p>" .
                    "<p>Venue capacity : " . $data->capacity . "</p>";
            }
            else if ($plan_selected->type == "Organizer") {

                     $mail->Body = $mail->Body .
                    "<p>Services : " . $data->services . "</p>" .
                    "<p>Promotion Categories  : " . $data->promotions . "</p>" ;


            }


            $mail->Body = $mail->Body . "<p>Plan Details :" .  $plan_selected->type   . " " . $plan_selected->tenure . "</p>";

            $mail->Body = $mail->Body . "<p>please login to continue</p>";
            $mail->addAddress($data->user->email, $data->user->fname);
            $mail->send();

        } catch (Exception $e) {
            $response_data = [
                'success' => 0,
                'message' => 'error',


            ];
            return response()->json($response_data,  $this->successStatus);
        }

            $response_data = [
                'success' => 1,
                'message' => 'success'
            ];

        return response()->json($response_data,  $this->successStatus);
    }


   // generate request
    public function  requestPlans(Request $request)
    {

        $user = Auth::user();

        $newmail = $request->email;
        $existsuser = Users::where('email',$newmail)->first();

        if( ($user->email != $newmail) && ($existsuser != null) )
        {
            $response_data = [
                'success' => 0,
                'message' => 'Email Address Already Exists'

            ];
            return response()->json($response_data,  $this->successStatus);
        }

        $user->lname = $request->lname;
        $user->fname = $request->fname;
        $user->address = $request->address;
        $user->mobile = $request->mobile;
        $user->save();

        if ($request->for == "VIP") {

            DB::table('plan_details')
                ->where('user_id', $user->id)
                ->where('status', 'Active')
                ->update(['status' => "Expired"]);
        }



        $data = new  PlansDetail;
        $data->user_id = $user->id;
        $data->licences = $request->licences ?? "";
        $data->capacity = $request->capacity ?? 0;
        $data->services = $request->services ?? "";
        $data->promotions = $request->promotions ?? "";
        $data->plan_id = $request->plan_id;
        $data->status = $request->status;
        $data->created_by = $user->id;
        $data->updated_by = $user->id;
        $data->save();

        if ($request->for == "VIP") {


            $plandetails =  PlansDetail::where('user_id',$user->id)->get();


            if ($plandetails)
            {
                $response_data = [
                    'success' => 1,
                    'message' => 'success',
                    'data' => PlansDetailResource::collection($plandetails),

                ];
            }
            else
            {
                $response_data = [
                    'success' => 0,
                    'message' => 'success',
                    'data' => PlansDetailResource::collection($plandetails),
                ];
            }


        }




        $mail = new PHPMailer(true);

        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = 'smtp.mailgun.org';                    // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = 'postmaster@mg.evngo.com.au';                     // SMTP username
            $mail->Password = 'cb03cfba6fa72239728cdff564ea5cb8-ee13fadb-c84d309a';                              // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = 2525;// TCP port to connect to

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Recipients
            $mail->setFrom('postmaster@mg.evngo.com.au', 'EVNGO');
            //  $mail->addAddress($email_a, $user->fname  );     // Add a recipient
         //   $mail->addAddress('sohaib6030@gmail.com', $data->fname);     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "New Subscription Plan Request For " . $request->for;
            $mail->Body = "<h2>Dear Admin</h2>
                           <p>New Subscription Plan Request Have Been Received With Following details:</p>
            <p>Name : " . $user->fname . " " . $user->lname . "</p>" .
                "<p>Contact : " . $user->mobile . "</p>" .
                "<p>Email : " . $user->email . "</p>" .
                "<p>Address : " . $user->address . "</p>";


            if ($request->for == "Promoter") {
                $mail->Body = $mail->Body .
                    "<p>Services : " . $request->services . "</p>" .
                    "<p>Licences : " . $request->licences . "</p>" .
                    "<p>Venue capacity : " . $request->capacity . "</p>";
            }
            else if ($request->for == "Organizer") {

                    $mail->Body = $mail->Body .
                    "<p>Services : " . $request->services . "</p>" .
                    "<p>Promotion Categories  : " . $request->promotions . "</p>" ;


            }


            $plan_selected = Plans::where('id',$data->plan_id)->first();
            $mail->Body = $mail->Body . "<p>Plan Details :" .  $plan_selected->type   . " " . $plan_selected->tenure . "</p>";

            $mail->Body = $mail->Body . "<p>please login to admin panel to activate plan</p>";
            $admins = Users::where('role_id',1)->get();
            foreach ($admins as $admin) {
                $mail->addAddress($admin->email, $admin->fname);

            }
            $mail->send();

        } catch (Exception $e) {
            $response_data = [
                'success' => 0,
                'message' => 'error',


            ];
            return response()->json($response_data,  $this->successStatus);
        }


        $plandetails =  PlansDetail::where('user_id',$user->id)->get();

        if ($plandetails)
        {
            $response_data = [
                'success' => 1,
                'message' => 'success',
                'data' => PlansDetailResource::collection($plandetails),
            ];
        }
        else
            {
                $response_data = [
                    'success' => 0,
                    'message' => 'success',
                    'data' => PlansDetailResource::collection($plandetails),
                ];
            }


        return response()->json($response_data,  $this->successStatus);
    }


    public function  getUserPlans(Request $request)
    {

        $user_id =  $request->user_id ?? 0;

        if ($user_id == 0)
        {
            $plandetails = PlansDetail::get();
        }
        else {
            $plandetails = PlansDetail::where('user_id', $user_id)->get();
        }

        if ($plandetails)
        {
            $response_data = [
                'success' => 1,
                'message' => 'success',
                'data' => PlansDetailResource::collection($plandetails),
            ];
        }
        else
        {
            $response_data = [
                'success' => 0,
                'message' => 'success',
                'data' => PlansDetailResource::collection($plandetails),
            ];
        }


        return response()->json($response_data,  $this->successStatus);
    }



    private function sendEmail($data,$code)
    {

        $url = url('verify/accounts/'.$data->email.'/'.urlencode($code));
        $email_a = trim($data->email);

        $mail = new PHPMailer(true);

        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = 'smtp.mailgun.org';                    // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = 'postmaster@mg.evngo.com.au';                     // SMTP username
            $mail->Password = 'cb03cfba6fa72239728cdff564ea5cb8-ee13fadb-c84d309a';                              // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = 2525;// TCP port to connect to

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Recipients
            $mail->setFrom('postmaster@mg.evngo.com.au', 'EVNGO');
            //  $mail->addAddress($email_a, $user->fname  );     // Add a recipient
            $mail->addAddress($email_a, $data->fname);     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "Hello " . $data->fname . " verify your account";
            //  $mail->Body    = "test";
            $mail->Body = "<h2>Verify Your Account</h2><p>To verify your account,<a href=".$url.">click here.</a></p><p>Or point your browser to this address: <br />".$url."</p><p>Thank you!</p>";

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }






    private function sendEmailold($data, $code)
    {

        $url = url('verify/accounts/'.$data->email.'/'.urlencode($code));
        $email = $data->email;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.pepipost.com/v2/sendEmail",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"personalizations\":[{\"recipient\":\"$email\"}],\"from\":{\"fromEmail\":\"info@evngo.com.au\",\"fromName\":\"Evngo\"},\"subject\":\"Hello ".$data->fname.", verify your account\",\"content\":\"<h2>Verify Your Account</h2><p>To verify your account,<a href=".$url.">click here.</a></p><p>Or point your browser to this address: <br />".$url."</p><p>Thank you!</p>\"}",
            CURLOPT_HTTPHEADER => array(
                "api_key: 437b449d2ba275404aad973471587f67",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        //print_r($response);
        curl_close($curl);

        return;

        // $mail = Mail::send('emails.verify', [
        // 'data' => $data,
        // 'code' => $code,
        // ], function($message) use ($data){

        //   $message->to($data->email);
        //   $message->subject("Hello ".$data->fname.", verify your account");

        //   });

    }

    public function sendEmails()
    {
        $data = User::find(1);

        $code ='sdsdsd';
        $mail = Mail::send('emails.verify', [
            'data' => $data,
            'code' => $code,
        ], function($message) use ($data){

            $message->to('postmaster@mg.evngo.com.au');
            $message->subject("Hello  verify your account");

        });

    }

    public function registerid(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'registerid'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }

        $userdevice = \App\Userdevice::firstOrCreate(['registerid' => $request->registerid, 'user_id' => $user->id]);



        $response_data = [
            'success' => 1,
            'message' => 'Registerid successfully saved.',
            // 'data' => new EventJoinResource($data)
        ];

        return response()->json($response_data, $this->successStatus);
    }

    public function addDevice($userid, $token)
    {
        //$data = UserDevices::findOrFail($request->group_id);
        $data = new UserDevices;
        $data->user_id = $userid;
        $data->registerid = $token;
        $data->save();


        //$Success = false;

        if($data){
            $response_data = [
                'success' => 1,
                'message' => 'Device Add Successfully',
            ];
            //$Success = true;
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            //$Success = false;

        }
        return response()->json($response_data,  $this->successStatus);
        //return $Success;

    }

    public function removeDevice(Request $request)
    {
        $affectedRows = UserDevices::where('user_id', '=', $request->user_id)->where('registerid', '=', $request->token)->delete();

        //$Success = false;
        if($affectedRows > 0){
            $response_data = [
                'success' => 1,
                'message' => 'Device Remove Successfully',
            ];
            //$Success = true;
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            //$Success = false;

        }
        return response()->json($response_data,  $this->successStatus);
        //return $Success;
    }


    public function sendEmailReminder()
    {
        $data = User::find(163);

        $code ='sdsdsd';
        $mail = Mail::send('emails.verify', [
            'data' => $data,
            'code' => $code,
        ], function($message) use ($data){

            $message->to('postmaster@mg.evngo.com.au');
            $message->subject("Hello  verify your account");

        });
    }


}
