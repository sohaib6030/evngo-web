<?php

namespace App\Http\Controllers\API;

use App\Helpers\Mail\Exception;
use App\Http\Helpers\Helper;
use App\Http\Resources\CitiesCollection;
use App\Http\Resources\EventTitleResource;
use App\Http\Resources\EventVenueResource;
use App\models\UserType;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventPaymentTypeCollection;
use App\Http\Resources\EventResource;
use App\Http\Resources\EventTicketTypeCollection;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\ServiceResource;
use App\Http\Resources\GroupCollection;
use App\Http\Resources\EventCommentCollection;
use App\Http\Resources\EventCommentResource;
use App\Http\Resources\EventJoinResource;
use App\Http\Resources\EventJoinCollection;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\NotificationResource;
use App\Http\Resources\EventJoinReceviedRequestResource;
use App\Http\Resources\EventJoinReceviedRequestCollection;
use App\models\Event;
use App\models\EventPaymentType;
use App\models\EventTicket;
use App\models\EventTicketType;
use App\models\Cities;
use App\models\EventType;
use App\models\Service;
use App\models\UserService;
use App\models\TitleVenue_Model;
use App\models\EventService;
use App\models\EventAction;
use App\models\Group;
use App\models\EventShare;
use App\models\EventComment;
use App\models\EventJoin;
use App\models\EventAttendance;
use App\models\GroupMember;
use App\models\UserDevices;
use App\models\Notifications;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use URL;
use Validator;
use function GuzzleHttp\json_encode;
use App\Http\Resources\EventCollection;
use App\Http\Resources\EventTypeCollection;
use App\Http\Resources\ServiceCollection;
use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;

class EventController extends Controller
{
    public $successStatus = 200;



    public function notification(Request $request)
    {

        $deviceToken = 'de1b362daf08670fbad0d57e9844e3b5e8af4aace3f4b8ae17a0c1f59367e19b';


        // Put your private key’s passphrase here:

        $passphrase = '123';


        // Put your alert message here:

        $message = 'A push notification has been sent!';

        $pem = __DIR__.'/DisttriCertificates.pem';
        $ctx = stream_context_create();

        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);

        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);


        // Open a connection to the APNS server

        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
        dd($fp);
        if (!$fp){

            exit('Failed to connect: $err $errstr' . PHP_EOL);


        }


        echo 'Connected to APNS' . '<br/>';


        // Create the payload body

        $body['aps'] = array(

            'alert' => array(

                'body' => $message,

            ),

            'type' => ‘sky_load’,

            'badge' => 1,

            'sound' => 'default',

            'mutable-content' => '1',

            'image_url' => 'image-url',

        );


        // Encode the payload as JSON

        $payload = json_encode($body);


        echo($payload);


        // Build the binary notification

        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;


        // Send it to the server

        $result = fwrite($fp, $msg, strlen($msg));


        if (!$result)

            echo 'Message not delivered' . PHP_EOL;

        else

            echo 'Message successfully delivered' . PHP_EOL;


        // Close the connection to the server

        fclose($fp);




    }



    //// for validating user the attendence after qr scanned
    public function eventQrValidate(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'id'  => 'required',
            'qr_code'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }

        $data = EventJoin::where('event_id',$request->id)->where('qr_code_md5',$request->qr_code)->where('status','Active')->where('is_deleted','0')->first();
        if($data){



            $response_data = [
                'success' => 1,
                'message' => 'Validated successfully.',
                'data' => new EventJoinResource($data)
            ];

        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'QR code does not match.'
            ];
        }

        return response()->json($response_data, $this->successStatus);
    }





    //// for chaging the attendence after qr scanned
    public function eventQrScan(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'id'  => 'required',
            'qr_code'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }

        $data = EventJoin::where('event_id',$request->id)->where('qr_code_md5',$request->qr_code)->where('status','Active')->where('is_deleted','0')->first();
        if($data)
        {
            $data->scaned_user_id = $user->id;
            $data->attanded = 'Present';
            $data->save();
            $body = '';  // for notification


            if($data){
                $attend = EventAttendance::where('event_id',$data->event_id)->where('user_id',$data->user_id)->latest('id')->first();
                if($attend){
                    if($attend->type=='IN'){
                        $type = 'OUT';
                        $body = 'Pass Out Accepted ' . $data->event->title;
                    }else{
                        $type = 'IN';
                        $body = 'Pass In Accepted ' . $data->event->title;
                    }
                }else{
                    $type = 'IN';
                    $body = 'Pass In Accepted ' . $data->event->title;
                }
                $attendance = new EventAttendance;
                $attendance->event_id = $data->event_id;
                $attendance->event_join_id = $data->id;
                $attendance->user_id = $data->user_id;
                $attendance->type = $type;
                $attendance->save();


                ////////// sending notification to  attendee devices /////////////

                $recipients = [];
                $recipients = UserDevices::where('user_id',$data->user_id)->pluck('registerid')->toArray();
                if(count($recipients) > 0){
                    $result=fcm()->to($recipients) // $recipients must an array
                    ->data([
                            'content_available' => true,
                            'title' => 'Evngo Event Attend',
                            'body' => $body,
                            'notification_type' => 'event_attendence_notification',
                            'event_id' => $data->event_id
                        ]
                    )
                        ->notification([
                            'sound' => 'default',
                            'content_available' => true,
                            'title' => 'Evngo Event Attend',
                            'body' =>   $body,
                            'channelid' => 'event_attendence_notification',
                            'notification_type' => 'event_attendence_notification',
                            'event_id' => $data->event_id    // sohaib addition

                        ])
                        ->send();
                }



                ///////////////////////////////////////////////////////////////////////


            }


            ///
            ///
            $response_data = [
                'success' => 1,
                'message' => 'Present successfully.',
                'data' => new EventJoinResource($data)
            ];

        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'QR code does not match.'
            ];
        }

        return response()->json($response_data, $this->successStatus);
    }



    //// for chaging the attendence for all attendee
    public function eventQrValidateAll(Request $request)
    {
        try {


            $user = Auth::user();

            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);
            if ($validator->fails()) {
                $response_data = [
                    'success' => 0,
                    'message' => 'Validation error.',
                    'data' => $validator->errors()
                ];
                return response()->json($response_data, $this->successStatus);
            }


            $event = Event::where('id', $request->id)->first();

            $event->attendence = 'in';

            $event->save();

            $data = EventJoin::where('event_id', $request->id)->where('status', 'Active')->where('is_deleted', '0')->get();

            if ($data->isEmpty())
            {
                $response_data = [
                    'success' => 1,
                    'message' => 'All Arrived successfully.'
                    //'data' => new EventJoinResource($data)
                ];

                return response()->json($response_data, $this->successStatus);
            }




            /////////////////   updating present value to all event  joins
            DB::table('event_join')
                ->where('event_id', $request->id)
                ->where('status', 'Active')
                ->where('is_deleted', '0')
                ->where('attanded', '!=' , 'Present')
                ->update([
                    'scaned_user_id' => $user->id,
                    'attanded' => 'Present',
                ]);

            $userIDS = [];
            /////// updating attendence   /////////
            foreach ($data as $usr) {

                if ($usr) {

                    $attendence = EventAttendance::
                         where('event_id', $usr->event_id)
                        ->where('user_id', $usr->user_id)
                       // ->where('status', 'Active')
                       // ->where('is_deleted', '0')
                        ->orderBy('created_at', 'desc')->first();


                    if( ($attendence == null) ||  ($attendence->type != 'IN')) {
                        $type = 'IN';
                        $attendance = new EventAttendance;
                        $attendance->event_id = $usr->event_id;
                        $attendance->event_join_id = $usr->id;
                        $attendance->user_id = $usr->user_id;
                        $attendance->type = $type;
                        $attendance->save();
                        array_push($userIDS, $usr->user_id);
                    }
                }
            }


            ////////// sending notification to  attendee devices /////////////
            $body = 'Pass In Accepted ' . $data->first()->event->title;
            $recipients = [];
            $recipients = UserDevices::whereIn('user_id', $userIDS)->pluck('registerid')->toArray();
            if (count($recipients) > 0) {
                $result = fcm()->to($recipients)// $recipients must an array
                ->data([
                        'content_available' => true,
                        'title' => 'Evngo Event Attend',
                        'body' => $body,
                        'notification_type' => 'event_attendence_notification',
                        'event_id' => $data->first()->event_id
                    ]
                )
                    ->notification([
                        'sound' => 'default',
                        'content_available' => true,
                        'title' => 'Evngo Event Attend',
                        'body' => $body,
                        'channelid' => 'event_attendence_notification',
                        'notification_type' => 'event_attendence_notification',
                        'event_id' => $data->first()->event_id    // sohaib addition

                    ])
                    ->send();
            }


            ///////////////////////////////////////////////////////////////////////


            $response_data = [
                'success' => 1,
                'message' => 'All Arrived successfully.'
                //'data' => new EventJoinResource($data)
            ];
            return response()->json($response_data, $this->successStatus);
        }
        catch (Exception $e)
        {
            $response_data = [
                'success' => 0,
                'message' => 'An Error Occured.'
                //'data' => new EventJoinResource($data)
            ];
            return response()->json($response_data, $this->successStatus);
        }


    }




    //// for chaging the attendence for all attendee
    public function eventQrValidateClose(Request $request)
    {
        try {


            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            if ($validator->fails()) {
                $response_data = [
                    'success' => 0,
                    'message' => 'Validation error.',
                    'data' => $validator->errors()
                ];
                return response()->json($response_data, $this->successStatus);
            }

            $event = Event::where('id', $request->id)->first();
            $event->attendence = 'out';
            $event->save();

            $data = EventJoin::where('event_id', $request->id)->where('status', 'Active')->where('is_deleted', '0')->get();

            if ($data->isEmpty())
            {
                $response_data = [
                    'success' => 1,
                    'message' => ' All Pass Out successfully.'
                    //'data' => new EventJoinResource($data)
                ];
                return response()->json($response_data, $this->successStatus);
            }




            /////////////////   updating present value to all event  joins
            DB::table('event_join')
                ->where('event_id', $request->id)
                ->where('status', 'Active')
                ->where('is_deleted', '0')
                ->where('attanded', '!=' , 'Present')
                ->update([
                    'scaned_user_id' => $user->id,
                    'attanded' => 'Present',
                ]);

            $userIDS = [];
            /////// updating attendence   /////////
            foreach ($data as $usr) {

                if ($usr) {

                    $attendence = EventAttendance::
                    where('event_id', $usr->event_id)
                        ->where('user_id', $usr->user_id)
                       // ->where('status', 'Active')
                       // ->where('is_deleted', '0')
                        ->orderBy('created_at', 'desc')->first();


                    if( ($attendence != null) &&  ($attendence->type == 'IN')) {

                        $type = 'OUT';
                        $attendance = new EventAttendance;
                        $attendance->event_id = $usr->event_id;
                        $attendance->event_join_id = $usr->id;
                        $attendance->user_id = $usr->user_id;
                        $attendance->type = $type;
                        $attendance->save();
                        array_push($userIDS, $usr->user_id);
                    }
                }
            }


            ////////// sending notification to  attendee devices /////////////
            $body = 'Pass Out Accepted ' . $data->first()->event->title;
            $recipients = [];
            $recipients = UserDevices::whereIn('user_id', $userIDS)->pluck('registerid')->toArray();
            if (count($recipients) > 0) {
                $result = fcm()->to($recipients)// $recipients must an array
                ->data([
                        'content_available' => true,
                        'title' => 'Evngo Event Attend',
                        'body' => $body,
                        'notification_type' => 'event_attendence_notification',
                        'event_id' => $data->first()->event_id
                    ]
                )
                    ->notification([
                        'sound' => 'default',
                        'content_available' => true,
                        'title' => 'Evngo Event Attend',
                        'body' => $body,
                        'channelid' => 'event_attendence_notification',
                        'notification_type' => 'event_attendence_notification',
                        'event_id' => $data->first()->event_id    // sohaib addition

                    ])
                    ->send();
            }


            ///////////////////////////////////////////////////////////////////////


            $response_data = [
                'success' => 1,
                'message' => 'All Pass Out successfully.'
                //'data' => new EventJoinResource($data)
            ];
            return response()->json($response_data, $this->successStatus);
        }
        catch (Exception $e)
        {
            $response_data = [
                'success' => 0,
                'message' => 'An Error Occured.'
                //'data' => new EventJoinResource($data)
            ];
            return response()->json($response_data, $this->successStatus);
        }


    }






    public function eventJoinlist(Request $request)
    {
        $data = EventJoin::where('event_id',$request->id)->where('status','Active')->where('is_deleted','0')->orderBy('id','desc')->get();

        if(count($data)>0){

            return new EventJoinCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }



    public function eventJoin(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'event_id'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        $join = EventJoin::where('event_id',$request->event_id)->where('user_id',$user->id)->where('is_deleted','0')->first();
        if($join){
            $response_data = [
                'success' => 1,
                'message' => 'Event already joined.',
                'data' => new EventJoinResource($join)
            ];
        }else{
            $qr_code_md5 = encrypt($user->id.','.$request->event_id);
            $file = 'generated_qrcode/'.$user->id.$request->event_id.'.png';

            \QrCode::size(500)
                ->margin(0)->format('png')
                ->generate($qr_code_md5, public_path($file));

            $event   = EventShare::where('event_id',$request->event_id)->first();
            if($event==null){
                $response_data = [
                    'success' => 0,
                    'message' => 'Event not found.',
                    'data' => null
                ];
                return response()->json($response_data,  $this->successStatus);
            }
            //Comment By Sohaib Ahmed Siddique 24-Feb-2020
            /*
            //dd($event);
            if($event->event_show=='Public'){
                $data = new EventJoin;
                $data->event_id = $request->event_id;
                $data->user_id = $user->id;
                $data->qr_code = $file;
                $data->qr_code_md5 = $qr_code_md5;
                $data->status = 'Pending';
                $data->attanded = 'Absent';
                $data->save();

                // for notification
                $recipients =[];
                if($event->user->firebase_token){
                    $recipients[] = $event->user->firebase_token;
                }
                if($event->user->devices){
                    foreach($event->user->devices as $device){
                        $recipients[] = $device->registerid;
                    }
                }

                if(count($recipients) > 0){
                    $result=fcm()->to($recipients) // $recipients must an array
                    ->data([
                        'content_available' => true,
                        'title' => 'Evengo Event Join',
                        'body' => $data->user->fname.' '.$data->user->lname.' request to join event ' . $data->event->title,
                        'notification_type' => 'event_join_notification',
                      ]
                    )
                    ->notification([
                        'sound' => 'default',
                        'content_available' => true,
                        'title' => 'Evengo Event Join',
                        'body' => $data->user->fname.' '.$data->user->lname.' request to join event ' . $data->event->title,
                        'channelid' => 'event_join_notification',
                        'notification_type' => 'event_join_notification',
                    ])
                    // ->notification([
                    //     'title' => $data->user->fname.' '.$data->user->lname.' request to join event ' . $data->event->title,
                    //      'body' => '',
                    // ])
                    ->send();
                }

                // for notification
                $response_data = [
                    'success' => 1,
                    'message' => 'Request successfully sent.',
                    'data' => new EventJoinResource($data)
                ];

            }else{
            */

            $data = new EventJoin;
            $data->event_id = $request->event_id;
            $data->user_id = $user->id;
            $data->qr_code = $file;
            $data->qr_code_md5 = $qr_code_md5;
            $data->status = 'Active';
            $data->attanded = 'Absent';
            $data->save();
            if($data){
                $totalJoinUser =  EventJoin::where('event_id',$data->event_id)->count();
                $total_join   = Event::where('id',$data->event_id)->update(['total_join'=>$totalJoinUser]);
            }

            $selecetedevent = Event::where('id',$data->event_id)->first();


            Helper::sendQR($user,$selecetedevent,$file);

            $response_data = [
                'success' => 1,
                'message' => 'Event join successfully.',
                'data' => new EventJoinResource($data)
            ];
            //}

        }
        return response()->json($response_data, $this->successStatus);
    }

    public function eventJoinReceviedRequest(Request $request)
    {
        $user = Auth::user();
        //////// new logic by sohaib  /////////////
        ///
        $data = Notifications::where('user_id',$user->id)
            ->orWhere('user_id',0)
            ->where('type_id',1)
            ->where('status','active')
            ->where('is_deleted','0')
             ->where('created_at', '>=', Carbon::now()->subDay(1))
            ->orderBy('id', 'desc')->paginate(10);
            //->get();
        if(count($data)>0){
            return new NotificationCollection($data);
        }

        ///////////////////////////////////////////

     /*   $data = EventJoin::whereHas('event', function ($query) use ($user) {
            $query->where('created_by',$user->id);
        })
            ->where('status','Pending')->where('is_deleted','0')->orderBy('id', 'desc')->paginate(10);
        if(count($data)>0){
            return new EventJoinReceviedRequestCollection($data);
        }  */
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }


    public function eventAcceptRejectRequest(Request $request)
    {
        if($request->status=='Approved'){
            $data = EventJoin::findOrFail($request->id);
            $data->status = 'Active';
            $data->save();
            $message = 'Request successfully Approved.';
        }else if($request->status=='Rejected'){
            $data = EventJoin::findOrFail($request->id);
            $data->status = 'Rejected';
            $data->save();
            $message = 'Request successfully Rejected.';
        }
        if($data){
            $response_data = [
                'success' => 1,
                'message' => $message,
            ];
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

        }
        return response()->json($response_data,  $this->successStatus);

    }

    public function wrong_ype()
    {
        $branch = array();
        $data = EventType::where('status','Active')->where('is_deleted','0')->orderBy('id', 'desc')->get();

        foreach ($data as $element) {
            //dd($element->id);
            $children = Service::where('event_type_id',$element->id)->where('status','Active')->where('is_deleted','0')->orderBy('id', 'desc')->get();

            if (count($children)>0) {

                $branch[] = ['id'=>$element->id,'title'=>$element->title,'children'=> new ServiceCollection($children)];
            }else{

                $branch[] = ['id'=>$element->id,'title'=>$element->title];
            }
        }


        if(count($branch)>0){
            $response_data=[
                'success' => 1,
                'message' => 'Data Found.',
                'data' => $branch
            ];
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

        }

        return response()->json($response_data,  $this->successStatus);
    }

    public function eventType()
    {
        $data = EventType::where('status','Active')->where('is_deleted','0')->orderBy('id','desc')->get();

        if(count($data)>0){

            return new EventTypeCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }


    /// return event payment types like free,coupon and tickets
    /// generated by sohaib ahmed on 14 JAN 2020
    public function eventPaymentType()
    {
        $data = EventPaymentType::where('status','Active')->where('is_deleted','0')->orderBy('id','asc')->get();

        if(count($data)>0){

            return new EventPaymentTypeCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    /// return event ticket types like normal , vip etc
    /// generated by sohaib ahmed on 14 JAN 2020
    public function eventTicketType()
    {
        $data = EventTicketType::where('status','Active')->where('is_deleted','0')->orderBy('id','asc')->get();

        if(count($data)>0){

            return new EventTicketTypeCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function GetCities(Request $request)
    {
        //$CountryCode = $request->code;

        $CountryCode = ($request->code != null || $request->code != "" ? $request->code : "");
        $CountryName = ($request->countryname != null || $request->countryname != "" ? $request->countryname : "");
        $list="";
        $countrylist="";

        if($CountryCode == "")
        {
            $list = Cities::where('is_deleted','0')->pluck('CountryCode');
        }
        else
        {
            $list= Cities::where('is_deleted','0')->where('CountryCode',$CountryCode)->pluck('CountryCode');
        }

        if($CountryName == "")
        {
            $countrylist = Cities::where('is_deleted','0')->pluck('countryName');
        }
        else
        {
            $countrylist= Cities::where('is_deleted','0')->where('countryName',$CountryName)->pluck('countryName');
        }

        $data = Cities::where('status','Active')->where('is_deleted','0')->whereIn('CountryCode',$list)->whereIn('countryName',$countrylist)->orderBy('id','asc')->get();

        if(count($data)>0){

            return new CitiesCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    public function GetTitleVenues()
    {
        $user = Auth::user();

        $title_data = Event::where('events.status','Active')
        ->where('events.is_deleted','0')
        ->join('event_shares', 'events.id', '=', 'event_shares.event_id')
        ->select('events.title')
        ->orderBy('events.title', 'asc')
        ->get();

        $venue_data = Event::where('events.status','Active')
            ->where('events.is_deleted','0')
            ->join('event_shares', 'events.id', '=', 'event_shares.event_id')
            ->select('events.venue')
            ->orderBy('events.venue', 'asc')
            ->get();

        $title = $title_data->unique('title');
        $venue = $venue_data->unique('venue');

        $response_data=[
            'success' => 1,
            'message' => 'success.',
            'title_list' => EventTitleResource::collection($title),
            'venue_list' => EventVenueResource::collection($venue)
        ];

        if($response_data){
            return response()->json($response_data, $this->successStatus);
        }

        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

            return response()->json($response_data,  $this->successStatus);
        }
    }


    public function servicesGetByType(Request $request)
    {
        $data = Service::where('event_type_id',$request->id)->where('status','Active')->where('is_deleted','0')->orderBy('id', 'desc')->get();

        if(count($data)>0){
            return new ServiceCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }



    public function userServicesStore(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'event_type_id'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }

        $type = User::findOrFail($user->id);
        $type->event_type_id = $request->event_type_id;
        $type->save();
        if($type){
            if(isset($request->services) && ($request->services !=""))
            {

                $servicesUniq = json_decode($request->services);
                foreach ($servicesUniq as $value) {
                    $findService = UserService::where('service_provider_id',$user->id)->where('service_id',$value)->first();
                    if($findService){
                        $findService->service_provider_id =   $user->id;
                        $findService->service_id =   $value;
                        $findService->status = 'Active';
                        $findService->created_by = $user->id;
                        $findService->save();
                    }else{
                        $data = new UserService;
                        $data->service_provider_id = $user->id;
                        $data->service_id = $value;
                        $data->status = 'Active';
                        $data->created_by = $user->id;
                        $data->save();
                    }

                }
            }

            $response_data = [
                'success' => 1,
                'message' => 'Services saved successfully.',
                // 'data' => new UserResource(Auth::user())
            ];
        }


        return response()->json($response_data, $this->successStatus);
    }






    public function selectGroup(Request $request)
    {
        $user = Auth::user();
        $search = $request->search;
        if(isset($search) && ($search !="") )
        {
            $data = Group::where(function ($query) use ($user, $search) {
                //print_r($search);exit;
                // $query->WhereHas('members', function($q) use ($user) {
                //     $q->where(function($q) use ($user) {
                //         $q->where('user_id',$user->id);
                //         $q->where('status','Approved');
                //     });
                // });

                $query->orwhere(function($query) use ($search) {
                    $query->orwhere('name', 'LIKE', '%' . $search . '%');
                });

            })
                ->where('is_deleted',0)->orderBy('id', 'desc')->paginate(10);
        }else{
            $data = Group::whereHas('members', function ($query) use ($user) {
                $query->where('user_id',$user->id);
                $query->where('status','Approved');
            })
                ->where('is_deleted',0)->orderBy('id', 'desc')->paginate(10);
        }
        if(count($data)>0){
            return new GroupCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }


    public function selectUser(Request $request)
    {
        $user = Auth::user();
        $search = $request->search;
        if(isset($search) && ($search !="") )
        {
            $data = User::where(function ($query) use ($search) {


                $query->orwhere(function($query) use ($search) {
                    $query->orwhere('fname', 'LIKE', '%' . $search . '%');
                    $query->orwhere('lname', 'LIKE', '%' . $search . '%');
                });

            })
                ->where('status',1)->where('id','!=',$user->id)->where('role_id',2)->orderBy('id', 'desc')->paginate(10);
            //$data = User::where('role_id',2)->where('status',1)->orderBy('id', 'desc')->paginate(10);
        }else{
            $data = User::where('id','!=',$user->id)->where('role_id',2)->where('status',1)->orderBy('id', 'desc')->paginate(10);
        }
        if(count($data)>0){

            return new UserCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }

    }


    public function shareToGroup(Request $request)
    {
        // Log::info($request->all());
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'event_id'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        if(isset($request->group_id) && ($request->group_id !=""))
        {

            $group_ids = json_decode($request->group_id);
            foreach ($group_ids as $value) {

                $groupmembers = GroupMember::where('group_id',$value)->get();
                //$allmem = groupMembers($value);

                foreach ($groupmembers as $groupvalues)
                {
                    $id = $groupvalues->user_id;
                    $data =  EventShare::where('group_id',$value)->where('event_id',$request->event_id)->where('user_id', $id)->first();

                    if($data){
                        $data->event_id =   $request->event_id;
                        $data->group_id =   $value;
                        $data->user_id =   $id;
                        $data->type = 'User';
                        $data->status = 'Active';
                        $data->created_by = $user->id;
                        $data->save();
                    }else{
                        $data = new EventShare;
                        $data->event_id =   $request->event_id;
                        $data->group_id =   $value;
                        $data->user_id =   $id;
                        $data->type = 'User';
                        $data->status = 'Active';
                        $data->created_by = $user->id;
                        $data->save();
                    }


                    $alreadyMember = GroupMember::where('group_id',$value)->where('user_id',$id)->first();

                    $notification  = new Notifications;
                    $notification->type_id = 1;
                    $notification->user_id = $id;
                    $notification->event_id = $data->event_id;
                    $notification->title = 'Evngo Group Event Share';
                    $notification->description = $alreadyMember->user->fname.' '.$alreadyMember->user->lname.' share in group event ' . $alreadyMember->group->name;
                    $notification->status = 'active';
                    $notification->created_by = $user->id;
                    $notification->save();





                    if ($alreadyMember)
                    {
                        $recipients =[];
                        if($alreadyMember->member->firebase_token){
                            $recipients[] = $alreadyMember->member->firebase_token;
                        }
                        if($alreadyMember->member->devices){
                            foreach($alreadyMember->member->devices as $device){
                                $recipients[] = $device->registerid;
                            }
                        }

                        if(count($recipients) > 0){
                            $result=fcm()->to($recipients) // $recipients must an array
                            ->data([
                                    'content_available' => true,
                                    'title' => 'Evngo Group Event Share',
                                    'body' => $alreadyMember->user->fname.' '.$alreadyMember->user->lname.' share in group event ' . $alreadyMember->group->name,
                                    'notification_type' => 'event_share_notification',
                                    'event_id' => $data->event_id
                                ]
                            )
                                ->notification([
                                    'sound' => 'default',
                                    'content_available' => true,
                                    'title' => 'Evngo Group Event Share',
                                    'body' => $alreadyMember->user->fname.' '.$alreadyMember->user->lname.' share in group event ' . $alreadyMember->group->name,
                                    'channelid' => 'event_share_notification',
                                    'notification_type' => 'event_share_notification',
                                    'event_id' => $data->event_id    // sohaib addition

                                ])

                                ->send();
                        }

                    }
                }

                /*
                                $recipients =[];
                                if($data->sharedWith->firebase_token){
                                    $recipients[] = $data->sharedWith->firebase_token;
                                }
                                if($data->sharedWith->devices){
                                    foreach($data->sharedWith->devices as $device){
                                        $recipients[] = $device->registerid;
                                    }
                                }


                                if(count($recipients) > 0){
                                    $result=fcm()->to($recipients) // $recipients must an array
                                    ->data([
                                            'title' => 'Evengo Event Share',
                                            'body' => $data->user->fname.' '.$data->user->lname.' share with you event ' . $data->allevent->name,
                                            'notification_type' => 'event_share_notification',
                                        ]
                                    )

                                        ->send();
                                }
                */







            }
        }
        if($data){
            $response_data = [
                'success' => 1,
                'message' => 'Shared successfully.',
            ];
        }else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

        }
        return response()->json($response_data,  $this->successStatus);

    }


    public function groupMembers($val)
    {
        $data = GroupMember::where('group_id',$val)->where('status','Approved')->where('is_deleted',0)->orderBy('id', 'desc')->paginate(10);

        if(count($data)>0){
            return new GroupMemberCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }


    public function shareToUser(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'event_id'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        if(isset($request->user_id) && ($request->user_id !=""))
        {

            $user_ids = json_decode($request->user_id);
            foreach ($user_ids as $value) {
                $data =  EventShare::where('user_id',$value)->where('event_id',$request->event_id)->first();
                if($data){
                    $data->event_id =   $request->event_id;
                    $data->user_id =   $value;
                    $data->type = 'User';
                    $data->status = 'Active';
                    $data->created_by = $user->id;
                    $data->save();
                }else{
                    $data = new EventShare;
                    $data->event_id =   $request->event_id;
                    $data->user_id =   $value;
                    $data->type = 'User';
                    $data->status = 'Active';
                    $data->created_by = $user->id;
                    $data->save();
                }

                $notification  = new Notifications;
                $notification->type_id = 1;
                $notification->user_id = $value;
                $notification->event_id = $data->event_id;
                $notification->title = 'Evngo Event Share';
                $notification->description = $data->user->fname.' '.$data->user->lname.' share with you event ' . $data->allevent->title;
                $notification->status = 'active';
                $notification->created_by = $user->id;
                $notification->save();

                //dd($data->sharedWith->devices);
                // for notification
                $recipients =[];
                if($data->sharedWith->firebase_token){
                    $recipients[] = $data->sharedWith->firebase_token;
                }
                if($data->sharedWith->devices){
                    foreach($data->sharedWith->devices as $device){
                        $recipients[] = $device->registerid;
                    }
                }


                if(count($recipients) > 0){
                    $result=fcm()->to($recipients) // $recipients must an array
                    /*
                    ->data([
                            'content_available' => true,
                            'title' => 'Evengo Event Share',
                            'body' => $data->user->fname.' '.$data->user->lname.' share with you event ' . $data->allevent->title,
                            'notification_type' => 'event_share_notification',
                      ]
                    )
                    */

                    ->data([
                        'content_available' => true,
                        'title' => 'Evngo Event Share',
                        'body' => $data->user->fname.' '.$data->user->lname.' share with you event ' . $data->allevent->title,
                        'notification_type' => 'event_share_notification',
                        'event_id' => $data->event_id
                    ])

                        ->notification([
                            'sound' => 'default',
                            'content_available' => true,
                            'title' => 'Evngo Event Share',
                            'body' => $data->user->fname.' '.$data->user->lname.' share with you event ' . $data->allevent->title,
                            'channelid' => 'event_share_notification',
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->event_id // sohaib addition
                        ])

                        /*
                                            ->notification([
                                                //'sound' => 'default',
                                                'content_available' => true,
                                                'title' => 'Evengo Event Share',
                                                'body' => $data->user->fname.' '.$data->user->lname.' share with you event ' . $data->allevent->title,
                                                'notification_type' => 'event_share_notification',
                                            ])
                        */
                        /*
                                            ->data([
                                                'content_available' => true,
                                                'title' => 'Evengo Event Share',
                                                'body' => $data->user->fname.' '.$data->user->lname.' share with you event ' . $data->allevent->title,
                                                'notification_type' => 'event_share_notification',
                                            ])
                        */
                        // ->notification([
                        //     'title' => $data->user->fname.' '.$data->user->lname.' share with you event ' . $data->allevent->title,
                        //      'body' => '',
                        // ])
                        ->send();
                }
                // for notification

            }
        }

        if($data){
            $response_data = [
                'success' => 1,
                'message' => 'Shared successfully.',
            ];
        }else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

        }
        return response()->json($response_data,  $this->successStatus);

    }

    public function allEvent(Request $request)
    {

        $user = Auth::user();
        $search = $request->search;
        if(isset($search) && ($search !="") )
        {

            $data = EventShare::with('allevent')->whereHas('allevent',function($q) use ($search) {

                $q->where('title','LIKE', '%' . $search . '%');
            })->where('status','Active')->where('event_show','Public')->where('is_deleted','0')
                ->where('type','Admin')->orderBy('id', 'desc')->paginate(10);

        }else{
            $data = EventShare::where('status','Active')->where('event_show','Public')->where('is_deleted','0')->where('type','Admin')->orderBy('id', 'desc');

            if($request->has('event') && is_numeric($request->query('event')) && $request->query('event') > 0){
                $data = $data->where('id',$request->query('event'));
            }

            if($request->has('limit') && is_numeric($request->query('limit')) && $request->query('limit') > 0){
                $data = $data->paginate($request->query('limit'));
            }else{
                $data = $data->paginate(10);
            }

        }


        // $data = EventShare::whereHas('event', function ($query) use ($user) {
        //         $query->where('status','Active');
        //         $query->where('is_deleted','0');
        // })
        // ->where('is_deleted',0)->orderBy('id', 'desc')->paginate(10);
        // dd($data);
        if(count($data)>0){
            return new EventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }



    public function myEvent()
    {
        $user = Auth::user();
        $data = EventShare::where('status','Active')->where('user_id',$user->id)->where('is_deleted','0')->orderBy('id', 'desc')->paginate(10);
        //print_r($data->toArray());exit;
        if(count($data)>0){
            return new EventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }



    public function serviceProviderGetByType(Request $request)
    {
        $data = User::where('event_type_id',$request->id)->where('status',1)->where('verify','1')->orderBy('id', 'desc')->get();

        if(count($data)>0){
            return new UserCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }


    public function servicesGetByServiceProvider(Request $request)
    {
        $service_id = UserService::where('service_provider_id',$request->id)->where('status','Active')->where('is_deleted','0')->orderBy('id', 'desc')->pluck('service_id');
        $data = Service::whereIN('id',$service_id)->orderBy('id', 'desc')->get();

        if(count($data)>0){
            return new ServiceCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }



    public function eventCreate(Request $request)
    {
        Log::info($request->all());
        $notificationsend_type = $request->notificationsend_type;


        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'event_type_id'  => 'required',
            // 'event_payment_id'  => 'required',
            'title'  => 'required',
            'description'  => 'required',
            'venue'  => 'required',
            'venue_address'  => 'required',
            'event_date'  => 'required',
            'event_time'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        if(isset($request->edit_id) && ($request->edit_id !="") && ($request->edit_id !='0') )
        {
            //echo "yes" ; exit;
            //dd($request->all());

            $data = Event::findOrFail($request->edit_id);
            $data->created_by  = $user->id;
            $data->event_type_id  = $request->event_type_id;
            $data->event_payment_id  = $request->event_payment_id;
            $data->service_provider_id  = $request->service_provider_id;
            $data->title  = $request->title;
            $data->description  = $request->description;
            $data->venue  = $request->venue;
            $data->venue_address  = $request->venue_address;
            $data->event_date  = date("Y-m-d", strtotime($request->event_date));
            $data->event_time  = $request->event_time;
            $data->event_show  = $request->event_show;
            $data->Country  = $request->country;
            $data->City  = $request->city;

            $address = $request->venue_address; // Google HQ
            $prepAddr = str_replace(' ','+',$address);
            $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?key=AIzaSyCUqumJmcf9gVRi5VDakoody8q59zw_IS4&address='.$prepAddr.'&sensor=false');
            $output= json_decode($geocode);
            //print_r($output);exit;
            if(isset($output->results[0]) && $output->results[0]!=""){
                $lat = $output->results[0]->geometry->location->lat;
                $long = $output->results[0]->geometry->location->lng;
                $data->lat  = $lat;
                $data->lon  = $long;
            }

            $data->status  = 'Active';
            
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $image = $file->getClientOriginalName();
                $pathTosave = Str::random(20) . ".png";
                $path = Storage::disk('local')->put('/public/event/' . $pathTosave  , File::get($file));
                $qpath = $pathTosave;
                if ($request->hasfile('video')) {
                $file = $request->file('video');
                $image = $file->getClientOriginalName();
                $pathTosavevid =  "AVDURLDATA=" . Str::random(20) . ".mp4";
                $qpath =  $pathTosave . $pathTosavevid;
                $path = Storage::disk('local')->put('/public/event/' . $pathTosavevid  , File::get($file));
              
            }
                
                
                $data->image = $qpath;
            }
            $data->save();

            /// send notification test
            if ($data->event_show == "Public" || $notificationsend_type == "all")
            {
                // $first = array( "ddfd", "fdfdfd");



                $recipients = [];
                $recipients = UserDevices::pluck('registerid')->toArray();
                // dd($recipients) ;

                // $recipients =   UserDevices::select('registerid')->get()->toArray();
                //dd($recipients)      ;
                if(count($recipients) > 0){
                    $result=fcm()->to($recipients) // $recipients must an array
                    ->data([
                            'content_available' => true,
                            'title' => 'Evngo New Event Share',
                            'body' => $user->fname.' '.$user->lname.' created new event ' . $data->title,
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->id
                        ]
                    )
                        ->notification([
                            'sound' => 'default',
                            'content_available' => true,
                            'title' => 'Evngo Event Share',
                            'body' => $user->fname.' '.$user->lname.' updated event ' . $data->title,
                            'channelid' => 'event_share_notification',
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->id    // sohaib addition

                        ])

                        ->send();

                    $notification  = new Notifications;
                    $notification->type_id = 1;
                    $notification->user_id = 0;
                    $notification->event_id = $data->id;
                    $notification->title = 'Evngo Event Share';
                    $notification->description = $user->fname.' '.$user->lname.' updated event ' . $data->title;
                    $notification->status = 'active';
                    $notification->created_by = $user->id;
                    $notification->save();

                }



            }
            else if($notificationsend_type == "vip")
            {
                $vip_members = UserType::where('status','Active')->where('type','vip')->distinct('user_id')->pluck('user_id');

                $recipients = [];
                $recipients = UserDevices::whereIn('user_id',$vip_members)->pluck('registerid')->toArray();

                if(count($recipients) > 0){
                    $result=fcm()->to($recipients) // $recipients must an array
                    ->data([
                            'content_available' => true,
                            'title' => 'Evngo New Event Share',
                            'body' => $user->fname.' '.$user->lname.' created new event ' . $data->title,
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->id
                        ]
                    )
                        ->notification([
                            'sound' => 'default',
                            'content_available' => true,
                            'title' => 'Evngo Event Share',
                            'body' => $user->fname.' '.$user->lname.' updated event ' . $data->title,
                            'channelid' => 'event_share_notification',
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->id    // sohaib addition

                        ])

                        ->send();

                    $notification  = new Notifications;
                    $notification->type_id = 1;
                    $notification->user_id = 0;
                    $notification->event_id = $data->id;
                    $notification->title = 'Evngo Event Share';
                    $notification->description = $user->fname.' '.$user->lname.' updated event ' . $data->title;
                    $notification->status = 'active';
                    $notification->created_by = $user->id;
                    $notification->save();

                }




            }

            if($data){
                if(isset($request->services) && ($request->services !=""))
                {
                    $servicesUniq = json_decode($request->services);
                    foreach ($servicesUniq as $value) {
                        $findService = EventService::where('event_id',$data->id)->where('service_id',$value)->first();
                        if($findService){
                            $findService->event_id =   $data->id;
                            $findService->service_id =   $value;
                            $findService->status = 'Active';
                            $findService->created_by = $user->id;
                            $findService->save();
                        }else{
                            $eventService = new EventService;
                            $eventService->event_id = $data->id;
                            $eventService->service_id = $value;
                            $eventService->status = 'Active';
                            $eventService->created_by = $user->id;
                            $eventService->save();
                        }
                    }
                }
            }

            if($data){
                $EventShare = EventShare::Where('event_id', $request->edit_id)->firstOrFail();
                $EventShare->event_show = $request->event_show;
                $EventShare->save();
            }


            $response_data = [
                'success' => 1,
                'message' => 'Event successfully created.',
                'data' => $data
            ];

        }else{
            // echo "no" ; exit;
            $data = new Event;
            $data->created_by  = $user->id;
            $data->event_type_id  = $request->event_type_id;
            $data->event_payment_id =  $request->event_payment_id;
            $data->service_provider_id  = $request->service_provider_id;
            $data->title  = $request->title;
            $data->description  = $request->description;
            $data->venue  = $request->venue;
            $data->venue_address  = $request->venue_address;
            $data->event_date  = date("Y-m-d", strtotime($request->event_date));
            $data->event_time  = $request->event_time;
            $data->event_show  = $request->event_show;
            $data->Country  = $request->country;
            $data->City  = $request->city;

            $address = $request->venue_address; // Google HQ
            $prepAddr = str_replace(' ','+',$address);
            $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?key=AIzaSyCUqumJmcf9gVRi5VDakoody8q59zw_IS4&address='.$prepAddr.'&sensor=false');
            $output= json_decode($geocode);
            //print_r($output);exit;
            if(isset($output->results[0]) && $output->results[0]!=""){
                $lat = $output->results[0]->geometry->location->lat;
                $long = $output->results[0]->geometry->location->lng;
                $data->lat  = $lat;
                $data->lon  = $long;
            }
            $data->status  = 'Active';
            
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $image = $file->getClientOriginalName();
                $pathTosave = Str::random(20) . ".png";
                $path = Storage::disk('local')->put('/public/event/' . $pathTosave  , File::get($file));
                $qpath = $pathTosave;
                if ($request->hasfile('video')) {
                $file = $request->file('video');
                $image = $file->getClientOriginalName();
                $pathTosavevid =  "AVDURLDATA=" . Str::random(20) . ".mp4";
                $qpath =  $pathTosave . $pathTosavevid;
                $path = Storage::disk('local')->put('/public/event/' . $pathTosavevid  , File::get($file));
              
            }
                
                
                $data->image = $qpath;
            }
            $data->save();

            /// send notification test
            if ($data->event_show == "Public" || $notificationsend_type == "all")
            {
                // $first = array( "ddfd", "fdfdfd");



                $recipients = [];
                $recipients = UserDevices::pluck('registerid')->toArray();
                // dd($recipients) ;

                // $recipients =   UserDevices::select('registerid')->get()->toArray();
                //dd($recipients)      ;
                if(count($recipients) > 0){
                    $result=fcm()->to($recipients) // $recipients must an array
                    ->data([
                            'content_available' => true,
                            'title' => 'Evngo New Event Share',
                            'body' => $user->fname.' '.$user->lname.' created new event ' . $data->title,
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->id
                        ]
                    )
                        ->notification([
                            'sound' => 'default',
                            'content_available' => true,
                            'title' => 'Evngo New Event Share',
                            'body' => $user->fname.' '.$user->lname.' created new event ' . $data->title,
                            'channelid' => 'event_share_notification',
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->id    // sohaib addition

                        ])

                        ->send();


                    $notification  = new Notifications;
                    $notification->type_id = 1;
                    $notification->user_id = 0;
                    $notification->event_id = $data->id;
                    $notification->title = 'Evngo New Event Share';
                    $notification->description = $user->fname.' '.$user->lname.' created new event ' . $data->title;
                    $notification->status = 'active';
                    $notification->created_by = $user->id;
                    $notification->save();
                }



            }
            else if ($notificationsend_type == "vip")
            {
                $vip_members = UserType::where('status','Active')->where('type','vip')->distinct('user_id')->pluck('user_id');

                $recipients = [];
                $recipients = UserDevices::whereIn('user_id',$vip_members)->pluck('registerid')->toArray();

                if(count($recipients) > 0){
                    $result=fcm()->to($recipients) // $recipients must an array
                    ->data([
                            'content_available' => true,
                            'title' => 'Evngo New Event Share',
                            'body' => $user->fname.' '.$user->lname.' created new event ' . $data->title,
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->id
                        ]
                    )
                        ->notification([
                            'sound' => 'default',
                            'content_available' => true,
                            'title' => 'Evngo New Event Share',
                            'body' => $user->fname.' '.$user->lname.' created new event ' . $data->title,
                            'channelid' => 'event_share_notification',
                            'notification_type' => 'event_share_notification',
                            'event_id' => $data->id    // sohaib addition

                        ])

                        ->send();


                    $notification  = new Notifications;
                    $notification->type_id = 1;
                    $notification->user_id = 0;
                    $notification->event_id = $data->id;
                    $notification->title = 'Evngo New Event Share';
                    $notification->description = $user->fname.' '.$user->lname.' created new event ' . $data->title;
                    $notification->status = 'active';
                    $notification->created_by = $user->id;
                    $notification->save();
                }
            }

            //////





            if($data){
                if(isset($request->services) && ($request->services !=""))
                {

                    $servicesUniq = json_decode($request->services);
                    foreach ($servicesUniq as $value) {
                        $findService = EventService::where('event_id',$data->id)->where('service_id',$value)->first();
                        if($findService){
                            $findService->event_id =   $data->id;
                            $findService->service_id =   $value;
                            $findService->status = 'Active';
                            $findService->created_by = $user->id;
                            $findService->save();
                        }else{
                            $eventService = new EventService;
                            $eventService->event_id = $data->id;
                            $eventService->service_id = $value;
                            $eventService->status = 'Active';
                            $eventService->created_by = $user->id;
                            $eventService->save();
                        }

                    }
                }
            }


            if($data){

                $EventShare = new EventShare;
                $EventShare->event_id =   $data->id;
                $EventShare->user_id =   $user->id;
                $EventShare->type = 'Admin';
                $EventShare->status = 'Active';
                $EventShare->event_show = $request->event_show;
                $EventShare->created_by = $user->id;
                $EventShare->save();
            }

            $response_data = [
                'success' => 1,
                'message' => 'Event successfully created.',
                'data' => $data
            ];



        }


        return response()->json($response_data, $this->successStatus);


    }


    public function edit(Request $request)
    {
        $data = Event::findOrFail($request->event_id);

        if($data){
            return new EventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }

    }


    public function eventDelete(Request $request)
    {
        //exit();
        //$data = EventShare::where('status','Active')->where('is_deleted','0')->orderBy('id', 'desc')->paginate(10);

        $data = EventShare::findOrFail($request->event_id);
        $data->is_deleted = 1;
        $data->save();
        if($data){
            $response_data = [
                'success' => 1,
                'message' => 'Event successfully deleted.',
            ];
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

        }
        return response()->json($response_data,  $this->successStatus);

    }


    public function eventLikeDislike(Request $request)
    {

        $user = Auth::user();
        $data =  EventAction::where('user_id',$user->id)->where('event_share_id',$request->event_id)->first();
        //$event = Event::findOrFail($request->event_id);
        if($data){
            $data->type = $request->type;
            $data->event_share_id = $request->event_id;
            $data->user_id = $user->id;
            $data->save();
            $message = $data->type;
        }else{
            $data = new EventAction;
            $data->type = $request->type;
            $data->event_share_id = $request->event_id;
            $data->user_id = $user->id;
            $data->status = 'Active';
            $data->save();
            $message =$data->type;
        }
        if($data){
            $response_data = [
                'success' => 1,
                'message' => $message,
            ];
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

        }
        return response()->json($response_data,  $this->successStatus);

    }




    public function eventComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_id'  => 'required',
            'comment'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        $user = Auth::user();
        $data = new EventComment;
        $data->event_share_id = $request->event_id;
        $data->user_id = $user->id;
        $data->created_by = $user->id;
        $data->comment = $request->comment;
        $data->status = 'Active';
        $data->save();
        ;
        if($data){
            $response_data = [
                'success' => 1,
                'message' => 'Comment successfully saved',
            ];
        }else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];

        }
        return response()->json($response_data,  $this->successStatus);

    }


    public function getComment(Request $request)
    {
        $event_id = $request->event_id;

        $data = EventComment::where('status','Active')->where('event_share_id',$event_id)->where('is_deleted','0')->orderBy('id', 'desc')->paginate(10);

        if(count($data)>0){
            return new EventCommentCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }


}
