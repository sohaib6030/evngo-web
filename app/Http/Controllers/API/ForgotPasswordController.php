<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Mail\MailchimpController;
use App\Http\Resources\UserResource;
use App\Http\Resources\SubscribeResource;
use App\SpCategory;
use App\User;
use App\UserDetails;
use App\models\UserType;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Password;
use URL;

use Validator;
use App\Helpers\Mail\PHPMailer;
use App\Helpers\Mail\SMTP;



use function GuzzleHttp\json_encode;

class ForgotPasswordController extends Controller
{
    public $successStatus = 200;


    public function postForgotPassword(Request $request){

        $email = $request->email;
        $user =  User::where('email',$email)->first();
        if($user)
        {
            $token = Password::getRepository()->create($user);
            $code = $token; //rand(999, 99999);
            $user->code = $code;
            $user->save();
            $this->sendEmail($user,$code);


            $response_data = [
                'success' => 1,
                'message' => 'Account password reset successfully.Please check your email account and follow the instruction!',
            ];

            return  response()->json($response_data, $this->successStatus);
        }else{
            $response_data = [
                'success' => 0,
                'message' => 'This email address does not exist!'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }




    public function sendEmail($user,$code)
    {


        $url = url('resetpassword/' . $user->email . '/' . urlencode($code));

        $url = str_replace('public/','', $url);

        $email_a =  trim($user->email);

        $mail = new PHPMailer(true);

        try {
            //Server settings
            //  $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'smtp.mailgun.org';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'postmaster@mg.evngo.com.au';                     // SMTP username
            $mail->Password   = 'cb03cfba6fa72239728cdff564ea5cb8-ee13fadb-c84d309a';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port       = 2525;                                    // TCP port to connect to

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Recipients
            $mail->setFrom('postmaster@mg.evngo.com.au', 'EVNGO');
            //  $mail->addAddress($email_a, $user->fname  );     // Add a recipient
            $mail->addAddress($email_a, $user->fname  );     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "Hello " . $user->fname . " Reset Your Password" ;
            //  $mail->Body    = "test";
            $mail->Body    = "<h2>Reset Your Password</h2><p>To change your password <a href='" . str_replace('public/','', $url) . "'>click here.</a></p><p>Or point your browser to this address: <br />" . str_replace('public/','', $url) . "</p> Thank you!</p>";

            $mail->send();
            //  return true;
        } catch (Exception $e) {
            //  return false;
        }




        /*

               $mail = new PHPMailer(true);
               $mail->SMTPAuth = true;
               $mail->isSMTP();
               $mail->CharSet = "utf-8";
               $mail->SMTPAuth = true;
               $mail->SMTPOptions = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));
               $mail->SMTPSecure = "ssl";
               $mail->Host = "secure.emailsrvr.com";
               $mail->Port = 587;
               $mail->Username = "support@inforox.com";
               $mail->Password = "Silver433x";
               $mail->setFrom("support@inforox.com","test");
               $mail->Subject = "Email Testing Subject";
               $mail->MsgHTML("This is test Email");
               $mail->addaddress("peter@sharklasers.com", "peter@sharklasers.com");
               $mail->send();
               return "Email Sent";
        //       Not Running
        //       $mail = new PHPMailer(true); // notice the \  you have to use root namespace here
        //       $mail->isSMTP(); // tell to use smtp
        //       $mail->CharSet = "utf-8"; // set charset to utf8
        //       $mail->SMTPAuth = false;  // use smpt auth
        //       $mail->SMTPSecure = "tls"; // or ssl
        //       $mail->Host = "smtp.mailgun.org";
        //       $mail->Port = 587; // most likely something different for you. This is the mailtrap.io port i use for testing.
        //       $mail->Username = "postmaster@mg.evngo.com.au";
        //       $mail->Password = "3332e02e542b9949f581dc475b9d60a9-f8faf5ef-a9cb663c";
        //       $mail->setfrom("postmaster@mg.evngo.com.au", "postmaster@mg.evngo.com.au");
        //       $mail->Subject = "Test";
        //       $mail->MsgHTML("This is a test");
        //       $mail->addaddress("sohaib6030@gmail.com", "sohaib6030@gmail.com");
        //       $mail->send();
        //       return "Success";
        //      $url = url('resetpassword/'.$user->email.'/'.urlencode($code));
              // dd($user->email);
        //      $email_a =  trim($user->email);

              $curl = curl_init();

             //echo ($email_a); exit;
         /*   curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.pepipost.com/v2/sendEmail",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "{\"personalizations\":[{\"recipient\":\"$email_a\"}],\"from\":{\"fromEmail\":\"info@evngo.com.au\",\"fromName\":\"Evngo\"},\"subject\":\"Hello ".$user->fname.", reset your password\",\"content\":\"<h2>Reset Your Password</h2><p>To change your password <a href='.$url.'>click here.</a></p><p>Or point your browser to this address: <br />".$url."</p> Thank you!</p>\"}",
             //CURLOPT_POSTFIELDS => $data_string,
              CURLOPT_HTTPHEADER => array(
                "api_key: 437b449d2ba275404aad973471587f67",
                "content-type: application/json"
              ),
            ));  */

        /*    curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.mailgun.net/v3/mg.evngo.com.au",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
     //           CURLOPT_POSTFIELDS => "{\"personalizations\":[{\"recipient\":\"$email_a\"}],\"from\":{\"fromEmail\":\"info@evngo.com.au\",\"fromName\":\"Evngo\"},\"subject\":\"Hello ".$user->fname.", reset your password\",\"content\":\"<h2>Reset Your Password</h2><p>To change your password <a href='.$url.'>click here.</a></p><p>Or point your browser to this address: <br />".$url."</p> Thank you!</p>\"}",
                //CURLOPT_POSTFIELDS => $data_string,
                CURLOPT_HTTPHEADER => array(
                    "api: 51189477a7d51691bfac9080e045f7c5-f8faf5ef-2e6a9418",
                    "content-type: application/json"
                ),
            ));

        */

        // $response = curl_exec($curl);
        // $err = curl_error($curl);
        //print_r($response);
        // curl_close($curl);

        //return;
        // if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   echo $response;
        // }



    }

}    
