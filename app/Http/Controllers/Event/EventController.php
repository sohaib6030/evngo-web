<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Http\Resources\TicketRevenueCollection;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\ServiceResource;
use App\Http\Resources\GroupCollection;
use App\Http\Resources\EventCommentCollection;
use App\Http\Resources\EventCommentResource;
use App\Http\Resources\EventJoinResource;
use App\Http\Resources\EventJoinCollection;
use App\Http\Resources\SubscripPriceResource;
use App\Http\Resources\FrontEventResource;
use App\Http\Resources\FrontEventCollection;
use App\models\SubscripPrice;
use App\models\Event;
use App\models\EventType;
use App\models\EventPaymentType;
use App\models\Service;
use App\models\EventTicketSold;
use App\models\UserService;
use App\models\EventService;
use App\models\EventAction;
use App\models\Group;
use App\models\EventShare;
use App\models\EventTicketType;
use App\models\EventTicket;
use App\models\EventComment;
use App\models\EventJoin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use URL;
use Validator;
use function GuzzleHttp\json_encode;
use App\Http\Resources\EventCollection;
use App\Http\Resources\EventTypeCollection;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\FrontGroupResource;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use DB;

class EventController extends Controller
{
    public $successStatus = 200;
    
    
    public function search(Request $request)
    {
        $user = Auth::user();
        $search = $request->search;
        if(isset($search) && ($search !="") )
        {
          $event   = EventShare::with('allevent')->whereHas('allevent',function($q) use ($search) {
              
                                               $q->where('title', 'LIKE', '%' . $search . '%');
                                               
                                            })->where('status','Active')->where('is_deleted','0')->where('type','Admin')->where('event_show','Public')->get();
        
         
        $group = Group::where('name', 'LIKE', '%' . $search . '%')->where('is_deleted',0)->orderBy('id', 'desc')->get();

         $response_data=[
                'success' => 1,
                'message' => 'success.',
                'event' =>  FrontEventResource::collection($event),
                'group' => FrontGroupResource::collection($group),
            ];
            
            
            if(count($response_data)>0){
            
             return response()->json($response_data,  $this->successStatus);
            }
            else{
                $response_data=[
                    'success' => 0,
                    'message' => 'Data Not Found.'
                ];
                return response()->json($response_data,  $this->successStatus);
            }
            
        }else{
                $response_data=[
                    'success' => 0,
                    'message' => 'Data Not Found.'
                ];
                return response()->json($response_data,  $this->successStatus);
            }
        
   
    }




    public function allShowcase(Request $request)
    {
        $latitude = $request->lat;
        $longitude = $request->long;

        $popularEvent  =  DB::select('SELECT v.*,y.join_users_count FROM event_shares AS v INNER JOIN
                          (select `events`.`id` ,(select count(*) from `event_join` where `events`.`id` = `event_join`.`event_id` ) as `join_users_count`
                          from `events`  where `events`.`is_deleted` = 0 and `events`.`status`  =  \'active\' AND `events`.`is_deleted` = 0 AND
                          `events`.`event_show` = \'Public\' AND `events`.`attendence` != \'out\' order by `join_users_count` desc LIMIT 5) as `y`
                          ON `v`.`event_id` = `y`.`id` where `v`.`type` = \'Admin\' AND `v`.`status`  =  \'active\' AND `v`.`is_deleted` = 0 order by y.join_users_count desc');


        $popularModels = EventShare::hydrate($popularEvent);

        $nearByEvent   = EventShare::with('getallevent')->whereHas('getallevent',function($q) use ($latitude, $longitude) {
            $q->whereRaw("ST_Distance_Sphere(
                                                    point(lon, lat),
                                                    point(?, ?)
                                                ) * .000621371192 < 50
                                            ", [
                $longitude,
                $latitude,
            ]);
        })->where('status','Active')->where('is_deleted','0')->where('type','Admin')->where('event_show','Public')->orderBy('id', 'desc')->limit(5)->get();


        $upcomingEvent  =  DB::select('SELECT v.* ,`y`.`event_date` ,`y`.`event_time` FROM event_shares AS v INNER JOIN
                              (select `events`.`id` ,`events`.`event_date` ,`events`.`event_time`  
                              from `events`  where `events`.`is_deleted` = 0 and `events`.`status`  =  \'Active\' and `event_show` = \'Public\' AND `events`.`is_deleted` = 0
                              and  cast(concat(`events`.`event_date`, \' \', `events`.`event_time`) as datetime) >= NOW() 
                              limit 5 ) as `y` ON `v`.`event_id` = `y`.`id` where `v`.`type` = \'Admin\' AND `v`.`status`  =  \'Active\' AND `v`.`is_deleted` = 0
                              and  cast(concat(`y`.`event_date`, \' \', `y`.`event_time`) as datetime) >= NOW() order by cast(concat(`y`.`event_date`, \' \', `y`.`event_time`) as datetime) asc');

        $upcomingModels = EventShare::hydrate($upcomingEvent);


        $response_data=[
            'success' => 1,
            'message' => 'success.',
            'popularEvent' =>  FrontEventResource::collection($popularModels),
            'nearByEvent' => FrontEventResource::collection($nearByEvent),
            'upcomingEvent' => FrontEventResource::collection($upcomingModels),

        ];

        if($response_data){
            return response()->json($response_data, $this->successStatus);
        }

        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }




    
    public function allEvent(Request $request)
    {
        $latitude = $request->lat;
        $longitude = $request->long;
            $user = Auth::user();
         /*   $popularEvent  = EventShare::with('getallevent')->whereHas('getallevent',function($q) use ($user) {
                                                $q->orderBy('total_join', 'desc');



                                            })->where('status','Active')->where('is_deleted','0')->where('type','Admin')->where('event_show','Public')->limit(5)->get();  */

        $popularEvent  =  DB::select('SELECT v.*,y.join_users_count FROM event_shares AS v INNER JOIN
                          (select `events`.`id` ,(select count(*) from `event_join` where `events`.`id` = `event_join`.`event_id` ) as `join_users_count`
                          from `events`  where `events`.`is_deleted` = 0 and `events`.`status`  =  \'active\' AND `events`.`is_deleted` = 0 AND
                          `events`.`event_show` = \'Public\' AND `events`.`attendence` != \'out\' order by `join_users_count` desc LIMIT 5) as `y`
                          ON `v`.`event_id` = `y`.`id` where `v`.`type` = \'Admin\' AND `v`.`status`  =  \'active\' AND `v`.`is_deleted` = 0 order by y.join_users_count desc');


        $popularModels = EventShare::hydrate($popularEvent);

              $nearByEvent   = EventShare::with('getallevent')->whereHas('getallevent',function($q) use ($latitude, $longitude) {
                                               $q->whereRaw("ST_Distance_Sphere(
                                                    point(lon, lat),
                                                    point(?, ?)
                                                ) * .000621371192 < 50
                                            ", [
                                                $longitude,
                                                $latitude,
                                            ]);
                             })->where('status','Active')->where('is_deleted','0')->where('type','Admin')->where('event_show','Public')->orderBy('id', 'desc')->limit(5)->get();




/*        $upcomingEvent   = EventShare::with('getallevent')->whereHas('getallevent',function($q) use ($user) {
            $q->whereDate('event_date', '>', Carbon::now());
        })->where('status','Active')->where('is_deleted','0')->where('type','Admin')->where('event_show','Public')->limit(5)->get()
            ->sortByDesc('getallevent.event_payment_id')
            ->sortBy('getallevent.event_date'.'getallevent.event_time');

*/
        $upcomingEvent  =  DB::select('SELECT v.* ,`y`.`event_date` ,`y`.`event_time` FROM event_shares AS v INNER JOIN
                              (select `events`.`id` ,`events`.`event_date` ,`events`.`event_time`  
                              from `events`  where `events`.`is_deleted` = 0 and `events`.`status`  =  \'Active\' and `event_show` = \'Public\' AND `events`.`is_deleted` = 0
                              and  cast(concat(`events`.`event_date`, \' \', `events`.`event_time`) as datetime) >= NOW() 
                              limit 5 ) as `y` ON `v`.`event_id` = `y`.`id` where `v`.`type` = \'Admin\' AND `v`.`status`  =  \'Active\' AND `v`.`is_deleted` = 0
                              and  cast(concat(`y`.`event_date`, \' \', `y`.`event_time`) as datetime) >= NOW() order by cast(concat(`y`.`event_date`, \' \', `y`.`event_time`) as datetime) asc');

        $upcomingModels = EventShare::hydrate($upcomingEvent);

            $attandedEvent     =  EventJoin::where('user_id',$user->id)->where('attanded','Present')->count();
            $failEvent = 6;
            $createdEvent = Event::where('created_by',$user->id)->where('status','Active')->where('is_deleted','0')->count();
            $subscripPrice = SubscripPrice::where('is_deleted','0')->first();

        $factor = 150;
        $points = 0;

        $amount =  EventJoin::where('event_join.user_id',$user->id)
            ->join('event_tickets_sold', 'event_join.id', '=', 'event_tickets_sold.event_join_id')
            ->where('event_tickets_sold.status','Active')
            ->select('event_tickets_sold.*')->get();

        if($amount->isEmpty())
        {

        }
        else
        {
            $val =  $amount->sum(function($t){
                return $t->event_ticket_sold * $t->event_ticket_price;
            });
            $points = floor($val / $factor);
        }




            $response_data=[
                'success' => 1,
                'message' => 'success.',
                'popularEvent' =>  FrontEventResource::collection($popularModels),
                'nearByEvent' => FrontEventResource::collection($nearByEvent),
                'upcomingEvent' => FrontEventResource::collection($upcomingModels),
                'subscripPrice' => new SubscripPriceResource($subscripPrice),
                'attandedEvent' => $attandedEvent,
                'failEvent' => $failEvent,
                'createdEvent' => $createdEvent,
                'user' => new UserResource($user),
                'points'   => $points
            ];

        if($response_data){
            return response()->json($response_data, $this->successStatus);
        }
        
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            //return response()->json(['error'=>'Unauthorised'], 401);
            return response()->json($response_data,  $this->successStatus);
        }
    }


    
   public function fetchEvent(Request $request)
    {
        $latitude = $request->lat;
        $latitude = $request->lat;
        $longitude = $request->long;
        $type = $request->type;
        $user = Auth::user();

        $search_event = ($request->search_event != null || $request->search_event != "" ? $request->search_event : "");
        $searchevent_list = "";

        $search_venue = ($request->search_venue != null || $request->search_venue != "" ? $request->search_venue : "");
        $search_venue_list = "";

        $search_datefrom = ($request->search_datefrom != null || $request->search_datefrom != "" ? $request->search_datefrom : "");
        $search_datefrom_list = "";

        $search_dateto = ($request->search_dateto != null || $request->search_dateto != "" ? $request->search_dateto : "");
        $search_dateto_list = "";

        $search_eventtype = ($request->search_eventtype != null || $request->search_eventtype != "" ? $request->search_eventtype : "");
        $search_eventtype_list = "";

        $search_paymenttype = ($request->search_paymenttype != null || $request->search_paymenttype != "" ? $request->search_paymenttype : "");
        $search_paymenttype_list = "";

        $search_country = ($request->search_country != null || $request->search_country != "" ? $request->search_country : "");
        $searchcountry_list = "";

        $search_city = ($request->search_city != null || $request->search_city != "" ? $request->search_city : "");
        $searchcity_list = "";

        //region Event Search List
        if($search_event == "") {
            $searchevent_list = Event::where('is_deleted','0')->distinct('title')->pluck('title');
        }
        else {
            $searchevent_list = Event::where('is_deleted','0')->whereRaw("title like ? ", '%' . $search_event . '%')->distinct('title')->pluck('title');
        }
        //#endregion
        //region Venue Search List
        if($search_venue == "") {
            $search_venue_list = Event::where('is_deleted','0')->distinct('venue')->pluck('venue');
        }
        else {
            $search_venue_list = Event::where('is_deleted','0')->whereRaw("venue like ? ", '%' . $search_venue . '%')->distinct('venue')->pluck('venue');
        }
        //endregion
        //region DateFrom Search List
        if($search_datefrom == "") {
            $search_datefrom_list = Event::where('is_deleted','0')->distinct('event_date')->pluck('event_date');
        }
        else {
            $search_datefrom_list = Event::where('is_deleted','0')->whereDate('event_date','>=',$search_datefrom)->distinct('event_date')->pluck('event_date');
        }
        //endregion
        //region DateTo Search List
        if($search_dateto == "") {
            $search_dateto_list = Event::where('is_deleted','0')->distinct('event_date')->pluck('event_date');
        }
        else {
            $search_dateto_list = Event::where('is_deleted','0')->whereDate('event_date','<=',$search_dateto)->distinct('event_date')->pluck('event_date');
        }
        //endregion
        //region Event Type Search List
        if($search_eventtype == "") {
            $search_eventtype_list = EventType::where('is_deleted','0')->distinct('id')->pluck('id');
        }
        else {
            $search_eventtype_list = EventType::where('is_deleted','0')->where('title',$search_eventtype)->distinct('id')->pluck('id');
        }
        //endregion
        //region Payment Type Search List
        if($search_paymenttype == "") {
            $search_paymenttype_list = EventPaymentType::where('is_deleted','0')->distinct('id')->pluck('id');
        }
        else {
            $search_paymenttype_list = EventPaymentType::where('is_deleted','0')->where('title',$search_paymenttype)->distinct('id')->pluck('id');
        }
        //endregion
        //region Country Search List
        if($search_country == "") {
            $searchcountry_list = Event::where('is_deleted','0')->distinct('Country')->pluck('Country');
        }
        else {
            $searchcountry_list = Event::where('is_deleted','0')->whereRaw("Country like ? ", '%' . $search_country . '%')->distinct('Country')->pluck('Country');
        }
        //endregion
        //region City Search List
        if($search_city == "") {
            $searchcity_list = Event::where('is_deleted','0')->distinct('City')->pluck('City');
        }
        else {
            $searchcity_list = Event::where('is_deleted','0')->whereRaw("City like ? ", '%' . $search_city . '%')->distinct('City')->pluck('City');
        }
        //endregion

        if($type=='upcomming'){

            $EventJoin_ =  EventJoin::where('user_id',$user->id)->pluck('event_id')->filter(function($value, $key) {
                return  $value != null;
            });

            //->whereIn('created_by',$seachusers)


           $data = EventShare::with('allevent')->whereHas('allevent',function($q) use ($user, $searchevent_list, $search_venue_list, $search_datefrom_list,
               $search_dateto_list, $search_eventtype_list, $search_paymenttype_list, $searchcountry_list, $searchcity_list) {
                                               $q->whereDate('event_date', '>', Carbon::now())
                                                 ->whereIn('title',$searchevent_list)
                                                 ->whereIn('venue',$search_venue_list)
                                                 ->whereIn('event_date',$search_datefrom_list)
                                                 ->whereIn('event_date',$search_dateto_list)
                                                 ->whereIn('event_type_id',$search_eventtype_list)
                                                 ->whereIn('event_payment_id',$search_paymenttype_list)
                                                 ->whereIn('Country',$searchcountry_list)
                                                 ->whereIn('City',$searchcity_list);
                                            })
               ->whereNotIn('event_id' ,$EventJoin_)
               ->where('status','Active')
               ->where('is_deleted','0')
               ->where('type','Admin')
               ->where('event_show','Public')
               ->orderBy('id', 'desc')
               ->paginate(10);
        }else
        if($type=='nearby'){
            $data   = EventShare::with('allevent')->whereHas('allevent',function($q) use ($latitude, $longitude, $searchevent_list, $search_venue_list, $search_datefrom_list,
                $search_dateto_list, $search_eventtype_list, $search_paymenttype_list, $searchcountry_list, $searchcity_list) {
                                               $q->whereRaw("ST_Distance_Sphere(
                                                    point(lon, lat),
                                                    point(?, ?)
                                                ) * .000621371192 < 50
                                            ", [
                                                $longitude,
                                                $latitude,
                                            ])
                                           ->whereIn('title',$searchevent_list)
                                           ->whereIn('venue',$search_venue_list)
                                           ->whereIn('event_date',$search_datefrom_list)
                                           ->whereIn('event_date',$search_dateto_list)
                                           ->whereIn('event_type_id',$search_eventtype_list)
                                           ->whereIn('event_payment_id',$search_paymenttype_list)
                                                   ->whereIn('Country',$searchcountry_list)
                                                   ->whereIn('City',$searchcity_list);

                                            })->where('status','Active')->where('is_deleted','0')->where('type','Admin')->where('event_show','Public')->orderBy('id', 'desc')->paginate(10);
        }else
        if($type=='popular'){
           $data  = EventShare::with('allevent')->whereHas('allevent',function($q) use ($user, $searchevent_list, $search_venue_list, $search_datefrom_list,
               $search_dateto_list, $search_eventtype_list, $search_paymenttype_list, $searchcountry_list, $searchcity_list) {
                                                $q->whereIn('title',$searchevent_list)
                                                  ->whereIn('venue',$search_venue_list)
                                                  ->whereIn('event_date',$search_datefrom_list)
                                                  ->whereIn('event_date',$search_dateto_list)
                                                  ->whereIn('event_type_id',$search_eventtype_list)
                                                  ->whereIn('event_payment_id',$search_paymenttype_list)
                                                    ->whereIn('Country',$searchcountry_list)
                                                    ->whereIn('City',$searchcity_list)
                                                  ->orderBy('total_join', 'desc');
                                            })->where('status','Active')->where('is_deleted','0')->where('type','Admin')->where('event_show','Public')->paginate(10);

        }else
        if($type=='createdbyme'){


           //$data  = EventShare::where('status','Active')->where('is_deleted','0')->where('user_id',$user->id)->where('type','Admin')->orderBy('id', 'desc')->paginate(10);


           $data  = EventShare::with('allevent')->whereHas('allevent',function($q) use ($searchevent_list, $search_venue_list, $search_datefrom_list,
               $search_dateto_list, $search_eventtype_list, $search_paymenttype_list, $searchcountry_list, $searchcity_list) {
                                                $q->whereIn('title',$searchevent_list)
                                                 ->whereIn('venue',$search_venue_list)
                                                 ->whereIn('event_date',$search_datefrom_list)
                                                 ->whereIn('event_date',$search_dateto_list)
                                                 ->whereIn('event_type_id',$search_eventtype_list)
                                                 ->whereIn('event_payment_id',$search_paymenttype_list)
                                                    ->whereIn('Country',$searchcountry_list)
                                                    ->whereIn('City',$searchcity_list);
                                            })->where('status','Active')->where('is_deleted','0')->where('user_id',$user->id)->where('type','Admin')->orderBy('id', 'desc')->paginate(10);

        }else
        if($type=='joined'){
           $EventJoin     =  EventJoin::where('user_id',$user->id)->pluck('event_id')->filter(function($value, $key) {
               return  $value != null;
           });;
           $data  = EventShare::with('allevent')->whereHas('allevent',function($q) use ($searchevent_list, $search_venue_list, $search_datefrom_list,
               $search_dateto_list, $search_eventtype_list, $search_paymenttype_list, $searchcountry_list, $searchcity_list) {
                                           $q->whereIn('title',$searchevent_list)
                                               ->whereIn('venue',$search_venue_list)
                                               ->whereIn('event_date',$search_datefrom_list)
                                               ->whereIn('event_date',$search_dateto_list)
                                               ->whereIn('event_type_id',$search_eventtype_list)
                                               ->whereIn('event_payment_id',$search_paymenttype_list)
                                               ->whereIn('Country',$searchcountry_list)
                                               ->whereIn('City',$searchcity_list);
                                       })
                                       ->whereIN('event_id',$EventJoin)
                                       ->where('status','Active')
                                       ->where('is_deleted','0')
                                       ->Where('user_id',$user->id)
                                      // ->Where('type','Admin')
                                    /*   ->where(function ($q)  use ($user){
                                           $q->Where('event_shares.event_show', 'Public')
                                           
                                           ->where(function ($p)  use ($user){
                                               $p->Where('type','Admin');
                                           });
                                       })
                                       ->orWhere(function ($s)  use ($user){
                                           $s->Where('event_shares.event_show', 'Private')->Where('user_id',$user->id);
                                       })  */
                                       ->orderBy('id', 'desc')->paginate(10);

        }else
        if($type=='sharedwithme'){
            $EventJoin_ =  EventJoin::where('user_id',$user->id)->pluck('event_id')->filter(function($value, $key) {
                return  $value != null;
            });;
           $data  = EventShare::with('allevent')->whereHas('allevent',function($q) use ($searchevent_list, $search_venue_list, $search_datefrom_list,
               $search_dateto_list, $search_eventtype_list, $search_paymenttype_list, $searchcountry_list, $searchcity_list) {
               $q->whereIn('title',$searchevent_list)
                   ->whereIn('venue',$search_venue_list)
                   ->whereIn('event_date',$search_datefrom_list)
                   ->whereIn('event_date',$search_dateto_list)
                   ->whereIn('event_type_id',$search_eventtype_list)
                   ->whereIn('event_payment_id',$search_paymenttype_list)
                   ->whereIn('Country',$searchcountry_list)
                   ->whereIn('City',$searchcity_list);
           })->whereNotIN('event_id', $EventJoin_)
               ->where('status','Active')
               ->where('is_deleted','0')
               ->Where('user_id',$user->id)
               ->where(function ($q)  use ($user){
                                           $q->Where('event_shares.event_show', 'Public')
                                           
                                           ->where(function ($p)  use ($user){
                                             $p->where('type','User');
                                           });
                                           
                                           
                                           
                                       })
               // ->orWhere(function ($s)  use ($user){
                                       //    $s->Where('event_shares.event_show', 'Private')->where('type','Admin');
                                     //  })  
              // ->where('type','Admin')
              // ->where('type','User')
               ->orderBy('id', 'desc')
               ->distinct()
               ->paginate(10);
               

        }
        
        else
        if($type=='singleevent'){

            $eventdata  = EventShare::where('event_id', $request->event_id)->first();
            
            if (($eventdata->event_show == 'Private') && ($eventdata->created_by != $user->id))
            {
                
                $eventdataexists  = EventShare::where('event_id', $request->event_id)->where('user_id', $user->id)->first();
                if (!$eventdataexists)
                {
                    $response_data=[
                    'success' => 0,
                    'message' => 'This Private Event Is Not Shared With You.'
                    ];
                    return response()->json($response_data,  $this->successStatus);
                }
            }
   
            
            $data  = EventShare::where('event_id', $request->event_id)
            ->where('status','Active')
            ->where('is_deleted','0')
            ->orderBy('id', 'desc')
            ->paginate(10);
            
        }
        
        
    
        if(count($data)>0){
            
            return new FrontEventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }


    public function getRevenueDetail(Request $request)
    {
        //$latitude = $request->lat;
        //$latitude = $request->lat;
        //$longitude = $request->long;
        $event_id = $request->event_id;
        $type_id = ($request->type_id != null || $request->type_id != "" ? $request->type_id : "0");
        $user_search = ($request->searchuser != null || $request->searchuser != "" ? $request->searchuser : "");
        $userid = ($request->userid != null || $request->userid != "" ? $request->userid : "0");
        $alltypes="";
        $users="";
        $seachusers="";

        //TypeID
        if($type_id == "0")
        {
            $alltypes = EventTicketType::where('is_deleted','0')->pluck('id');
        }
        else
        {
            $alltypes = EventTicketType::where('is_deleted','0')->where('id',$type_id)->pluck('id');
        }

        //UserID
        if($userid == "0")
        {
            $users = user::where('status','1')->pluck('id');
        }
        else
        {
            $users = user::where('status','1')->where('id',$userid)->pluck('id');
            //$users = EventTicketType::where('is_deleted','0')->where('id',$type_id)->pluck('id');
        }

        //Search User
        if($user_search == "")
        {
            $seachusers = user::where('status','1')->pluck('id');
        }
        else
        {
            $seachusers = user::where('status','1')->whereRaw("CONCAT(`fname`, ' ', `lname`) like ? ", '%' . $user_search . '%')->pluck('id');
            //$users = EventTicketType::where('is_deleted','0')->where('id',$type_id)->pluck('id');
        }


        //$singletype = EventTicketType::where('is_deleted','0')->where('','')->pluck('id');


        $user = Auth::user();


        $EventJoin = EventJoin::where('event_id',$event_id)->pluck('id');
        $data  = EventTicketSold::WhereIN('event_join_id', $EventJoin)
            ->whereIn('event_ticket_type_id',$alltypes)
            ->whereIn('created_by',$users)
            ->whereIn('created_by',$seachusers)
            ->where('event_ticket_sold','>',0)
            ->paginate(10);

        if(count($data)>0){

            return new TicketRevenueCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            //return response()->json(['error'=>'Unauthorised'], 401);
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    public function myEvent(Request $request)
    {
            $user = Auth::user();
            $createdbyMe  = EventShare::where('status','Active')->where('is_deleted','0')->where('user_id',$user->id)->where('type','Admin')->orderBy('id', 'desc')->limit(5)->get();
            $EventJoin     =  EventJoin::where('user_id',$user->id)->pluck('event_id');
            $joinedEvents  = EventShare::whereIN('event_id',$EventJoin)->where('status','Active')->where('is_deleted','0')->orderBy('id', 'desc')->limit(5)->get();
            //$joinedEvents   = EventShare::where('status','Active')->where('is_deleted','0')->where('type','Admin')->orderBy('id', 'desc')->limit(5)->get();
            $sharedwithme = EventShare::where('status','Active')->where('is_deleted','0')->where('user_id',$user->id)->where('type','User')->orderBy('id', 'desc')->limit(5)->get();
           
            
            $response_data=[
                'success' => 1,
                'message' => 'success.',
                'createdbyMe' =>  FrontEventResource::collection($createdbyMe),
                'joinedEvents' => FrontEventResource::collection($joinedEvents),
                'sharedwithme' => FrontEventResource::collection($sharedwithme),
            ];

        if($response_data){
            return response()->json($response_data, $this->successStatus);
        }
        
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            //return response()->json(['error'=>'Unauthorised'], 401);
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    
    public function eventDetail(Request $request)
    {
            $user = Auth::user();
            $id = $request->event_id;
            $data  = EventShare::findOrFail($id);
            
            
            $response_data=[
                'success' => 1,
                'message' => 'success.',
                'data' =>  new FrontEventResource($data),
            ];

        if($response_data){
            return response()->json($response_data, $this->successStatus);
        }
        
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            //return response()->json(['error'=>'Unauthorised'], 401);
            return response()->json($response_data,  $this->successStatus);
        }
    }


}
//  $popularEvent  = EventShare::whereHas('getallevent',function($q)  {
//  $q->orderBy('total_join', 'desc');
//   $q->
//where('attendence','!=' ,'out')
//->has('joinUsers', '>' , 3)
// ->with('joinUsers')
//   withCount('joinUsers')
//  ->orderByDesc('join_users_count');
//  ->sortByDesc('join_users_count');
//->orderBy('joincount', 'desc');
//  ->with('joincount');
//   })
//->where('status','Active')
// ->where('is_deleted','0')
//->where('type','Admin')
// ->where('event_show','Public')
///   ->limit(1)->toSql();
// ->get();

//echo ($popularEvent);