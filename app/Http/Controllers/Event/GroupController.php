<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\GroupResource;
use App\Http\Resources\GroupCollection;
use App\Http\Resources\GroupMemberResource;
use App\Http\Resources\GroupMemberCollection;
use App\Http\Resources\GroupTypeResource;
use App\Http\Resources\GroupTypeCollection;
use App\Http\Resources\UserReceviedGroupRequestResource;
use App\Http\Resources\UserReceviedGroupRequestCollection;
use App\models\Event;
use App\models\Group;
use App\models\GroupMember;
use App\models\GroupEvent;
use App\models\GroupType;
use App\models\EventShare;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use URL;
use Validator;
use function GuzzleHttp\json_encode;
use App\Http\Resources\EventCollection;
use Illuminate\Support\Facades\Log;
use FCM;
use App\Http\Resources\FrontGroupResource;
use App\Http\Resources\FrontGroupCollection;
use App\Http\Resources\FrontEventCollection;
use App\Http\Resources\FrontEventResource;

class GroupController extends Controller
{
    public $successStatus = 200;
    
    
    public function groupLeave(Request $request)
    {
      $user = Auth::user();
      $data = GroupMember::where('user_id',$user->id)->where('group_id',$request->group_id)->get();
    //  $data->is_deleted = '1';
    //  $data->save();
      if($data){
          
        GroupMember::where('user_id',$user->id)->where('group_id',$request->group_id)->update(['is_deleted' => 1]);  
          
          
      $response_data = [
                'success' => 1,
                'message' =>  'Group successfully leaved.',
            ];
      }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
           
        }
         return response()->json($response_data,  $this->successStatus);

    }
    
    public function groupEvents(Request $request)
    {
        $user = Auth::user();
        $group_id = $request->group_id;
        $data = EventShare::where('status','Active')->where('user_id',$user->id)->where('group_id',$group_id)->where('is_deleted','0')->orderBy('id', 'desc')->paginate(10);

        if(count($data)>0){
            return new FrontEventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    
    
    
    
    public function allGroups()
    {
        $user = Auth::user();

        $groupids = [];
        $datass = Group::whereHas('joinmembers', function ($query) use ($user) {
            
          $query->where('user_id',$user->id);
          $query->where('status','Pending');
          $query->where('is_deleted',0);
          

        })
        ->where('is_deleted',0)->orderBy('id', 'desc')->where('created_by','!=',$user->id)->paginate(10);

        if($datass->items() > 0){
            foreach($datass->items() as $device){
                $groupids[] = $device->id;
            }
        }
        
    $members = GroupMember::select('group_id')->where('status','Approved')->where('is_deleted',0)->where('user_id','=',$user->id)->orderBy('group_id', 'desc')->distinct()->get();
        
        
      $newgrps =  Group::where('is_deleted',0)->where('created_by','!=',$user->id)->whereNotIn('id',$members)->orderBy('id', 'desc')->paginate(10);



        $data = Group::whereHas('members', function ($query) use ($groupids) {
            //$query->whereNotIn('group_id',$groupids);

        })
            ->whereNotIn('id',$groupids)->where('is_deleted',0)->orderBy('id', 'desc')->where('created_by','!=',$user->id)->paginate(10);

        if(count($data)>0){
            return new FrontGroupCollection($newgrps);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

    
    public function myGroups()
    {
        $user = Auth::user();
        $data = Group::where('is_deleted',0)->where('created_by',$user->id)->orderBy('id', 'desc')->paginate(10);
       
        if(count($data)>0){
            return new FrontGroupCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
    
    
    
    
    
    public function myJoinGroups()
    {
        
        $user = Auth::user();
        $data = Group::whereHas('members', function ($query) use ($user) {
            $query->where('user_id',$user->id);
            $query->where('status','Approved');
            $query->where('is_deleted',0);
        })
        //->where('is_deleted',0)
            ->orderBy('id', 'desc')->where('created_by','!=',$user->id)->paginate(10);
        
        if(count($data)>0){
            return new FrontGroupCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }

   

    public function store(Request $request)
    {
        
        Log::info($request->all());
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'group_type_id'  => 'required',
            'description'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        if(isset($request->edit_id) && ($request->edit_id !="") )
        {
            
            $data = Group::findOrFail($request->edit_id);
            $data->updated_by  = $user->id;
            $data->name  = $request->name;
            $data->group_type_id  = $request->group_type_id;
            $data->description  = $request->description;
            
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $image = $file->getClientOriginalName();
                Storage::disk('local')->put('/public/groups/' . $image, File::get($file));
                $data->image = $image;
            }    
            $data->save();
    
            $response_data = [
                'success' => 1,
                'message' => 'Group successfully updated.',
                //'data' => new GroupResource($data)
            ];
        }else{
           
            $data = new Group;
            $data->created_by  = $user->id;
            $data->name  = $request->name;
            $data->group_type_id  = $request->group_type_id;
            $data->description  = $request->description;
            $data->status  = 'Pending';
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $image = $file->getClientOriginalName();
                Storage::disk('local')->put('/public/groups/' . $image, File::get($file));
                $data->image = $image;
            }    
            $data->save();
            
            if($data){
                
                $member = new GroupMember;
                $member->created_by     = $user->id;
                $member->group_id       = $data->id;
                $member->user_id        = $user->id;
                $member->role_in_group  = 'Admin';
                $member->status         = 'Approved';
                $member->save();
            }
    
            $response_data = [
                'success' => 1,
                'message' => 'Group successfully created.',
               // 'data' => new GroupResource($data)
            ]; 
            
        }
            
        
        return response()->json($response_data, $this->successStatus);

    
    }
    
    
    public function groupDelete(Request $request)
    {
      $data = Group::findOrFail($request->group_id);
      $data->is_deleted = '1';
      $data->save();
      if($data){
      $response_data = [
                'success' => 1,
                'message' => 'Group successfully deleted.',
            ];
      }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
           
        }
         return response()->json($response_data,  $this->successStatus);

    }
    
    public function memberAddInGroup(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'group_id'  => 'required',
            'user_id'  => 'required',
            'role_in_group'  => 'required',
        ]);
        if ($validator->fails()) {
            $response_data = [
                'success' => 0,
                'message' => 'Validation error.',
                'data' => $validator->errors()
            ];
            return response()->json($response_data,  $this->successStatus);
        }
        $alreadyMember = GroupMember::where('user_id',$request->user_id)->where('group_id',$request->group_id)->first();
        if($alreadyMember)
        {
            $response_data = [
                'success' => 0,
                'message' => 'Aready a member.'
            ];
        }else{
             $group = Group::findOrFail($request->group_id);
            $data = new GroupMember;
            $data->created_by     = $user->id;
            $data->group_id       = $request->group_id;
            $data->user_id        = $request->user_id;
            $data->requested_by        = $request->user_id;
            $data->role_in_group  = $request->role_in_group;
            $data->status         = 'Pending';
            $data->type         = 'RequestGroup';
            $data->save();
            
            $recipients =[];
            if($data->member->devices){
                foreach($data->member->devices as $device){
                    $recipients[] = $device->registerid;
                }
            }
                    
                    
            if(count($recipients) > 0){
                $result=fcm()->to($recipients) // $recipients must an array
                ->data([
                        'content_available' => true,
                        'title' => 'Evengo Group Invite',
                        'body' => $data->user->fname.' '.$data->user->lname.' requested you to  join  ' . $data->group->name,
                        'notification_type' => 'group_request_notification',
                  ]
                )
                ->notification([
                    'sound' => 'default',
                    'content_available' => true,
                    'title' => 'Evengo Group Invite',
                    'body' => $data->user->fname.' '.$data->user->lname.' requested you to  join  ' . $data->group->name,
                    'channelid' => 'group_request_notification',
                    'notification_type' => 'group_request_notification',
                ])

                ->send();
            }
    
            $response_data = [
                'success' => 1,
                'message' => 'Member successfully added.',
                'data' => new GroupMemberResource($data)
            ]; 
            
        }

        return response()->json($response_data, $this->successStatus);
    }
    
    public function groupMembers(Request $request)
    {
        $data = GroupMember::where('group_id',$request->group_id)->where('status','Approved')->where('is_deleted',0)->orderBy('id', 'desc')->paginate(10);

        if(count($data)>0){
            return new GroupMemberCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }
    }
    
   
    
    public function edit(Request $request)
    {
      $data = Event::findOrFail($request->event_id);

        if($data){
            return new EventCollection($data);
        }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
            return response()->json($response_data,  $this->successStatus);
        }

    }


    public function memberBlock(Request $request)
    {
      $data = GroupMember::findOrFail($request->member_id);
      $data->status = 'Block';
      $data->save();
      if($data){
      $response_data = [
                'success' => 1,
                'message' => 'Member successfully blocked.',
            ];
      }
        else{
            $response_data=[
                'success' => 0,
                'message' => 'Data Not Found.'
            ];
           
        }
         return response()->json($response_data,  $this->successStatus);

    }

  

}
