<?php

namespace App\Http\Controllers\Admin;

use App\models\SubscripPrice;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use DataTables;
use Auth;
use Session;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SubscripPrice::findOrFail(1);
        return view('admin.subscrib.index')->with('data',$data);
    }

 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'vip' => 'required',
            'serviceProvider' => 'required',
            'promoter' => 'required',
        );

        $data = [
            'vip' => trim($request->get('vip')),
            'serviceProvider' => trim($request->get('serviceProvider')),
            'promoter' => trim($request->get('promoter')),
            ];
        $validator = Validator::make($data,$rules);
     
    if($validator->fails())
       {
        return  response()->json(['errors'=>$validator->errors()]);
       }else
       {
        $user_id = Auth::user()->id;

         
            $data = SubscripPrice::findOrFail(1);
            $data->vip = $request->vip;
            $data->serviceProvider     = $request->serviceProvider;
            $data->promoter     = $request->promoter;
            $data->created_by     = $user_id;
            $data->save(); 
            $success = 'Subscrip Price has been updated.';
            Session::flash('success','Subscrip Price has been updated');
            return response()->json($success);
            

        }
    
    }

    
    public function edit(Request $request)
    {
      $data = Country::findOrFail($request->id);
      return response()->json($data);

    }


    public function countryActive(Request $request)
    {
      $data = Country::findOrFail($request->id);
      $data->status = 'Active';
      $data->save();
      $message = 'Successfully Active.';
      return response()->json($message);

    }
     

    public function countryDisable(Request $request)
    {
      $data = Country::findOrFail($request->id);
      $data->status = 'Disable';
      $data->save();
      $message = 'Successfully Disable.';
      return response()->json($message);

    }

   
}
