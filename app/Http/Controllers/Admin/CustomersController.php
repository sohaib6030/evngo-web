<?php

namespace App\Http\Controllers\Admin;

use App\models\EventType;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use DataTables;
use Auth;
use App\User;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::where('status','Active')->where('role_id','2')->get();
        return view('admin.customer.index')->with('data',$data);
    }

   

    public function fetch(){

        $data = User::orderBy('id','desc')->where('role_id','2');
        return DataTables::of($data)
        ->addColumn('created_at',function($data){
            return $data->created_at->format('Y-m-d');
        })
        ->addColumn('status',function($data){
          if($data->status=='Disable') {
            return '<span class="label label-danger">Disable</span>';
          }else if($data->status=='1'){
            return '<span class="label label-success">Active</span>';
          }
          else{
            return '<span class="label  label-primary">'.$data->status.'</span>';
          }
        })

        ->addColumn('options',function($data){
            if($data->status=='1'){
            return "&emsp;<a class='btn btn-success edit_model'
                                     href='#' data-id='".$data->id."'><i class='fa fa-edit'></i></a>
                                     <a data-toggle='tooltip' data-placement='bottom' title='Disable' class='btn btn-danger disable' data-original-title='Disable' href='#' data-id='".$data->id."'><i class='fa fa-close'></i></a>
                                     ";
            }else if($data->status=='0'){
             return "&emsp;<a class='btn btn-success edit_model'
                                     href='#' data-id='".$data->id."'><i class='fa fa-edit'></i></a>
                                     <a data-toggle='tooltip' data-placement='bottom' title='Active' class='btn btn-success active' data-original-title='Active' href='#' data-id='".$data->id."'><i class='fa fa-check'></i></a>
                                     "; 
            }                         
        })
     
        ->rawColumns(['created_at', 'status','options'])
        ->make(true);
    }


    
    public function edit(Request $request)
    {
      $data = User::findOrFail($request->id);
      return response()->json($data);

    }


    public function customerActive(Request $request)
    {
      $data = User::findOrFail($request->id);
      $data->status = '1';
      $data->save();
      $message = 'Successfully Active.';
      return response()->json($message);

    }
     

    public function customerDisable(Request $request)
    {
      $data = User::findOrFail($request->id);
      $data->status = '0';
      $data->save();
      $message = 'Successfully Disable.';
      return response()->json($message);

    }

   
}
