<?php
$curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.pepipost.com/v2/sendEmail",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\"personalizations\":[{\"recipient\":\"nomanshah434514@gmail.com\"}],\"from\":{\"fromEmail\":\"info@evngo.com.au\",\"fromName\":\"Evngo\"},\"subject\":\"Welcome to Pepipost\",\"content\":\"Hi, this is my first trial mail\"}",
      CURLOPT_HTTPHEADER => array(
        "api_key: 437b449d2ba275404aad973471587f67",
        "content-type: application/json"
      ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    //print_r($response);
    curl_close($curl);
    
    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }

?>