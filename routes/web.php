<?php
use App\Http\Helpers\Helper;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/noti', 'API\EventController@notification')->name('noti');
Route::get('/home', 'Admin\HomeController@index')->name('home');
//Route::get('/dashboard', 'HomeController@index')->name('dashboard');

//Dashboard
Route::get('/dashboard', 'Admin\HomeController@dashboard')->middleware('auth')->name('dashboard');

Route::get('/changepassword', ['as' => 'changepassword' , function () {
    return view('admin/home/changepassword');
 }])->middleware('auth');

 Route::get('/profile', ['as' => 'profile' , function () {
    return view('admin/home/profile');
 }])->middleware('auth');


Route::get('verify/accounts/{email}/{code}', 'API\UserController@verify');
Route::get('sendEmails', 'API\UserController@sendEmails');

Route::get('sendEmailReminder', 'API\UserController@sendEmailReminder');

//Roles
 Route::get('/roles/deactivate/{id}', 'Admin\RoleController@deactivate')->middleware('auth');
 Route::get('/roles/active/{id}', 'Admin\RoleController@active')->middleware('auth');
 Route::resource('roles', 'Admin\RoleController')->middleware('auth');

//Staff/Admins
 Route::post('admins.delete', 'Admin\UserController@delete')->middleware('auth')->name('admins.delete');
 Route::group(['prefix'=> 'admins'],function(){
    Route::get('create', 'Admin\UserController@create')->middleware('can:create-staff')->name('admins.create');
    Route::post('', 'Admin\UserController@store')->middleware('can:create-staff')->name('admins.store');   
    Route::get('', 'Admin\UserController@index')->middleware('can:admins-index')->name('admins.index');
    Route::get('admins/fetch', 'Admin\UserController@fetch')->middleware('auth')->name('admins.fetch');
    Route::get('{id}', 'Admin\UserController@show')->middleware('can:show-staff')->name('admins.show');
    Route::delete('delete/{id}', 'Admin\UserController@destroy')->middleware('can:delete-staff')->name('admins.destroy');
    Route::get('{id}/edit', 'Admin\UserController@edit')->middleware('can:edit-staff')->name('admins.edit');
    Route::patch('{id}', 'Admin\UserController@update')->middleware('auth')->name('admins.update');
    //Staff Status
    Route::get('resetpassword/{id}', 'Admin\UserController@resetPassword')->middleware('can:staff-reset-password')->name('resetpassword');
    Route::get('deactivate/{id}', 'Admin\UserController@deactivate')->middleware('can:status-staff');
    Route::get('active/{id}', 'Admin\UserController@active')->middleware('can:status-staff');
    
 });

 //Admin Menu
 Route::get('/menu/deactivate/{id}', 'Admin\AdminmenuController@deactivate')->middleware('can:menu-index');
 Route::get('/menu/active/{id}', 'Admin\AdminmenuController@active')->middleware('can:menu-index');
 Route::resource('menu', 'Admin\AdminmenuController')->middleware('can:menu-index');


// groupType
Route::get('/groupType', 'Admin\GroupTypeController@index')->middleware('can:groupType-index')->name('groupType.index');
Route::get('/groupType/fetch', 'Admin\GroupTypeController@fetch')->middleware('can:groupType-fetch')->name('groupType.fetch');
Route::post('/groupType/store', 'Admin\GroupTypeController@store')->middleware('can:groupType-store')->name('groupType.store');
Route::post('/groupType/edit', 'Admin\GroupTypeController@edit')->middleware('can:groupType-edit')->name('groupType.edit');
Route::post('groupType/active', 'Admin\GroupTypeController@groupTypeActive')->middleware('can:groupType-active')->name('groupType.active');
Route::post('groupType/disable', 'Admin\GroupTypeController@groupTypeDisable')->middleware('can:groupType-disable')->name('groupType.disable');

// eventType
Route::get('/eventType', 'Admin\EventTypeController@index')->middleware('can:eventType-index')->name('eventType.index');
Route::get('/eventType/fetch', 'Admin\EventTypeController@fetch')->middleware('can:eventType-fetch')->name('eventType.fetch');
Route::post('/eventType/store', 'Admin\EventTypeController@store')->middleware('can:eventType-store')->name('eventType.store');
Route::post('/eventType/edit', 'Admin\EventTypeController@edit')->middleware('can:eventType-edit')->name('eventType.edit');
Route::post('eventType/active', 'Admin\EventTypeController@eventTypeActive')->middleware('can:eventType-active')->name('eventType.active');
Route::post('eventType/disable', 'Admin\EventTypeController@eventTypeDisable')->middleware('can:eventType-disable')->name('eventType.disable');

// customer
Route::get('/customer', 'Admin\CustomersController@index')->middleware('can:customer-index')->name('customer.index');
Route::get('/customer/fetch', 'Admin\CustomersController@fetch')->middleware('can:customer-fetch')->name('customer.fetch');
Route::post('/customer/edit', 'Admin\CustomersController@edit')->middleware('can:customer-edit')->name('customer.edit');
Route::post('customer/active', 'Admin\CustomersController@customerActive')->middleware('can:customer-active')->name('customer.active');
Route::post('customer/disable', 'Admin\CustomersController@customerDisable')->middleware('can:customer-disable')->name('customer.disable');

// group
Route::get('/group', 'Admin\GroupController@index')->middleware('can:group-index')->name('group.index');
Route::get('/group/fetch', 'Admin\GroupController@fetch')->middleware('can:group-fetch')->name('group.fetch');
Route::post('/group/edit', 'Admin\GroupController@edit')->middleware('can:group-edit')->name('group.edit');
Route::post('group/active', 'Admin\GroupController@groupActive')->middleware('can:group-active')->name('group.active');
Route::post('group/disable', 'Admin\GroupController@groupDisable')->middleware('can:group-disable')->name('group.disable');

// event
Route::get('/event', 'Admin\EventController@index')->middleware('can:event-index')->name('event.index');
Route::get('/event/fetch', 'Admin\EventController@fetch')->middleware('can:event-fetch')->name('event.fetch');
Route::post('/event/edit', 'Admin\EventController@edit')->middleware('can:event-edit')->name('event.edit');
Route::post('event/active', 'Admin\EventController@eventActive')->middleware('can:event-active')->name('event.active');
Route::post('event/disable', 'Admin\EventController@eventDisable')->middleware('can:event-disable')->name('event.disable');

//subscriptionPrice
Route::get('/subscriptionPrice', 'Admin\SubscriptionController@index')->middleware('can:subscriptionPrice-index')->name('subscriptionPrice.index');
Route::post('/subscriptionPrice/store', 'Admin\SubscriptionController@store')->middleware('can:subscriptionPrice-store')->name('subscriptionPrice.store');