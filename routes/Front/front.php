<?php

use App\models\Group;

// Front End Routes
Route::get('/test', function(){
    $group =  Group::find(9);
    $data = [];
    foreach($group as $g){
    	 dd($g);
    }
})->name('test');

Route::get('/', 'Front\BaseController@landing_page')->name('landing_page');
Route::get('/user_panel', 'Front\BaseController@user_panel')->name('user_panel');
Route::get('/user_panel_register', 'Front\BaseController@user_panel_register')->name('user_panel_register');
Route::get('/user_success', 'Front\BaseController@user_success')->name('user_success');
Route::get('/user_failed', 'Front\BaseController@user_failed')->name('user_failed');
Route::get('/make_payment', 'Front\BaseController@make_payment')->name('make_payment');
// Route::get('/', function(){
// 	return redirect('/events');
// })->name('landing_page');events

// Users
Route::get('/setting/profile', 'Front\SettingController@profile')->name('setting.profile');
Route::post('/setting/profile', 'Front\SettingController@profile_update')->name('setting.profile.update');
// Users

// Auth Routes
Route::post('/attempt/login', 'Front\BaseController@attempt_login')->name('attempt_login');
Route::post('/user/logout', 'Front\BaseController@user_logout')->name('user_logout')->middleware('is_user');
Route::post('/user/register', 'Front\BaseController@user_register')->name('user_register');
Route::get('/resetpassword/{email}/{code}', 'Front\BaseController@reset_password');
Route::get('/password/recover', 'Front\BaseController@reset_password_form')->name('reset_password_form');
Route::post('/resetpassword', 'Front\BaseController@reset_new_password')->name('reset_new_password');
Route::post('/user/user_forget_password', 'Front\BaseController@user_forget_password')->name('user_forget_password');
// End Auth Routes

Route::get('verify/account/{email}/{code}', 'Front\BaseController@verify_email');

// Membership Routes

Route::get('/membership/subscribe/{type}', 'Front\MembershipController@subscribe_type')->name('m.subscribe.type')->middleware('is_user');

Route::get('/membership/subscribe', 'Front\MembershipController@subscribe')->name('m.subscribe')->middleware('is_user');
Route::post('/membership/subscribe', 'Front\MembershipController@subscribe_avail')->name('m.subscribe_avail')->middleware('is_user');


Route::get('/search', 'Front\EventController@searh')->name('front_searh')->middleware('is_user');

// Events Routes
Route::get('/events', 'Front\EventController@events')->name('events')->middleware('is_user');
Route::get('/event/{id}', 'Front\EventController@event')->name('event')->middleware('is_user');
Route::get('/my-events', 'Front\EventController@my_events')->name('my_events')->middleware('is_user');

Route::get('/groups/list', 'Front\GroupController@groups')->name('groups')->middleware('is_user');
Route::get('/requests', 'Front\GroupController@requests')->name('requests')->middleware('is_user');
Route::get('/group/view/{id}', 'Front\GroupController@group_view')->name('group-view')->middleware('is_user');
Route::get('/group/view/{id}/members', 'Front\GroupController@group_view')->name('group-view')->middleware('is_user');
Route::get('/group/view/{id}/groupmembers', 'Front\GroupController@group_view')->name('group-view')->middleware('is_user');
Route::get('/group/view/{id}/requests', 'Front\GroupController@group_view')->name('group-view')->middleware('is_user');
// Route::get('/my-events', 'Front\EventController@my_events')->name('my_events')->middleware('is_user');

// Services
Route::get('/services/add', 'Front\ServiceController@service_add')->name('services.add')->middleware('is_user');
Route::post('/services/add', 'Front\ServiceController@service_store')->name('services.store')->middleware('is_user');

Route::post('firebase/token', 'Front\BaseController@firebase_token_store')->name('front.firebase_token_store');