<?php


Route::group(['middleware' => 'auth:api'], function(){
	// Events Types
Route::get('eventType', 'Front\EventController@events_types')->name('front.events_types');
// Events Types

Route::get('allEvent', 'Front\EventController@all_events_api')->name('front.all_events_api');
Route::post('eventCreate', 'Front\EventController@event_create')->name('front.event_create');
Route::get('event/view', 'Front\EventController@event_view')->name('front.event-view');

Route::get('group/search', 'Front\GroupController@search')->name('front.group_search');


Route::post('events/search', 'Front\EventController@search_data')->name('front.event_search');

Route::get('events/sharedwithme', 'Front\EventController@sharedwithme')->name('front.event.sharedwithme');

// Route::get('group/{id}', 'Front\GroupController@group_view')->name('front.group_view');

});
