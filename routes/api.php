<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('postForgotPassword', 'API\ForgotPasswordController@postForgotPassword');
Route::post('GetCities', 'API\EventController@GetCities'); // created by Touqeer Hanif
Route::get('testmail', 'API\UserController@testmail');
Route::post('mainPageShowcase', 'Event\EventController@allShowcase');
Route::post('getPlans', 'API\UserController@getPlans');


Route::group(['middleware' => 'auth:api'], function(){
    //login user routes


    Route::post('acceptPlans', 'API\UserController@acceptPlans')->name('acceptPlans');
    Route::post('requestPlans', 'API\UserController@requestPlans')->name('requestPlans');
    Route::post('getUserPlans', 'API\UserController@getUserPlans')->name('getUserPlans');
    Route::post('uploadAvatar', 'API\UserController@uploadAvatar')->name('uploadAvatar');
    Route::post('editProfile', 'API\UserController@editProfile')->name('editProfile');
    Route::post('changePassword', 'API\UserController@changePassword')->name('changePassword');
    Route::post('locationUpdate', 'API\UserController@locationUpdate')->name('locationUpdate');
    Route::post('subscribe', 'API\UserController@subscribe')->name('subscribe');
    
    Route::post('registerid', 'API\UserController@registerid')->name('registerid');
    
    Route::post('userServicesStore', 'API\EventController@userServicesStore')->name('userServicesStore');

    //Route::post('addDevice', 'API\UserController@addDevice')->name('addDevice');
    Route::post('removeDevice', 'API\UserController@removeDevice')->name('removeDevice');


    //tickets
    Route::post('eventTickets', 'API\TicketController@eventTickets')->name('eventTickets');
    Route::post('generateeventTickets', 'API\TicketController@generateeventTickets')->name('generateeventTickets');
    Route::post('buyEventTickets', 'API\TicketController@buyEventTickets')->name('buyEventTickets');
    Route::post('confirmrevertEventTickets', 'API\TicketController@confirmrevertEventTickets')->name('confirmrevertEventTickets');
    Route::post('eventTicketsPoints', 'API\TicketController@eventTicketsPoints')->name('eventTicketsPoints');

   
    //event
    Route::get('eventType', 'API\EventController@eventType')->name('eventType');
    Route::get('eventPaymentType', 'API\EventController@eventPaymentType')->name('eventPaymentType'); // created by sohaib
    Route::get('eventTicketType', 'API\EventController@eventTicketType')->name('eventTicketType'); // created by sohaib
    
    Route::get('GetTitleVenues', 'API\EventController@GetTitleVenues')->name('GetTitleVenues'); // created by Touqeer Hanif
    Route::post('servicesGetByType', 'API\EventController@servicesGetByType')->name('servicesGetByType');
    Route::post('serviceProviderGetByType', 'API\EventController@serviceProviderGetByType')->name('serviceProviderGetByType');
    Route::post('servicesGetByServiceProvider', 'API\EventController@servicesGetByServiceProvider')->name('servicesGetByServiceProvider');
    Route::get('allEvent', 'API\EventController@allEvent')->name('allEvent');
    Route::get('myEvent', 'API\EventController@myEvent')->name('myEvent');
    Route::post('eventCreate', 'API\EventController@eventCreate')->name('eventCreate');
   // Route::post('event/edit', 'API\EventController@edit')->name('event/edit');
    Route::post('eventDelete', 'API\EventController@eventDelete')->name('eventDelete');
    Route::post('eventLikeDislike', 'API\EventController@eventLikeDislike')->name('eventLikeDislike');
    //share
    Route::post('selectGroup', 'API\EventController@selectGroup')->name('selectGroup');
    Route::post('selectUser', 'API\EventController@selectUser')->name('selectUser');
    Route::post('shareToGroup', 'API\EventController@shareToGroup')->name('shareToGroup');
    Route::post('shareToUser', 'API\EventController@shareToUser')->name('shareToUser');
    //comment
    Route::post('eventComment', 'API\EventController@eventComment')->name('eventComment');
    Route::post('getComment', 'API\EventController@getComment')->name('getComment');
    //event join qr code
    Route::post('eventJoin', 'API\EventController@eventJoin')->name('eventJoin');
    Route::post('eventJoinlist', 'API\EventController@eventJoinlist')->name('eventJoinlist');
    Route::post('eventQrScan', 'API\EventController@eventQrScan')->name('eventQrScan');
    Route::post('eventQrValidate', 'API\EventController@eventQrValidate')->name('eventQrValidate');
    Route::post('eventQrValidateAll', 'API\EventController@eventQrValidateAll')->name('eventQrValidateAll');
    Route::post('eventQrValidateClose', 'API\EventController@eventQrValidateClose')->name('eventQrValidateClose');
    Route::get('eventJoinReceviedRequest', 'API\EventController@eventJoinReceviedRequest')->name('eventJoinReceviedRequest');
    Route::post('eventAcceptRejectRequest', 'API\EventController@eventAcceptRejectRequest')->name('eventAcceptRejectRequest');


    //groups
    Route::post('allUsers', 'API\GroupController@allUsers')->name('allUsers');
    Route::get('groupTypes', 'API\GroupController@groupTypes')->name('groupTypes');
    Route::get('allGroups', 'API\GroupController@allGroups')->name('allGroups');
    Route::get('myGroups', 'API\GroupController@myGroups')->name('myGroups');
    Route::get('myJoinGroups', 'API\GroupController@myJoinGroups')->name('myJoinGroups');
    Route::post('createGroup', 'API\GroupController@store')->name('createGroup');
    Route::post('groupDelete', 'API\GroupController@groupDelete')->name('groupDelete');
    Route::post('memberAddInGroup', 'API\GroupController@memberAddInGroup')->name('memberAddInGroup');
    Route::post('groupMembers', 'API\GroupController@groupMembers')->name('groupMembers');
    Route::post('memberBlock', 'API\GroupController@memberBlock')->name('memberBlock');
   // Route::get('userReceviedGroupRequest', 'API\GroupController@userReceviedGroupRequest')->name('userReceviedGroupRequest');
    Route::post('userSendGroupRequest', 'API\GroupController@userSendGroupRequest')->name('userSendGroupRequest');
    Route::post('userAcceptRequest', 'API\GroupController@userAcceptRequest')->name('userAcceptRequest');
    Route::post('removeRequest', 'API\GroupController@removeRequest')->name('removeRequest');
    Route::post('acceptRejectRequest', 'API\GroupController@acceptRejectRequest')->name('acceptRejectRequest');
    Route::get('receviedRequest', 'API\GroupController@receviedRequest')->name('receviedRequest');
    Route::post('groupEvents', 'API\GroupController@groupEvents')->name('groupEvents');
    
    
    ////////////////////////////////////For New Design////////////////////
    ///
    Route::post('getRevenueDetail', 'Event\EventController@getRevenueDetail')->name('getRevenueDetail');
    Route::post('mainPageEvent', 'Event\EventController@allEvent')->name('mainPageEvent');
    Route::post('getEvent', 'Event\EventController@fetchEvent')->name('getEvent');
    Route::post('eventDetail', 'Event\EventController@eventDetail')->name('eventDetail');
    Route::get('newMyEvent', 'Event\EventController@myEvent')->name('newMyEvent');
    Route::post('search', 'Event\EventController@search')->name('search');
    
    Route::get('newAllGroups', 'Event\GroupController@allGroups')->name('newAllGroups');
    Route::get('newMyGroups', 'Event\GroupController@myGroups')->name('newMyGroups');
    Route::get('newMyJoinGroups', 'Event\GroupController@myJoinGroups')->name('newMyJoinGroups');
    Route::post('newGroupEvents', 'Event\GroupController@groupEvents')->name('newGroupEvents');
    Route::post('groupLeave', 'Event\GroupController@groupLeave')->name('groupLeave');


});




