<?php 

$url = url('resetpassword/'.$user->email.'/'.urlencode($code));	
?>
<h2>Reset Your Password</h2>
<p>To change your password,<a href="{{$url}}">click here.</a></p>
<p>Or point your browser to this address: <br />{{$url}}</p>
<p>Thank you!</p>