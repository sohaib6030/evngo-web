<?php 

$url = url('verify/account/'.$data->email.'/'.urlencode($code));	
?>
<h2>Verify Your Account</h2>
<p>To verify your account,<a href="{{$url}}">click here.</a></p>
<p>Or point your browser to this address: <br />{{$url}}</p>
<p>Thank you!</p>