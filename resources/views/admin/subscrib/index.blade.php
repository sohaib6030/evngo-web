@extends('layouts.mainlayout')
@section('content')
@if(session('success'))
    <script>
      $( document ).ready(function() {
        swal("Success", "{{session('success')}}", "success");
      });
      
    </script>
@endif
@if(session('failed'))
    <script>
      $( document ).ready(function() {
        swal("Failed", "{{session('failed')}}", "error");
      });
      
    </script>
@endif


<!-- Form end -->
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Subscription Price</h3>
              <div class="box-tools pull-right">
                    
                  </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" >
           

                    <div class="alert alert-danger alert-styled-left" style="display: none;" id="delete">
                         <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                         <p class="delete"></p>
                    </div>

                    <div class="alert alert-success alert-styled-left" style="display: none;" id="success">
                         <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                         <p class="success"></p>
                    </div>

              <div class="col-md-10">
                <h3>Subscription Price Form</h3>
                <div class="" style="height:20px;"></div>
              <form class="form-horizontal form" action="{{route('subscriptionPrice.store')}}" method="post"  id="add_form" enctype="multipart/form-data">
                  @csrf 

                   <div class="form-group">
                      <label for="" class="col-sm-3">VIP</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="vip" id="vip" placeholder="" value="{{$data->vip}}">
                        <span class="text-red">
                                <strong class="vip"></strong>
                        </span>
                      </div>
                    </div> 

                     <div class="form-group">
                      <label for="" class="col-sm-3">Service Provider</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="serviceProvider" id="serviceProvider" placeholder="" value="{{$data->serviceProvider}}">
                        <span class="text-red">
                                <strong class="serviceProvider"></strong>
                        </span>
                      </div>
                    </div> 

                     <div class="form-group">
                      <label for="" class="col-sm-3">Promoter</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="promoter" id="promoter" placeholder="" value="{{$data->promoter}}">
                        <span class="text-red">
                                <strong class="promoter"></strong>
                        </span>
                      </div>
                    </div> 

                 
                
              </div>

            </div>

              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="add_form_btn">Save</button>
              </div>
              <!-- /.box-footer -->
              </form>
    </div>

 

@endsection
@push('scripts')

<script type="text/javascript">
// code for add form
$('#add_form_btn').on('click', function(e) {
  //var data = $('#add_form').serializeArray();
   
  e.preventDefault();
  var data = $('#add_form')[0];
  var formData = new FormData(data);
  $.ajax({
  data: formData,
  type: $('#add_form').attr('method'),
  url: $('#add_form').attr('action'),
  processData: false,
  contentType: false,
  success: function(response)
  {
  if(response.errors)
  {
  $.each(response.errors, function( index, value ) {
    $("."+index).html(value);
    $("."+index).fadeIn('slow', function(){
      $("."+index).delay(3000).fadeOut(); 
    });
  });
  }
  else 
  {
        location.reload();
     
  }
  
  }
  });
}); 


</script>
@endpush