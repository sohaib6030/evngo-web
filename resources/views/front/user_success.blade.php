<!DOCTYPE html>
<html lang="en">
<head>
  <!-- ========== Meta Tags ========== -->
  <meta charset="UTF-8">
  <meta name="description" content="Evngo -Event Html Template">
  <meta name="keywords" content="Evngo , Event , Html, Template">
  <meta name="author" content="Evngo">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- ========== Title ========== -->
  <title> Evngo </title>
  <!-- ========== Favicon Ico ========== -->
  <!--<link rel="icon" href="fav.ico">-->
  <!-- ========== STYLESHEETS ========== -->
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/bootstrap.min.css') }}" />
  <!-- Fonts Icon CSS -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('public/front-end/css/et-line.css') }}" />
  <link rel="stylesheet" href="{{ asset('public/front-end/css/ionicons.min.css') }}" />
  <!-- Carousel CSS -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/owl.carousel.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('public/front-end/css/owl.theme.default.min.css') }}" />


  <!-- Animate CSS -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/animate.min.css') }}" />
  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/main.css') }}" />
</head>
<body>
<div class="loader">
  <div class="loader-outter"></div>
  <div class="loader-inner"></div>
</div>

<!--header end here-->

<!--cover section slider -->
<section id="home" class="home-cover">


  <div class="cover_slider owl-carousel owl-theme">


    <div class="cover_item" style="background-image: url({{asset('public/front-end/landing_page_images/bg/evngo.jpg')}})" >
      <div class="slider_content">
        <div class="slider-content-inner">
          <div class="container" style="margin-top: 15%">

            <div class="col-xs-12" style="text-align: center;margin: 0 auto">
              <img style="text-align: center;margin: 0 auto" class="img-responsive" width="300" src="{{ asset('public/front-end/landing_page_images/logo.png')}}" alt="evngo">
            </div>
            <div >


              <p class="cover-date" style="text-align: center">
                We Have Successfully Verified Your Account Please Login To Continue
              </p>
              <p class="cover-date" style="text-align: center">
                <a href="{{url('/user_panel')}}" class=" btn btn-primary btn-rounded" style="padding: 15px 27px;" >
                  Login Here
                </a>
              </p>


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

<!--footer end -->
<script src="{{ asset('public/front-end/js/popper.js') }}"></script>
<!-- jquery -->
<script src="{{ asset('public/front-end/landing_page_images/jquery.min.js') }}"></script>
<script src="{{ asset('public/front-end/landing_page_images/bootstrap.min.js') }}"></script>
<!-- bootstrap -->



<script src="{{ asset('public/front-end/js/waypoints.min.js') }}"></script>
<!--slick carousel -->

<script src="{{ asset('public/front-end/js/owl.carousel.min.js') }}"></script>
<!--parallax -->

<script src="{{ asset('public/front-end/js/parallax.min.js') }}"></script>
<!--Counter up -->

<script src="{{ asset('public/front-end/js/jquery.counterup.min.js') }}"></script>
<!--Counter down -->

<script src="{{ asset('public/front-end/js/jquery.countdown.min.js') }}"></script>
<!-- WOW JS -->

<script src="{{ asset('public/front-end/js/wow.min.js') }}"></script>
<!-- Custom js -->

<script src="{{ asset('public/front-end/js/main.js') }}"></script>
</body>
</html>