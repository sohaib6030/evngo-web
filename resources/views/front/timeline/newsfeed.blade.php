
@extends('front.layouts.front-layout')
@section('content')

<div id="page-contents">
      <div class="container">
        <div class="row">

          @include('front.sidebars.sidebar-1')
          
          <div class="col-md-7">

            <!-- Post Create Box
            ================================================= -->
   {{--          <div class="create-post">
              <div class="row">
                <div class="col-md-7 col-sm-7">
                  <div class="form-group">
                    <img src="http://placehold.it/300x300" alt="" class="profile-photo-md" />
                    <textarea name="texts" id="exampleTextarea" cols="30" rows="1" class="form-control" placeholder="Write what you wish"></textarea>
                  </div>
                </div>
                <div class="col-md-5 col-sm-5">
                  <div class="tools">
                    <ul class="publishing-tools list-inline">
                      <li><a href="#"><i class="ion-compose"></i></a></li>
                      <li><a href="#"><i class="ion-images"></i></a></li>
                      <li><a href="#"><i class="ion-ios-videocam"></i></a></li>
                      <li><a href="#"><i class="ion-map"></i></a></li>
                    </ul>
                    <button class="btn btn-primary pull-right">Publish</button>
                  </div>
                </div>
              </div>
            </div> --}}

            <!-- Post Create Box End-->

            <!-- Post Content
            ================================================= -->
            <div class="post-content">
              <div class="user-info evengo-user-info">
                <img src="http://placehold.it/300x300" alt="user" class="profile-photo-md pull-left" />
                <h5><a href="#" class="profile-link">Alexis Clark</a> {{-- <span class="following">following</span> --}}</h5>
                <p class="text-muted">Published a photo about 3 mins ago</p>
              </div>
              <div class="post-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.<i class="em em-anguished"></i> <i class="em em-anguished"></i> <i class="em em-anguished"></i></p>
                  </div>
              <div class="post-img">
                <img src="{{ asset('public/front-end/images/posts/post-1.jpg') }}" alt="post-image" class="img-responsive post-image" />
                <div class="post-img-detail-overlay">
                  <div class="d-content">
                    <span>Likes 10</span>
                    <span class="float-right">Comments 10</span>
                  </div>
                </div>
              </div>
              <div class="post-footer">
                <div class="sec">
                  <span><i class="icon ion-ios-heart liked"></i></span>
                  <span class="sec-text">Liked</span>
                </div>
                <div class="sec">
                  <span><i class="icon ion-chatbox-working"></i></span>
                  <span class="sec-text">Comment</span>
                </div>
                <div class="sec">
                  <span>
                    <i class="icon ion-ios-star start-checked"></i>
                    <i class="icon ion-ios-star start-checked"></i>
                    <i class="icon ion-ios-star start-checked"></i>
                    <i class="icon ion-ios-star start-checked"></i>
                    <i class="icon ion-ios-star"></i>
                  </span>
                </div>
              </div>
              
            </div>

          <!-- Post Content
            ================================================= -->
            <div class="post-content">
              <div class="user-info evengo-user-info">
                <img src="http://placehold.it/300x300" alt="user" class="profile-photo-md pull-left" />
                <h5><a href="#" class="profile-link">Nisar Ahmed</a> {{-- <span class="following">following</span> --}}</h5>
                <p class="text-muted">Published a photo about 3 mins ago</p>
              </div>
              <div class="post-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.<i class="em em-anguished"></i> <i class="em em-anguished"></i> <i class="em em-anguished"></i></p>
                  </div>
              <div class="post-img">
                <img src="http://placehold.it/1920x1160" alt="" class="img-responsive post-image" />
                <div class="post-img-detail-overlay">
                  <div class="d-content">
                    <span>Likes 10</span>
                    <span class="float-right">Comments 10</span>
                  </div>
                </div>
              </div>
              <div class="post-footer">
                <div class="sec">
                  <span><i class="icon ion-ios-heart liked"></i></span>
                  <span class="sec-text">Liked</span>
                </div>
                <div class="sec">
                  <span><i class="icon ion-chatbox-working"></i></span>
                  <span class="sec-text">Comment</span>
                </div>
                <div class="sec">
                  <span>
                    <i class="icon ion-ios-star start-checked"></i>
                    <i class="icon ion-ios-star start-checked"></i>
                    <i class="icon ion-ios-star start-checked"></i>
                    <i class="icon ion-ios-star"></i>
                    <i class="icon ion-ios-star"></i>
                  </span>
                </div>
              </div>
              
            </div>
</div>
          <!-- Newsfeed Common Side Bar Right
          ================================================= -->
          <div class="col-md-3 static">
            <div class="suggestions" id="sticky-sidebar">
              <h4>Upcoming Events</h4>

              <div class="follow-user sidebar-box">
                <span class="date-circle-sm pull-left">
                  <div>
                    <span class="d-day">07</span>
                     <span class="d-month">Aug</span>
                  </div>
                </span>
                <div>
                  <h5><a href="#">Diana Amber</a></h5>
                  <a href="#" class="sub-text">Location</a>
                  <h4>$ 250</h4>
                </div>
              </div>
              <div class="follow-user sidebar-box">
                <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm pull-left" />
                <div>
                  <h5><a href="#">Diana Amber</a></h5>
                  <a href="#" class="sub-text">Location</a>
                  <h4>$ 250</h4>
                </div>
              </div>
              <div class="follow-user sidebar-box">
                <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm pull-left" />
                <div>
                  <h5><a href="#">Diana Amber</a></h5>
                  <a href="#" class="sub-text">Location</a>
                  <h4>$ 250</h4>
                </div>
              </div>
              
             
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
