<!DOCTYPE html>
<html lang="en">
<head>
  <!-- ========== Meta Tags ========== -->
  <meta charset="UTF-8">
  <meta name="description" content="Evngo -Event Html Template">
  <meta name="keywords" content="Evngo , Event , Html, Template">
  <meta name="author" content="Evngo">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- ========== Title ========== -->
  <title> Evngo </title>
  <!-- ========== Favicon Ico ========== -->
  <!--<link rel="icon" href="fav.ico">-->
  <!-- ========== STYLESHEETS ========== -->
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/bootstrap.min.css') }}" />
  <!-- Fonts Icon CSS -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('public/front-end/css/et-line.css') }}" />
  <link rel="stylesheet" href="{{ asset('public/front-end/css/ionicons.min.css') }}" />
  <!-- Carousel CSS -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/owl.carousel.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('public/front-end/css/owl.theme.default.min.css') }}" />


  <!-- Animate CSS -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/animate.min.css') }}" />
  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="{{ asset('public/front-end/css/main.css') }}" />
</head>
<body>
<div class="loader">
  <div class="loader-outter"></div>
  <div class="loader-inner"></div>
</div>

<!--header end here-->

<!--cover section slider -->
<section id="home" class="home-cover">


  <div class="cover_slider owl-carousel owl-theme">


    <div class="cover_item" style="background-image: url({{asset('public/front-end/landing_page_images/bg/evngo.jpg')}})" >
      <div class="slider_content">
        <div class="slider-content-inner">
          <div class="container">

            <div class="col-xs-12">
              <img class="img-responsive" width="300" src="{{ asset('public/front-end/landing_page_images/logo.png')}}" alt="evngo">
            </div>
            <div class="slider-content-left">
              <h2 class="cover-title">
                All-in-One Event Management Platform
              </h2>
              <strong class="cover-xl-text">EVNGO</strong>
              <p class="cover-date">
                Create public or private events, amplify event awareness, manage promotions, ticketing and engage attendees all in one place. We offer credits as a special cash to our users for marketing and advertising activities.
              </p>
              <div style="text-align: center">
                <a href="/user_panel" class=" btn btn-primary btn-rounded" >
                  Login
                </a>
                <a href="/user_panel_register" class=" btn btn-primary btn-rounded" >
                  Register
                </a>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- Login Section End -->
<!--about the event -->
<section class="pt100 pb100">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section_title">
          <h3 class="title">
            The start of a new era
          </h3>
          <p>
            Evngo vastly differs to any other event management app on the market, its unique design is redefining a new era of social order, providing a platform for passionate individuals to embrace the art of event management and design.

            Users now have the potential to construct, promote and run their own events, with financial data, promotions, security, venue, ticketing and all the other stressful and time consuming requirements taken care of, the user can relax and concentrate on important commodities such as pioneering ambitious events or parties of significance.
          </p>

        </div>
      </div>
      <div class="col-12">
        <div class="section_title">
          <h3 class="title">
            Our Mission
          </h3>
          <p>
            Our team follows a Customer Centric Approach. As well as providing users with a platform for accessing events far or nearby, we also focus on providing solutions for the obstacles experienced by event organisers.

            Organisers will have access to an expansive array of tools designed to help achieve an events true potential. Its Evngos mission to help increase event awareness, provide the market for promotions and advertisement, arrange staff and security if required, provide live data reports (LDX) of ticket sales, employees and attended patrons. Also for the first time engaging with the crowd with live communications.
          </p>

        </div>
      </div>
    </div>
    <!--event features-->
    <div class="row justify-content-center mt30">
      <h3 class="title">
        EVNGO EVENT MANAGEMENT SOFTWARE FEATURES
      </h3>
      <div class="col-12">
        <div class="icon_box_one">
          <i class="lnr lnr-mic"></i>
          <div class="content">
            <h4>Manage your event in one place</h4>
            <p class="content-p">
              The EVNGO platform gives organisers the game changing tool,
              live data exchange (LDX). This software is a breathe of fresh air,
              it will give organisers that needed sense of security where they can monitor and manage their events from one place.
              Some perks of LDX are the number of tickets sold/ remaining, the number of attended and unattended patrons (Live pass out pass ins),
              profit charts and graphs, communication system with employees and their payrolls and even a QR CODE based
              time management sheet where musicians and employees scan on and off. LDX is an organisers right hand man monitoring everything without an
              organiser even being there
            </p>
          </div>
        </div>
      </div>

      <div class="col-12 col-md-4 col-lg-4">
        <div class="icon_box_one">
          <i class="lnr lnr-rocket"></i>
          <div class="content">
            <h4>Increase Event Attendance</h4>
            <p class="content-p">
              Organise can increase their event attendance and sell more tickets with amazing multi-channel marketing campaigns, Evngo’s latest marketing tools or our experienced marketing team.
            </p>
          </div>
        </div>
      </div>

      <div class="col-12 col-md-4 col-lg-4">
        <div class="icon_box_one">
          <i class="lnr lnr-bullhorn"></i>
          <div class="content">
            <h4>Rewards Program </h4>
            <p class="content-p">
              Users can either purchase or be rewarded points,which can be used for special privileges such as discounted tickets
                  ,backstage passes,upgraded tickets,complementary drinks, plus ones, complementary promotional items and so on.
            </p>
          </div>
        </div>
      </div>

      <div class="col-12 col-md-4 col-lg-4">
        <div class="icon_box_one">
          <i class="lnr lnr-clock"></i>
          <div class="content">
            <h4>Promotional goods & services</h4>
            <p class="content-p">
              Organisers will have access to countless promotional goods and services enticing users to give their events the needed boost and WOW factor.
            </p>
          </div>
        </div>
      </div>
    </div>
    <!--event features end-->
  </div>
</section>
<!--about the event end -->


<!--planning software-->
<section class="bg-img pt100 pb100" style="background: linear-gradient(to right, rgba(0,0,0, 0.7) , rgba(0,0,0, 0.7)),url({{ asset('public/front-end/landing_page_images/bg/bg-img.png')}}) fixed no-repeat;">
  <div class="container">
    <div class="section_title mb50">
      <h3 class="title" style="color:#fff;">
        EVNGO Event planning software handles everything from start to finish
      </h3>
      <p style="text-align: center;color:#fff;">
        Users can automate the their event planning process with <strong>EVNGO Event Mangement Software.</strong> Start with creating event, managing tickets and attendess with custom dashboards and event promotions. We have everything covered.
      </p>
    </div>

    <div class="row justify-content-center">
      <div class="col-md-4 col-12">
        <div class="price_box1">

          <div class="price_header">
            <h4>
              CREATE THE PERFECT EVENT
            </h4>

          </div>

          <div class="price_features ">
            <ul>
              <li>
                Easily Manage all your events with LDX with just a click of a
                click of a button
                Increase attendance with promotional contrivance and intuitive
                advertising
                Engage with attendees like never before with reward schemes
                live feedback and promotional goods.
                Promote your events with numerous onboard promoters and
                partners.
                Feedback taken seriously for future developments.
              </li>

            </ul>
          </div>

        </div>
      </div>
      <div class="col-md-4 col-12">
        <div class="price_box1">
          <div class="price_header">
            <h4>
              Live Communications
            </h4>

          </div>
          <div class="price_features">
            <ul>
              <li>
                Engage with the patrons and create a sense of importance by notifying them off rewards, lineups or any sudden changes, general updates or empathic apologies and warnings of present dangers if required or just a simple welcome message on arrival could make the difference between them coming back or not next time.

              </li>

            </ul>
          </div>

        </div>
      </div>
      <div class="col-md-4 col-12">
        <div class="price_box1">
          <div class="price_header">
            <h4>
              Custom QR code scanning
            </h4>

          </div>
          <div class="price_features">
            <ul>
              <li>
                Unique QR codes are embedded into every ticket, once purchased by the users they will automatically receive it in the app and email with sophisticated ticket scanning technology and inbuilt encryption it protects the users and vendors from mischievous hacks. We also provide users with last minute purchases at the door.
              </li>

            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--planning software end -->

<!--Price section-->
<section class="pb100">
  <div class="container">
    <div class="section_title mb50">
      <h3 class="title">
        MEMBERSHIP of EVNGO EVENT MANAGEMENT PLATFORM
      </h3>
    </div>

    <div class="row justify-content-center">
      <div class="col-md-4 col-12">
        <div class="price_box active d-flex flex-column">
          <div class="price_highlight">
            recommended
          </div>
          <div class="price_header">
            <h4>
              VIP SILVER USERS
            </h4>

          </div>

          <div class="price_features content-p price">
            <ul>
              <li>
               Search for the latest nearby and upcoming events.</li>

              <li>Receive the latest news about upcoming popular events and one of underground parties.</li>
              <li>Have access to exclusive VIP invite only private events and feel like a celebrity.</li>
              <li>Host your own party like a real rockstar.</li>
              <li>Have Evngo do all the hard work with our database synching software, only displaying recommended events selected to your taste.</li>
              <li>Be rewarded with Evngo’s loyalty rewards program with membership upgrades, discounts, exclusive invites, complementary credits and promotional gifts.</li>
              <li>Membership upgrades to VIP Gold and VIP Black.
              </li>
            </ul>
          </div>

        </div>
      </div>
      <div class="col-md-4 col-12">
        <div class="price_box">
          <div class="price_header">
            <h4>
              ORGANISERS
            </h4>
          </div>
          <div class="price_features content-p price">
            <ul>

              <li>Be the talk amongst patrons as your cities greatest Event Organiser</li>

              <li>Unlimited access to promotional oﬀers from partners.</li>

              <li>Advertise, promote and share with our vast array of tools.

              </li>
            </ul>
          </div>

        </div>
      </div>
      <div class="col-md-4 col-12">
        <div class="price_box">
          <div class="price_header">
            <h4>
              PARTNERS
            </h4>
          </div>
          <div class="price_features content-p">
            <ul class="price">

              <li>Company's looking to promote their goods can connect with organisers to secure contracts and agreements.</li>

              <li>Can create events for promotional reasons.</li>
              <li>Can supply EVNGO with services and goods.

                Contact Evngo directly to enquire about partnership opportunities at 'partners@Evngo.com.au'

              </li>
            </ul>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
<!--price section end -->

<!--evngo points section -->
<section class="pt100 pb100">
  <div class="container">
    <div class="section_title mb50">
      <h3 class="title">
        Loyalty Rewards Program
      </h3>
    </div>

    <div class="row justify-content-center">

      <div class="container pt100 pb100">
        <div class="row justify-content-center">
          <div class="col-12 col-md-12">

            <p>

              We are excited to Introduce Evngo loyalty rewards program, now users can be rewarded for:<br>
            </p>
            <ul>
              <li>
                Buying tickets
              </li>
              <li>
                Attending events early
              </li>
              <li>
                Sharing content with friends </li>

              <li>Creating events </li>

              <li>Promoting events </li>

              <li>Recommending and sharing the evngo platform on all sorts of social media </li>

              <li>Registering and verifying users profile </li>

              <li>Joining events

              </li>
            </ul>
            <p>
              and so on. Once membership is upgraded the ratio of points earned will be increased and the rewards will become more generous, users loyalty will not go unnoticed and will be favourably rewarded.
            </p><br>
          </div>
          <div class="col-12 col-md-12">

            <ul>
              <li>
                Organisers can purchase loyalty points to entice vip users rewarding them with free drink cards on arrival, back stage passes, meet and greet with artists, no waiting in line straight in straight out, free competition entries and so much more.
              </li>

            </ul>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--evngo points section end -->

<!--get tickets section -->
<section class="bg-img pt100 pb100" style="background-image: url({{ asset('public/front-end/landing_page_images/bg/tickets.png')}});">
  <div class="container">
    <div class="section_title mb30 text-center">
      <h3 class="title color-light">
        Download the App
      </h3>
    </div>
    <div class="row justify-content-center align-items-center">
      <div class="col-md-9 text-md-left text-center color-light">
        Download the EVNGO apps to create the events, manage attendees and handle event promotions. Access EVNGO Event Management system via your smartphone and start getting notifications about your attendees, created events, sold tickets and promotions.
      </div>

      <div class="row text-center">

        <a href="#" class="btn btn-store">
          <span class="fa fa-apple fa-3x pull-left"></span>
          <span class="btn-label">Download on the</span>
          <span class="btn-caption">App Store</span>
        </a>
        <a href="#" class="btn btn-store">
          <span class="fa fa-android fa-3x pull-left"></span>
          <span class="btn-label">Download on the</span>
          <span class="btn-caption">Google Play</span>
        </a>

      </div>

    </div>
  </div>
</section>
<!--get tickets section end-->

<!--footer start -->
<footer>
  <div class="container">
    <div class="row justify-content-center">

      <div class="col-md-4 col-12">
        <div class="footer_box">
          <div class="footer_header">
            <div class="footer_logo">
              <img src="{{ asset('public/front-end/landing_page_images/logo.png')}}" alt="evngo">
            </div>
          </div>
          <div class="footer_box_body">
            <p>
              Friend Finder is a social network template that can be used to connect people. The template offers Landing pages, News Feed, Image/Video Feed, Chat Box, Timeline and lot more.
            </p>

          </div>
        </div>
      </div>

      <div class="col-12 col-md-4">
        <div class="footer_box">
          <div class="footer_header">
            <h4 class="footer_title">
              Contact Us
            </h4>
          </div>
          <div class="footer_box_body">
            <p>
              Evngo
              122 Albert St, Port Melbourne, VIC 3207
              Australia
              Tel: (03) 9646 1960
              Phone: (03) 9646 5253
            </p>
            <ul class="footer_social">
              <li>
                <a href="#"><i class="ion-social-pinterest"></i></a>
              </li>
              <li>
                <a href="#"><i class="ion-social-facebook"></i></a>
              </li>
              <li>
                <a href="#"><i class="ion-social-twitter"></i></a>
              </li>
              <li>
                <a href="#"><i class="ion-social-dribbble"></i></a>
              </li>
              <li>
                <a href="#"><i class="ion-social-instagram"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-12 col-md-4">
        <div class="footer_box">
          <div class="footer_header">
            <h4 class="footer_title">
              subscribe to our newsletter
            </h4>
          </div>
          <div class="footer_box_body">
            <div class="newsletter_form">
              <input type="email" class="form-control" placeholder="E-Mail here">
              <button class="btn btn-rounded btn-block btn-primary">SUBSCRIBE</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<div class="copyright_footer">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6 col-12">
        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
      </div>
      <div class="col-12 col-md-6 ">
        <ul class="footer_menu">
          <li>
            <a href="#">Home</a>
          </li>
          <li>
            <a href="#">Speakers</a>
          </li>
          <li>
            <a href="#">Events</a>
          </li>
          <li>
            <a href="#">News</a>
          </li>
          <li>
            <a href="#">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--footer end -->
<script src="{{ asset('public/front-end/js/popper.js') }}"></script>
<!-- jquery -->
<script src="{{ asset('public/front-end/landing_page_images/jquery.min.js') }}"></script>
<script src="{{ asset('public/front-end/landing_page_images/bootstrap.min.js') }}"></script>
<!-- bootstrap -->



<script src="{{ asset('public/front-end/js/waypoints.min.js') }}"></script>
<!--slick carousel -->

<script src="{{ asset('public/front-end/js/owl.carousel.min.js') }}"></script>
<!--parallax -->

<script src="{{ asset('public/front-end/js/parallax.min.js') }}"></script>
<!--Counter up -->

<script src="{{ asset('public/front-end/js/jquery.counterup.min.js') }}"></script>
<!--Counter down -->

<script src="{{ asset('public/front-end/js/jquery.countdown.min.js') }}"></script>
<!-- WOW JS -->

<script src="{{ asset('public/front-end/js/wow.min.js') }}"></script>
<!-- Custom js -->

<script src="{{ asset('public/front-end/js/main.js') }}"></script>
</body>
</html>

<style>
  .content-p{
    text-align: left;
  }

  .price_box .price_features ul li {

    text-align: left;
  }

  .price_box {
    min-height: 780px;
  }

</style>