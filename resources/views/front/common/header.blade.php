    <!-- Header
    ================================================= -->
    <header id="header">
      <nav class="navbar navbar-default navbar-fixed-top menu">
        <div class="container">

          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header" id="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ asset('public/front-end/images/evngo-logo-512.png')  }}"  height="50" alt="logo" /></a>

          </div>
          <div class="navbar-header" style="
    margin-top: 20px;
    margin-left: 20px;
    font-size: 16px;
    font-weight: 900;
    color: #fff;
">
          <span>Available Points : <strong id="pointvalue"><points></points></strong> </span></div>


          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right main-menu evengo-dropdown">
              {{-- <li class="dropdown header-icon"><i class="icon ion-ios-people"></i></li> --}}
              {{-- <li class="dropdown header-icon"><i class="icon ion-ios-chatboxes-outline"></i></li> --}}

              <li class="dropdown">
                <a href="#" class="dropdown-toggle pages profile-menu-icon" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="txt-profile">{{ auth()->user()->fname }} &nbsp{{ auth()->user()->lname }}</span> <span><img src="{{ asset('public/front-end/images/300x300.png') }}" id="profile-icon-img" alt="" class="img-responsive profile-menu-photo"></span></a>
                <ul class="dropdown-menu page-list">
                  <li><a href="{{ route('setting.profile') }}">Profile</a></li>
                  
                <li><a href="{{ route('m.subscribe') }}">Premium Membership</a></li>
                <li><a href="#" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">Logout</a>
                    <form id="frm-logout" action="{{ route('user_logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        <input type="hidden" name="logout" value="1">
                    </form>
                </li>
                </ul>
              </li>              

            </ul>
         <!--   <form action="/search" class="navbar-form navbar-right hidden-sm" method="get" id="qsearchform">
              <div class="form-group">

                <a data-toggle="tab" href="#groups">

                    <input type="text" name="q" id="qsearch" class="form-control" placeholder="Search....">



              </div>
            </form>   -->
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
      </nav>

<!--<script type="application/javascript">


    if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
    {
      var search = document.getElementById("qsearchform");
      search.action = search.action.replace('localhost','localhost/evngo')
    }

    var pageURL = window.location.href;
    var lastURLSegment = pageURL.substr(pageURL.lastIndexOf('/') + 1);
    var search = document.getElementById("qsearch");
    search.style.display = "none";
    debugger
    if( (lastURLSegment == 'events') || (lastURLSegment == 'groups') || (lastURLSegment == 'search'))
    {
        search.style.display = "block";
    }
    console.log(search)

</script>    -->



    </header>
    <!--Header End-->