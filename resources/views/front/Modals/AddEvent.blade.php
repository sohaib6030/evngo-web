
<!-- Modal -->
<div id="EventModel" class="modal fade" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add an Event</h4>
      </div>
      <div class="modal-body">
        <form role="form">
          <div class="form-group">
            <label for="title">Title</label>
            <input v-modal="eventForm.title" type="text" class="form-control" id="title" placeholder="Title">
          </div>
          <div class="form-group">
            <label for="venue">Venue</label>
            <input v-modal="eventForm.venue" type="text" class="form-control" id="venue" placeholder="Enter Venue">
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea v-modal="eventForm.description" name="description" id="" style="width: 100%;" rows="3"></textarea>
          </div>
          <div class="form-group">
            <label>Select Date</label>
            <input v-modal="eventForm.date" type="text" class="form-control" name="date" id="date" placeholder="Date">
          </div>
          <div class="form-group">
            <label>Choose Image</label>
            <input v-modal="eventForm.image" type="file" class="form-control" name="image" placeholder="Date">
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button class="btn btn-default">Save</button>
      </div>
    </div>

  </div>
</div>