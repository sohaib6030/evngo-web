  <div class="post-content">
    <div class="user-info evengo-user-info">
      <img src="{{ asset('public/front-end/images/300x300.png') }}" alt="user" class="profile-photo-md pull-left" />
      <h5><a href="#" class="profile-link">Event 1</a> {{-- <span class="following">following</span> --}}</h5>
      <p class="text-muted">Published a photo about 3 mins ago</p>
    </div>
    <div class="post-text">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.<i class="em em-anguished"></i> <i class="em em-anguished"></i> <i class="em em-anguished"></i></p>
        </div>
    <div class="post-img">
      <img src="{{ asset('public/front-end/images/posts/post-1.jpg') }}" alt="post-image" class="img-responsive post-image" />
      <div class="post-img-detail-overlay">
        <div class="d-content">
          <span>Likes 10</span>
          <span class="float-right">Comments 10</span>
        </div>
      </div>
    </div>
    <div class="post-footer">
      <div class="sec">
        <span><i class="icon ion-ios-heart liked"></i></span>
        <span class="sec-text">Liked</span>
      </div>
      <div class="sec">
        <span><i class="icon ion-chatbox-working"></i></span>
        <span class="sec-text">Comment</span>
      </div>
      <div class="sec">
        <span>
          <i class="icon ion-ios-star start-checked"></i>
          <i class="icon ion-ios-star start-checked"></i>
          <i class="icon ion-ios-star start-checked"></i>
          <i class="icon ion-ios-star start-checked"></i>
          <i class="icon ion-ios-star"></i>
        </span>
      </div>
    </div>
    
  </div>