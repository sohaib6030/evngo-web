@extends('front.layouts.front-layout')
@section('content')
<!-- Page Title Section
================================================= -->
<div class="page-title-section faq" style="background:linear-gradient(to right, rgba(0,0,0, 0.7) , rgba(0,0,0, 0.7)), url('{{ asset('public/front-end/images/evengo-background.jpg')  }}') fixed no-repeat;background-size: 100%;background-position: bottom center;
    background-size: cover;
">
  <div class="container">
    <div class="headline">
      <h1 class="title">Get Membership?</h1>
    </div>
    
  </div>
  </div><!-- .page-header faq -->
  <div id="page-contents">
    <div class="container ">
      <!-- FAQ Menu
      ================================================= -->
      <ul class="nav nav-tabs faq-cat-list">
        <li class="evn-sub-li">
          <span class="subs-item" href="#faq_cat_1" data-toggle="tab">
            <div class="type" style="background-color: #4b1b7d;">VIP User</div>
            <div class="type">15$</div>
            <p style="font-size: 16px;">Subscribe to Launch new events, groups, directly communicate with vendors & service providers and avail much more features.</p>
            <div class="text-center">

@php
$vip = App\models\UserType::where('type','VIP')->where('user_id',auth()->user()->id)->first();

@endphp

              @if($vip)
                <span class="btn btn-evngo-primary btn-subcribe" data-type="VIP" data-amount="15$" :data-status=" subscribed_status('VIP')">@{{ subscribed_status('VIP') }}</span>

              @else
                <div  class="posts-inner-wrap" id="paymentmembership"></div>
              @endif

</div>




</span>
</li>

{{-- <li class="evn-sub-li">
<span class="subs-item" href="#faq_cat_1" data-toggle="tab">
<div class="type" style="background-color: #4b1b7d;">Service Provider</div>
<div class="type">30$</div>
<p style="font-size: 16px;">Subscribe to offer services you're proficient in and earn better ranking for events hosted by you, directly communicate with VIP users including much more features.</p>
<div class="text-center">
<span class="btn btn-evngo-primary btn-subcribe" data-type="ServiceProvider" data-amount="30$" :data-status=" subscribed_status('ServiceProvider')">@{{ subscribed_status('ServiceProvider') }}</span>
</div>
</span>
</li>

<li class="evn-sub-li">
<span class="subs-item" href="#faq_cat_1" data-toggle="tab">
<div class="type" style="background-color: #4b1b7d;">Promoter</div>
<div class="type">50$</div>
<p style="font-size: 16px;">Subscribe to promote events created by VIP Users & earn your commission by easily doing what you're expert in, including much more features.</p>
<div class="text-center">
<span class="btn btn-evngo-primary btn-subcribe" data-type="Promoter" data-amount="50$" :data-status=" subscribed_status('Promoter')">@{{ subscribed_status('Promoter') }}</span>
</div>
</span>
</li> --}}

</ul>

<subscribe-modal ></subscribe-modal>
{{--   <ul class="nav nav-tabs faq-cat-list">
<li>
<a href="{{ route('m.subscribe.type',['type' => 2]) }}">VIP User:
<div>15$</div>

</a>
</li>
<li>
<a href="#">Service Provider
<div>59.99$</div>

</a>
</li>
<li><a href="#">Promoter
<div>99$</div>

</a>
</li>
</ul> --}}

</div>
</div>

@endsection

@section('scripts')
<script>
jQuery(document).ready(function(){




//debugger
////////// gateway button  //////////////
var amount = 15 * 100
var inv = Math.random().toString(16);
inv = inv.replace(".","")
var callbackurl = window.location.origin + window.location.pathname + "?inv=" + inv.replace(".","")

//alert(amount)
var elem = $('#paymentmembership')

elem.append("<script src='https://secure.ewaypayments.com/scripts/eCrypt.min.js' " +
"class='eway-paynow-button' " +
"data-publicapikey='epk-1CCB4E43-C2E8-4181-B330-EF5138F3BBEC' " +
"data-amount='" + amount + "' " +
"data-currency='AUD' " +
"data-buttoncolor='#ffffff'" +
"data-buttonerrorcolor='#880c18'" +
"data-buttonprocessedcolor='#5e2b94'" +
"data-buttondisabledcolor='#ababab'" +
"data-buttontextcolor='#5e2b94'" +
"data-invoicedescription='Membership Purchase'" +
"data-invoiceref='" + inv + "'" +
"data-email='" + window.auth_user.email + "'" +
"data-phone='0000 00 00 00'" +
"data-label='Proceed Membership Payment'" +
"data-resulturl='" + callbackurl +"' > <\/script>");


    if(document.location.search.length) {
        debugger
        var q =  document.location.search

        jQuery('#sub_type').val('VIP');
        jQuery('#sub_token').val(q);
        jQuery('#send_data').click();
     //   alert($('meta[name="csrf-token"]').attr('content'))

     /*   $.ajax({
            url: window.base_path + '/api/subscribe',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhrFields: {
                withCredentials: true
            },
            data: {
                _token: CSRF_TOKEN,
                type:'VIP',
                stripe_token:q
            },
            contentType: "application/json; charset=utf-8",
            dataType : "json",
            success: function(data)
            {
                window.location =  window.base_path +'/events'
            },
            error: function(){},
        });  */

    } else {
        // no query string exists
    }




jQuery('.btn-subcribe').click(function(){
/*if(jQuery(this).data('status') != 'Subscribed'){
window.subtype_selected = jQuery(this).data('type');
jQuery('#sub_total_amount').val(window.sub_total_amount);
jQuery('#payModelsub').modal('show');
}*/
});
});
</script>



@endsection