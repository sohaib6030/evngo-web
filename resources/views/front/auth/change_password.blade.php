<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Evengo') }}</title>
  <link rel="icon" href="" type="image/png">

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="This is social network html5 template available in themeforest......" />
	<meta name="keywords" content="Social Network, Social Media, Make Friends, Newsfeed, Profile Page" />
	<meta name="robots" content="index, follow" />
	<title>Friend Finder | A Complete Social Network Template</title>

    <!-- Stylesheets
    ================================================= -->
		<link rel="stylesheet" href="{{ asset('public/front-end/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('public/front-end/css/style.css') }}" />
		<link rel="stylesheet" href="{{ asset('public/front-end/css/ionicons.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/front-end/css/font-awesome.min.css') }}" />
    
    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">
    
    <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('public/front-end/images/fav.png') }}"/>
	</head>
	<body>


    <!-- Landing Page Contents
    ================================================= -->
    <div id="lp-register" style="background:linear-gradient(to right, rgba(0,0,0, 0.7) , rgba(0,0,0, 0.7)), url('{{ asset('public/front-end/images/evengo-background.jpg')  }}') fixed no-repeat;background-size: 100%;">
    	<div class="container wrapper">
        <div class="row">
        	<div class="col-sm-5">
            <div class="intro-texts">
            	<h1 class="text-white">Make Cool Friends !!!</h1>
            	<p>Friend Finder is a social network template that can be used to connect people. The template offers Landing pages, News Feed, Image/Video Feed, Chat Box, Timeline and lot more. <br /> <br />Why are you waiting for? Buy it now.</p>
              <button class="btn btn-primary">Learn More</button>
            </div>
          </div>
        	<div class="col-sm-6 col-sm-offset-1">
            <div class="reg-form-container"> 
            
              <div class="tab-content">
              
                <div class="tab-pane active" id="email_confirmation">
                  <h3>Change Password</h3>
                  @if(session('message'))
                    @if(session('message')['success'] == 0)
                      <div class="error">{{ session('message')['message'] }}</div>
                    @endif  
                  @endif
                  <!--Login Form-->
                  <form action="{{ route('reset_new_password') }}" method="post">
                     {{csrf_field()}}
                     <div class="row">
                      <div class="form-group col-xs-12">
                        <label for="password" class="sr-only">New Password</label>
                        <input id="password" class="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="Password"/ required="true">
                        @if($errors->has('password'))
                            <div class="error">{{ $errors->first('password') }}</div>
                        @endif
                      </div>              
                    </div>

                    <div class="row">
                      <div class="form-group col-xs-12">
                        <label for="password" class="sr-only">Confirm Password</label>
                        <input id="password" class="form-control input-group-lg" type="password" name="password_confirmation" title="Enter password" placeholder="Confirm Password"/ required="true">
                        @if($errors->has('password_confirmation'))
                            <div class="error">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                      </div>
                    </div>

                    <input type="hidden" name="email" value="{{ $user->email }}">
                    <input type="hidden" name="token" value="{{ $token }}">

                    <p>Login? <a href="#">Login</a></p>
                     <button type="submit" class="btn btn-primary">Confirm</button>
                  </form><!--Login Form Ends--> 
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-sm-offset-6">
          
            <!--Social Icons-->
            <ul class="list-inline social-icons">
              <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
              <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
              <li><a href="#"><i class="icon ion-social-googleplus"></i></a></li>
              <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
              <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!--preloader-->
    <div id="spinner-wrapper">
      <div class="spinner"></div>
    </div>

    <!-- Scripts
    ================================================= -->
    <script src="{{ asset('public/front-end/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('public/front-end/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/front-end/js/jquery.appear.min.js') }}"></script>
		<script src="{{ asset('public/front-end/js/jquery.incremental-counter.js') }}"></script>
    <script src="{{ asset('public/front-end/js/script.js') }}"></script>
    
	</body>
</html>
