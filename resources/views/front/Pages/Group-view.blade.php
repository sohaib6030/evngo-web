
@extends('front.layouts.front-layout')
@section('content')

<div id="page-contents">
      <div class="container">
        <div class="row">

          @include('front.sidebars.sidebar-1')
          
          <div class="col-md-10">


            <!-- Post Content
            ================================================= -->
            {{-- <div class="page-inner-header">
              <div></div>
              <div>
                <a href="#" data-toggle="modal" data-target="#EventModel">Create Group</a>
                <a href="#" data-toggle="modal" data-target="#GroupModal">Create Group</a>
                
              </div>
            </div> --}}

      <div class="timeline-cover" style="background:url('{{ $response['image']  }}') no-repeat;background-size: cover;background-position: inherit;">

          <!--Timeline Menu for Large Screens-->
          <div class="timeline-nav-bar hidden-sm hidden-xs" style="background: #5a2591; height: 50px; ">
            <div class="row">
              <div class="col-md-7">
               <ul class="list-inline profile-menu" style="margin-top: 10px; margin-left: 5px">
                  <li style="color: #fff;
            font-size: 18px;
            padding-left: 20px;" class="btn btn-xs btn-primary">


                      <router-link to="/group/view/{{ $response['id'] }}/">{{ $response['name'] }}</router-link>

                  </li>
                          
                </ul>
              </div>


                <div class="col-md-5">

                    <ul class="list-inline profile-menu" style="float:right; margin-top: 15px; margin-right: 5px">
                  <!--<li><router-link to="/group/view/{{ $response['id'] }}/groupmembers" style="color: white">Members</router-link></li>-->


                      <!--<li class="btn btn-xs btn-primary btn-member" href="#" data-toggle="modal" data-target="#MemberModel">Members</li>-->
                      <li class="btn btn-xs btn-primary btn-member">Members</li>

                      @if($response['my_group'] == 1)
                        <li class="btn btn-xs btn-primary btn-invite">Invite</li>
                      @endif

                        <!--<li><router-link to="/group/view/{{ $response['id'] }}/members" style="color: white">Invite</router-link></li>-->

                  {{-- <li><a href="#" class="active">About</a></li> --}}
                  {{-- <li><a href="#">Album</a></li> --}}
                  {{-- <li style="color: #fff;padding-right: 20px;">{{ $response['totalMember'] }} members</li> --}}
                </ul>
                <ul class="follow-me list-inline">
                  
                  {{-- <li><button class="btn-primary">Add Friend</button></li> --}}
                </ul>

              </div>

                <groupmember-modal ref="groupmember" :group-id="{{ $response['id'] }}"></groupmember-modal>
                <invitemember-modal ref="invitemember" :group-id="{{ $response['id'] }}"></invitemember-modal>


            </div>
          </div><!--Timeline Menu for Large Screens End-->

          <!--Timeline Menu for Small Screens-->
          <div class="navbar-mobile hidden-lg hidden-md">
            <div class="profile-info">
              <img src="http://placehold.it/300x300" alt="" class="img-responsive profile-photo">
              <h4>{{ $response['name'] }}</h4>
            </div>
            <div class="mobile-menu">
              <ul class="list-inline">
                <li><router-link to="/group/view/{{ $response['id'] }}/members">Members</router-link></li>
                
              </ul>
           
            </div>
          </div><!--Timeline Menu for Small Screens End-->

        </div>

        <div class="row">
          <div class="col-md-12">
            
            <router-view :group-post="true" :group-id="{{ $response['id'] }}"></router-view>
            <div id="posts-wrapper" style="padding-top: 20px">

            
            </div>
            
          </div>
          <!--<div class="col-md-4">
              <invite-members-group :group-id="{{ $response['id'] }}"></invite-members-group>
          </div>-->
        </div>
            
   

      </div>
        
          
        </div>

      </div>
    </div>
  
    {{-- Modals --}}
    {{-- @include('front.Modals.AddEvent') --}}
    {{-- <group-modal></group-modal> --}}
    {{-- Modals --}}

@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function(){
            jQuery('.btn-member').click(function(){
                    debugger
                    jQuery('#txtid').click();
                    jQuery('#MemberModel').modal({
                        backdrop: 'static',
                        keyboard: false
                    })

                $('#MemberModel').on('hidden.bs.modal', function (item) {
                    debugger
                    try {
                        if(item.target.id == "MemberModel")
                        {
                            jQuery('#txtgroupmemberdispose').click();
                        }
                    } catch (Exception) {
                    }
                })

            });


            jQuery('.btn-invite').click(function(){
                debugger
                jQuery('#txtinvitememberid').click();
                jQuery('#InviteMemberModel').modal({
                    backdrop: 'static',
                    keyboard: false
                })

                $('#InviteMemberModel').on('hidden.bs.modal', function (item) {
                    debugger
                    try {
                        if(item.target.id == "InviteMemberModel")
                        {
                            jQuery('#txtdispose').click();
                        }
                    } catch (Exception) {
                    }
                })

            });

        });
    </script>
@endsection