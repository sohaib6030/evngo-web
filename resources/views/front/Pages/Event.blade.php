
@extends('front.layouts.front-layout')
@section('content')

<div id="page-contents">
      <div class="container">
        <div class="row">

          @include('front.sidebars.sidebar-1')
          
          <div class="col-md-7">

            <!-- Post Create Box
            ================================================= -->
   {{--          <div class="create-post">
              <div class="row">
                <div class="col-md-7 col-sm-7">
                  <div class="form-group">
                    <img src="http://placehold.it/300x300" alt="" class="profile-photo-md" />
                    <textarea name="texts" id="exampleTextarea" cols="30" rows="1" class="form-control" placeholder="Write what you wish"></textarea>
                  </div>
                </div>
                <div class="col-md-5 col-sm-5">
                  <div class="tools">
                    <ul class="publishing-tools list-inline">
                      <li><a href="#"><i class="ion-compose"></i></a></li>
                      <li><a href="#"><i class="ion-images"></i></a></li>
                      <li><a href="#"><i class="ion-ios-videocam"></i></a></li>
                      <li><a href="#"><i class="ion-map"></i></a></li>
                    </ul>
                    <button class="btn btn-primary pull-right">Publish</button>
                  </div>
                </div>
              </div>
            </div> --}}

            <!-- Post Create Box End-->

            <!-- Post Content
            ================================================= -->
            <div class="page-inner-header">
              <div></div>
              <div>
                  
                  @php
                    $vip = App\models\UserType::where('type','VIP')->where('user_id',auth()->user()->id)->first();
                      if($vip){
                          
                         $VIP = 1; 
                      }else{
                         $VIP = 0;
                      }
                  @endphp
                  <a @if($VIP) class="btn btn-xs btn-primary" href="#" data-toggle="modal" data-target="#EventModel" @else href="{{ route('m.subscribe') }}" @endif></a>

                
              </div>
            </div>

          

          <div id="posts-wrapper">
              <event></event>
          </div>

</div>
          <!-- Newsfeed Common Side Bar Right
          ================================================= -->
          <div class="col-md-3 static">
            <upcoming-events></upcoming-events>

            <popular-events></popular-events>

            <nearby-events></nearby-events>
          </div>

        </div>
      </div>
    </div>
  
    {{-- Modals --}}
    {{-- @include('front.Modals.AddEvent') --}}
    <event-modal></event-modal>
    {{-- Modals --}}

@endsection
