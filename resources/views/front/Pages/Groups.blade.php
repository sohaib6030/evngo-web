
@extends('front.layouts.front-layout')
@section('content')

<div id="page-contents">
      <div class="container">
        <div class="row">

          @include('front.sidebars.sidebar-1')

          <div class="col-md-9">
                <!-- Post Content
                ================================================= -->
                <div class="page-inner-header">
                    <div></div>
                    <div style="margin-bottom: 10px">

                        @php
                            $vip = App\models\UserType::where('type','VIP')->where('user_id',auth()->user()->id)->first();
                              if($vip){

                                 $VIP = 1;
                              }else{
                                 $VIP = 0;
                              }
                        @endphp
                        <a class="btn btn-xs btn-primary" @if($VIP) href="#" data-toggle="modal" data-target="#GroupModal" @else href="{{ route('m.subscribe') }}" @endif><i class="icon ion-plus" style="display: inline-block;transform: translateX(-3px);"> </i>Create Group</a>

                    </div>




                </div>

                <group-types ref="grouptypes"></group-types>

            </div>

        </div>
      </div>
    </div>

    {{-- Modals --}}
    {{-- @include('front.Modals.AddEvent') --}}
    <group-modal></group-modal>
    {{-- Modals --}}

@endsection
