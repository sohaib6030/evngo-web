
@extends('front.layouts.front-layout')
@section('content')

<div id="page-contents">
      <div class="container">
        <div class="row">

          @include('front.sidebars.sidebar-1')
          
          <div class="col-md-7">

            <!-- Post Create Box
            ================================================= -->
   {{--          <div class="create-post">
              <div class="row">
                <div class="col-md-7 col-sm-7">
                  <div class="form-group">
                    <img src="http://placehold.it/300x300" alt="" class="profile-photo-md" />
                    <textarea name="texts" id="exampleTextarea" cols="30" rows="1" class="form-control" placeholder="Write what you wish"></textarea>
                  </div>
                </div>
                <div class="col-md-5 col-sm-5">
                  <div class="tools">
                    <ul class="publishing-tools list-inline">
                      <li><a href="#"><i class="ion-compose"></i></a></li>
                      <li><a href="#"><i class="ion-images"></i></a></li>
                      <li><a href="#"><i class="ion-ios-videocam"></i></a></li>
                      <li><a href="#"><i class="ion-map"></i></a></li>
                    </ul>
                    <button class="btn btn-primary pull-right">Publish</button>
                  </div>
                </div>
              </div>
            </div> --}}

            <!-- Post Create Box End-->

            <!-- Post Content
            ================================================= -->
            <div class="page-inner-header">
              <div></div>
              <div>
                <a href="#" data-toggle="modal" data-target="#EventModel">Add an Event</a>
                
              </div>
            </div>

          
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#my-events">My Events</a></li>
            <li><a data-toggle="tab" href="#joined-events">Joined Events</a></li>
            <li><a data-toggle="tab" href="#shared-events">Shared With Me</a></li>
        </ul>
        <div class="tab-content">

            <div id="my-events" class="tab-pane fade in active">
              <events></events>
            </div>
            
        </div>
        {{--   <div id="posts-wrapper">
            <events></events>
            
          </div> --}}

</div>
          <!-- Newsfeed Common Side Bar Right
          ================================================= -->
          <div class="col-md-3 static">
            <div class="suggestions" id="sticky-sidebar">
              <h4>Upcoming Events</h4>

              <div class="follow-user sidebar-box">
                <span class="date-circle-sm pull-left">
                  <div>
                    <span class="d-day">07</span>
                     <span class="d-month">Aug</span>
                  </div>
                </span>
                <div>
                  <h5><a href="#">Diana Amber</a></h5>
                  <a href="#" class="sub-text">Location</a>
                  <h4>$ 250</h4>
                </div>
              </div>
              <div class="follow-user sidebar-box">
                <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm pull-left" />
                <div>
                  <h5><a href="#">Diana Amber</a></h5>
                  <a href="#" class="sub-text">Location</a>
                  <h4>$ 250</h4>
                </div>
              </div>
              <div class="follow-user sidebar-box">
                <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm pull-left" />
                <div>
                  <h5><a href="#">Diana Amber</a></h5>
                  <a href="#" class="sub-text">Location</a>
                  <h4>$ 250</h4>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  
    {{-- Modals --}}
    {{-- @include('front.Modals.AddEvent') --}}
    <event-modal :for-auth="true"></event-modal>
    {{-- Modals --}}

@endsection
