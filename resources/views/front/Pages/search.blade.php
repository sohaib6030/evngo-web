
@extends('front.layouts.front-layout')
@section('content')

<div id="page-contents">
      <div class="container">
        <div class="row">

          @include('front.sidebars.sidebar-1')
          
          <div class="col-md-10">

            <!-- Post Create Box
            ================================================= -->
   {{--          <div class="create-post">
              <div class="row">
                <div class="col-md-7 col-sm-7">
                  <div class="form-group">
                    <img src="http://placehold.it/300x300" alt="" class="profile-photo-md" />
                    <textarea name="texts" id="exampleTextarea" cols="30" rows="1" class="form-control" placeholder="Write what you wish"></textarea>
                  </div>
                </div>
                <div class="col-md-5 col-sm-5">
                  <div class="tools">
                    <ul class="publishing-tools list-inline">
                      <li><a href="#"><i class="ion-compose"></i></a></li>
                      <li><a href="#"><i class="ion-images"></i></a></li>
                      <li><a href="#"><i class="ion-ios-videocam"></i></a></li>
                      <li><a href="#"><i class="ion-map"></i></a></li>
                    </ul>
                    <button class="btn btn-primary pull-right">Publish</button>
                  </div>
                </div>
              </div>
            </div> --}}

            <!-- Post Create Box End-->

            <!-- Post Content
            ================================================= -->
            <div class="page-inner-header">
              <div></div>
              
            </div>
{{-- 
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#my-events">My Events</a></li>
              <li><a data-toggle="tab" href="#joined-events">Joined Events</a></li>
              <li><a data-toggle="tab" href="#shared-events">Shared With Me</a></li>
          </ul> --}}
          {{-- <div class="tab-content" style="padding-top: 15px;">

              <div id="my-events" class="tab-pane fade in active">
                <events></events>
              </div>

              <div id="joined-events" class="tab-pane fade">
             
              </div>
              
          </div> --}}          

          <div id="posts-wrapper">

              <search-result></search-result>
          </div>

</div>
          <!-- Newsfeed Common Side Bar Right
          ================================================= -->
            <!--     <div class="col-md-3 static">
                 <upcoming-events></upcoming-events>

                 <popular-events></popular-events>

                 <nearby-events></nearby-events>
               </div> -->

             </div>
           </div>
         </div>


@endsection
