
@extends('front.layouts.front-layout')
@section('content')
@php
  
@endphp
  
<div id="page-contents">
      <div class="container">
        <div class="row">

          @include('front.sidebars.sidebar-1')
          
          <div class="col-md-10">

            <!-- Post Create Box
            ================================================= -->
   {{--          <div class="create-post">
              <div class="row">
                <div class="col-md-7 col-sm-7">
                  <div class="form-group">
                    <img src="http://placehold.it/300x300" alt="" class="profile-photo-md" />
                    <textarea name="texts" id="exampleTextarea" cols="30" rows="1" class="form-control" placeholder="Write what you wish"></textarea>
                  </div>
                </div>
                <div class="col-md-5 col-sm-5">
                  <div class="tools">
                    <ul class="publishing-tools list-inline">
                      <li><a href="#"><i class="ion-compose"></i></a></li>
                      <li><a href="#"><i class="ion-images"></i></a></li>
                      <li><a href="#"><i class="ion-ios-videocam"></i></a></li>
                      <li><a href="#"><i class="ion-map"></i></a></li>
                    </ul>
                    <button class="btn btn-primary pull-right">Publish</button>
                  </div>
                </div>
              </div>
            </div> --}}

            <!-- Post Create Box End-->

            <!-- Post Content
            ================================================= -->

           <form action="{{ route('setting.profile.update')  }}" method="post" class="form-inline profile-form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="form-group col-xs-6">
                        <label for="firstname" class="sr-only">First Name</label>
                        <input id="firstname" class="form-control input-group-lg" value="{{ $user->fname }}" type="text" name="fname" title="Enter first name" placeholder="First name" required="true"/>
                        @if($errors->has('fname'))
                            <div class="error">{{ $errors->first('fname') }}</div>
                        @endif
                      </div>
                      <div class="form-group col-xs-6">
                        <label for="lastname" class="sr-only">Last Name</label>
                        <input id="lastname" class="form-control input-group-lg" type="text" value="{{ $user->lname }}" name="lname" title="Enter last name" placeholder="Last name" required="true"/>
                        @if($errors->has('lname'))
                            <div class="error">{{ $errors->first('lname') }}</div>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-xs-12">
                        <label for="email" class="sr-only">Email</label>
                        <input id="email" class="form-control input-group-lg" type="email" value="{{ $user->email }}" name="email" title="Enter Email" placeholder="Your Email"/>
                        
                      </div>
                    </div>
{{-- 
                    <div class="row">
                      <div class="form-group col-xs-12">
                        <label for="password" class="sr-only">Password</label>
                        <input id="password" class="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="Password"/>
                        @if($errors->has('password'))
                            <div class="error">{{ $errors->first('password') }}</div>
                        @endif
                      </div>
                      
                    </div>

                    <div class="row">
                      <div class="form-group col-xs-12">
                        <label for="password" class="sr-only">Confirm Password</label>
                        <input id="password" class="form-control input-group-lg" type="password" name="password_confirmation" title="Enter password" placeholder="Confirm Password"/>
                        @if($errors->has('password_confirmation'))
                            <div class="error">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                      </div>
                    </div> --}}
                    <div class="row">
                      <div class="col-xs-12">
                        <p class="birth"><strong>Date of Birth</strong></p>
                      </div>
                      <div class="form-group col-sm-3 col-xs-6">
                        <label for="day" class="sr-only"></label>
                        <select class="form-control" id="day" name="day" required="true">
                          <option value="Day" disabled selected>Day</option>
                          @for($i = 1;$i <= 31;$i++)
                              <option value="{{ $i }}" @if($i == \Carbon\Carbon::parse($user->dob)->format('d')) selected @endif>{{ $i }}</option>
                          @endfor
                        
                        </select>
                        @if($errors->has('day'))
                            <div class="error">{{ $errors->first('day') }}</div>
                        @endif
                      </div>
                      <div class="form-group col-sm-3 col-xs-6">
                        <label for="month" class="sr-only"></label>
                        <select class="form-control" id="month" name="month" required="true">
                          <option value="month" disabled selected>Month</option>
                          @php
                            $months = \App\Http\Helpers\Helper::months();
                          @endphp
                          @foreach($months as $sh_month => $month)
                            <option value="{{$month}}" @if($month == \Carbon\Carbon::parse($user->dob)->format('F')) selected @endif>{{ $month }}</option>
                          @endforeach
                        </select>
                        @if($errors->has('month'))
                              <div class="error">{{ $errors->first('month') }}</div>
                          @endif
                      </div>
                      <div class="form-group col-sm-6 col-xs-12">
                        <label for="year" class="sr-only"></label>
                        <select class="form-control" id="year" name="year" required="true">
                          <option value="" disabled selected>Year</option>
                          @foreach(\App\Http\Helpers\Helper::get_last_years(1900,'desc') as $year)
                            <option value="{{ $year }}" @if($year == \Carbon\Carbon::parse($user->dob)->format('Y')) selected @endif>{{ $year }}</option>
                          @endforeach
                        </select>
                        @if($errors->has('year'))
                            <div class="error">{{ $errors->first('year') }}</div>
                        @endif
                      </div>
                    </div>
                    <div class="form-group gender">
                      
                      <label class="radio-inline">
                        <input type="radio" name="gender" value="male" @if($user->gender == 'male') checked @endif>Male
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="gender" value="female" @if($user->gender == 'female') checked @endif>Female
                      </label>
                      @if($errors->has('gender'))
                            <div class="error">{{ $errors->first('gender') }}</div>
                      @endif
                    </div>
                    <div class="row">
                     
                      <div class="form-group col-xs-6">
                        <label for="country">Country</label>
                        <select class="form-control" id="country" name="country" required="true">
                          @php
                            $countries = \App\Http\Helpers\Helper::countries();
                          @endphp
                          <option value="" disabled selected>Country</option>
                          @foreach($countries as $code => $country)
                            <option value="{{$country}}" @if($country == $user->country) selected @endif>{{ $country }}</option>
                          @endforeach
                          
                        </select>
                        @if($errors->has('country'))
                              <div class="error">{{ $errors->first('country') }}</div>
                        @endif
                      </div>
                       <div class="form-group col-xs-6">
                        <label for="city" name="city">City</label>
                        <input id="city" class="form-control input-group-lg reg_name" value="{{ $user->city }}" type="text" name="city" title="Enter city" placeholder="Your city"/ required="true">
                        @if($errors->has('city'))
                            <div class="error">{{ $errors->first('city') }}</div>
                        @endif
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <label>Profile Pic</label>
                          <input type="file" id="file" name="avatar"/>
                        </div>
                      </div>
                    </div>

                    <div class="text-right">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form><!--Register Now Form Ends-->

</div>
          <!-- Newsfeed Common Side Bar Right
          ================================================= -->
            <!--   <div class="col-md-3 static">
                 <upcoming-events></upcoming-events>
               </div> -->

             </div>
           </div>
         </div>
@endsection
