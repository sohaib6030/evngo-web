<!-- Newsfeed Common Side Bar Left
          ================================================= -->
          <div class="col-md-2 static">
            {{-- <div class="profile-card">
              <img src="http://placehold.it/300x300" alt="user" class="profile-photo" />
              <h5><a href="#" class="text-white">Sarah Cruiz</a></h5>
              <a href="#" class="text-white"><i class="ion ion-android-person-add"></i> 1,299 followers</a>
            </div> --}}<!--profile card ends-->
            <div style="display: flex;justify-content: space-between;align-items: center;">
              <span style="color: #000000;
    font-size: 18px;">Menu</span>
             <div id="menu-btn-wrapper"><i id="menu-btn" class="fa fa-bars" style="font-size: 20px;cursor: pointer;"></i></div>
            </div>
            <ul class="nav-news-feed" id="side-menu">
              
              {{-- <li><div><a href="{{ url('/') }}"><i class="icon ion-ios-paper"></i> Home</a></div></li> --}}
              <li><div><a href="{{ route('events') }}"><i class="icon ion-android-list"></i>Events</a></div></li>
            <!-- <li><div><a href="{{ route('events',['m' => 'evnts']) }}"><i class="icon ion-android-list"></i> My Events</a></div></li>-->
              <li><div><a href="{{ route('groups') }}"><i class="icon ion-ios-people"></i> Groups</a></div></li>
              <!--<li><div><a href="{{ route('groups',['m' => 'groups']) }}"><i class="icon ion-ios-people"></i> My Groups</a></div></li>-->
              <li><div><a href="{{ route('requests') }}"><i class="icon ion-android-notifications"></i>Notifications <requests-counter></requests-counter></a></div></li>


              
              {{-- <li><div><a href="#"><i class="icon ion-images"></i> Services</a></div></li> --}}
              {{-- <li><div><a href="#"><i class="icon ion-ios-videocam"></i> Marketing</a></div></li> --}}
            </ul><!--news-feed links ends-->
            {{-- <div id="chat-block">
              <div class="title">Chat online</div>
              <ul class="online-users list-inline">
                <li><a href="newsfeed-messages.html" title="Linda Lohan"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
                <li><a href="newsfeed-messages.html" title="Sophia Lee"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
                <li><a href="newsfeed-messages.html" title="John Doe"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
                <li><a href="newsfeed-messages.html" title="Alexis Clark"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
                <li><a href="newsfeed-messages.html" title="James Carter"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
                <li><a href="newsfeed-messages.html" title="Robert Cook"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
                <li><a href="newsfeed-messages.html" title="Richard Bell"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
                <li><a href="newsfeed-messages.html" title="Anna Young"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
                <li><a href="newsfeed-messages.html" title="Julia Cox"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
              </ul>
            </div> --}}
            <!--chat block ends-->
          </div>

          