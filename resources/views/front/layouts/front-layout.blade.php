<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Evengo') }}</title>

	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{ config('app.name', 'Evengo') }}" />
	<meta name="keywords" content="Social Network, Social Media, Make Friends, Newsfeed, Profile Page" />
	<meta name="robots" content="index, follow" />



    <!-- Stylesheets
    ================================================= -->
    <link rel="stylesheet" href="{{ asset('public/front-end/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/front-end/css/snackbarlight.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/front-end/css/style.css') }}?ver=1.0.2" />
    <link rel="stylesheet" href="{{ asset('public/front-end/css/ionicons.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/front-end/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/front-end/css/jquery-ui.min.css') }}" />
    
    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">
    <link href="{{ asset('public/front-end/fonts/MYRIADPRO-REGULAR.woff') }}" rel="stylesheet">
    
    <!--Favicon-->
   <link rel="shortcut icon" type="image/png" href="{{ asset('public/front-end/images/evngo-logo.png') }}"/>
   <link rel="manifest" href="{{ asset('/manifest.json') }}" />
   <meta name="theme-color" content="#5a2591">
	</head>
	<body>
      <input type="hidden" id="token">
    {{-- <div id="evengo-app"> --}}
      <div id="app">
      @include('front.common.header')
      @yield('content')
      </div>
     
        
      
    {{-- </div> --}}
    

       <!-- Footer
    ================================================= -->
    <footer id="footer">
      {{-- <div class="container">
        <div class="row">
          <div class="footer-wrapper">
            <div class="col-md-3 col-sm-3">
              <a href=""><img src="images/logo-black.png" alt="" class="footer-logo" /></a>
              <ul class="list-inline social-icons">
                <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
                <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                <li><a href="#"><i class="icon ion-social-googleplus"></i></a></li>
                <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
                <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
              </ul>
            </div>
            <div class="col-md-2 col-sm-2">
              <h5>For individuals</h5>
              <ul class="footer-links">
                <li><a href="">Signup</a></li>
                <li><a href="">login</a></li>
                <li><a href="">Explore</a></li>
                <li><a href="">Finder app</a></li>
                <li><a href="">Features</a></li>
                <li><a href="">Language settings</a></li>
              </ul>
            </div>
            <div class="col-md-2 col-sm-2">
              <h5>For businesses</h5>
              <ul class="footer-links">
                <li><a href="">Business signup</a></li>
                <li><a href="">Business login</a></li>
                <li><a href="">Benefits</a></li>
                <li><a href="">Resources</a></li>
                <li><a href="">Advertise</a></li>
                <li><a href="">Setup</a></li>
              </ul>
            </div>
            <div class="col-md-2 col-sm-2">
              <h5>About</h5>
              <ul class="footer-links">
                <li><a href="">About us</a></li>
                <li><a href="">Contact us</a></li>
                <li><a href="">Privacy Policy</a></li>
                <li><a href="">Terms</a></li>
                <li><a href="">Help</a></li>
              </ul>
            </div>
            <div class="col-md-3 col-sm-3">
              <h5>Contact Us</h5>
              <ul class="contact">
                <li><i class="icon ion-ios-telephone-outline"></i>+1 (234) 222 0754</li>
                <li><i class="icon ion-ios-email-outline"></i>info@evngo.com.au</li>
                <li><i class="icon ion-ios-location-outline"></i>Australia</li>
              </ul>
            </div>
          </div>
        </div>
      </div> --}}
      <div class="copyright">
        <p>Evengo Pte Ltd© 2019. All rights reserved</p>
      </div>
    </footer>
    
    <!--preloader-->
    <div id="spinner-wrapper">
      <div class="spinner"></div>
    </div>
      <!-- Scripts
    ================================================= -->
    <script src="{{ asset('public/front-end/js/jquery-3.1.1.min.js') }}"></script>
  {{-- <script src="{{ asset('/reg-sw.js') }}"></script> --}}
    <script src="{{ asset('public/front-end/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/front-end/js/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('public/front-end/js/jquery.incremental-counter.js') }}"></script>
      <script src="{{ asset('public/front-end/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/front-end/js/script.js') }}"></script>
    <script>
      window.base_path = '<?php echo url('/public'); ?>';

      window.base_url = '<?php echo url('/'); ?>';
      window.auth_user = {!! auth()->user() !!};
      @php 
        $vip = App\models\UserType::where('type','VIP')->where('user_id',auth()->user()->id)->first();
        if($vip){
            
           $VIP = 1; 
        }else{
           $VIP = 0;
        }

        $promoter = App\models\UserType::where('type','Promoter')->where('user_id',auth()->user()->id)->first();
        if($promoter){
            
           $promoter = 1; 
        }else{
           $promoter = 0;
        }

        $service_provider = App\models\UserType::where('type','ServiceProvider')->where('user_id',auth()->user()->id)->first();
        if($service_provider){
            
           $service_provider = 1; 
        }else{
           $service_provider = 0;
        }
      @endphp
      window.auth_user_vip = {{ $VIP }};
      window.service_provider = {{ $service_provider }};
      window.promoter = {{ $promoter }};
      window.auth_user_type = {!! auth()->user()->user_type !!};

      @php
        if(auth()->user()->avatar=='placeholder.png'){
            $Avatarurl=URL::to('/public').Storage::disk('local')->url('public/users/'.auth()->user()->avatar);
        }else{
            $Avatarurl=URL::to('/public').Storage::disk('local')->url('public/users/'.auth()->user()->id.'/'.auth()->user()->avatar);
        }
      @endphp
      window.user_avatar = '{{ $Avatarurl }}';
     
      window.web_token = '{{ session('web_token') }}';
      
      
     jQuery('#profile-icon-img').attr('src',window.user_avatar);

    </script>
     <script async src="{{ asset('public/js/instascan.min.js') }}"></script>
    <script src="{{ asset('public/js/evngo-app.js') }}?ver=1.0.1"></script>
    <script src="{{ asset('public/front-end/js/snackbarlight.min.js') }}"></script>
    <script>
      @if(\Session::has('success'))
        new Snackbar('{{ \Session::get('success') }}',{timeout: 3000});
      @endif

    </script>
    @yield('scripts')

      <!-- The core Firebase JS SDK is always required and must be listed first -->
      <script src="https://www.gstatic.com/firebasejs/7.2.3/firebase-app.js"></script>
      <script src="https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js"></script>
      

      <!-- TODO: Add SDKs for Firebase products that you want to use
           https://firebase.google.com/docs/web/setup#available-libraries -->

      <script>


        jQuery(document).ready(function(){
          jQuery('#menu-btn').click(function(){
            jQuery('#side-menu').slideToggle();
          })
        });
        // Your web app's Firebase configuration
        var firebaseConfig = {
          apiKey: "AIzaSyCd_q79WbWEjim_9WrYsMmvw0Okwl-T6HA",
          authDomain: "evngo-62b0f.firebaseapp.com",
          databaseURL: "https://evngo-62b0f.firebaseio.com",
          projectId: "evngo-62b0f",
          storageBucket: "evngo-62b0f.appspot.com",
          messagingSenderId: "861935465954",
          appId: "1:861935465954:web:af642513bb9637426735bb"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);


      // Retrieve Firebase Messaging object.
      const messaging = firebase.messaging();

        messaging.usePublicVapidKey("BKnony19LvUp492Yr34LF2Cgbm3-OTIiYboOAmWWTkCP_3XLIuqmo9mxQhKUOWF3cFcJv6rM5izJ50dq1IOYrRU");

        Notification.requestPermission().then((permission) => {
          if (permission === 'granted') {
            console.log('Notification permission granted.');
            if(isTokenSentToServer()){
              console.log('Token Already Sent');
            }else{
              getRegisterToken();
            }
            getRegisterToken();
            // TODO(developer): Retrieve an Instance ID token for use with FCM.
            // ...
          } else {
            console.log('Unable to get permission to notify.');
          }
        });

        
        // Retrieve an instance of Firebase Messaging so that it can handle background
        // messages.


        function getRegisterToken(){
          messaging.getToken().then((currentToken) => {
            if (currentToken) {
              console.log(currentToken);
              sendTokenToServer(currentToken);
              // updateUIForPushEnabled(currentToken);
            } else {
              // Show permission request.
              console.log('No Instance ID token available. Request permission to generate one.');
              // Show permission UI.
              // updateUIForPushPermissionRequired();
              setTokenSentToServer(false);
            }
          }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            showToken('Error retrieving Instance ID token. ', err);
            setTokenSentToServer(false);
          });

          // Retrieve an instance of Firebase Messaging so that it can handle background
          // messages.

        }


        function sendTokenToServer(currentToken) {
          if (!isTokenSentToServer()) {

            console.log('Sending token to server...');
            
            axios.post('public/firebase/token', {
              firebase_token: currentToken,
            })
            .then(function (response) {
              console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            });
            setTokenSentToServer(true);

          } else {
            console.log('Token already sent to server so won\'t send it again ' +
                'unless it changes');
          }
        }

         function showToken(currentToken) {
          // Show token in console and UI.
          const tokenElement = document.querySelector('#token');
          tokenElement.textContent = currentToken;
        }
        
        function isTokenSentToServer() {
          return window.localStorage.getItem('sentToServer') === '1';
        }
        function setTokenSentToServer(sent) {
          window.localStorage.setItem('sentToServer', sent ? '1' : '0');
        }

        messaging.onMessage((payload) => {
          console.log('Message received. ', payload);
          if(payload){
            new Snackbar(payload.data.body,{timeout: 3000});
          }
        });

      </script>
  </body>
</html>
