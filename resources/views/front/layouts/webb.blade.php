<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Evengo') }}</title>
  <link rel="icon" href="" type="image/png">

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="This is social network html5 template available in themeforest......" />
	<meta name="keywords" content="Social Network, Social Media, Make Friends, Newsfeed, Profile Page" />
	<meta name="robots" content="index, follow" />
	<title>Friend Finder | A Complete Social Network Template</title>

    <!-- Stylesheets
    ================================================= -->
		<link rel="stylesheet" href="{{ asset('public/front-end/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('public/front-end/css/style.css') }}" />
		<link rel="stylesheet" href="{{ asset('public/front-end/css/ionicons.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/front-end/css/font-awesome.min.css') }}" />
    
    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">
    <link href="{{ asset('public/front-end/fonts/MYRIADPRO-REGULAR.woff') }}" rel="stylesheet">
    
    <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('public/front-end/images/fav.png') }}"/>
	</head>
	<body>

    {{-- <div id="evengo-app"> --}}
      <div id="app">
      @include('front.common.header')
      @yield('content')
      </div>
     
        
      
    {{-- </div> --}}
    

       <!-- Footer
    ================================================= -->
    <footer id="footer">
      <div class="container">
        <div class="row">
          <div class="footer-wrapper">
            <div class="col-md-3 col-sm-3">
              <a href=""><img src="images/logo-black.png" alt="" class="footer-logo" /></a>
              <ul class="list-inline social-icons">
                <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
                <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                <li><a href="#"><i class="icon ion-social-googleplus"></i></a></li>
                <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
                <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
              </ul>
            </div>
            <div class="col-md-2 col-sm-2">
              <h5>For individuals</h5>
              <ul class="footer-links">
                <li><a href="">Signup</a></li>
                <li><a href="">login</a></li>
                <li><a href="">Explore</a></li>
                <li><a href="">Finder app</a></li>
                <li><a href="">Features</a></li>
                <li><a href="">Language settings</a></li>
              </ul>
            </div>
            <div class="col-md-2 col-sm-2">
              <h5>For businesses</h5>
              <ul class="footer-links">
                <li><a href="">Business signup</a></li>
                <li><a href="">Business login</a></li>
                <li><a href="">Benefits</a></li>
                <li><a href="">Resources</a></li>
                <li><a href="">Advertise</a></li>
                <li><a href="">Setup</a></li>
              </ul>
            </div>
            <div class="col-md-2 col-sm-2">
              <h5>About</h5>
              <ul class="footer-links">
                <li><a href="">About us</a></li>
                <li><a href="">Contact us</a></li>
                <li><a href="">Privacy Policy</a></li>
                <li><a href="">Terms</a></li>
                <li><a href="">Help</a></li>
              </ul>
            </div>
            <div class="col-md-3 col-sm-3">
              <h5>Contact Us</h5>
              <ul class="contact">
                <li><i class="icon ion-ios-telephone-outline"></i>+1 (234) 222 0754</li>
                <li><i class="icon ion-ios-email-outline"></i>info@evngo.com.au</li>
                <li><i class="icon ion-ios-location-outline"></i>Australia</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <p>Evengo Pte Ltd© 2019. All rights reserved</p>
      </div>
    </footer>
    
    <!--preloader-->
    <div id="spinner-wrapper">
      <div class="spinner"></div>
    </div>
      <!-- Scripts
    ================================================= -->
    <script src="{{ asset('public/front-end/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('public/front-end/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/front-end/js/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('public/front-end/js/jquery.incremental-counter.js') }}"></script>
    <script src="{{ asset('public/front-end/js/script.js') }}"></script>
    <script>
      window.base_path = '<?php echo url('/public'); ?>';
      window.base_url = '<?php echo url('/'); ?>';
      window.auth_user = {!! auth()->user() !!};
      window.user_avatar = '{{ URL::to('/public').Storage::disk('local')->url('public/users/'.auth()->user()->avatar) }}';
      // window.base_path = 'https://dev.evngo.com.au/public';
      window.web_token = '{{ session('web_token') }}';
      // window.web_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjMzZTk5ZjZhMTNkMjlkMjI4MTM5MjE3N2I2ZTZmNDEwZWQ0YzMzOTc0Y2Q4Y2VmNzhhNDlkYjk3NDhkNmY3OTliYzc3YzBkYTY0NDI5YmI4In0.eyJhdWQiOiI1IiwianRpIjoiMzNlOTlmNmExM2QyOWQyMjgxMzkyMTc3YjZlNmY0MTBlZDRjMzM5NzRjZDhjZWY3OGE0OWRiOTc0OGQ2Zjc5OWJjNzdjMGRhNjQ0MjliYjgiLCJpYXQiOjE1NzAyNDg3MTQsIm5iZiI6MTU3MDI0ODcxNCwiZXhwIjoxNjAxODcxMTE0LCJzdWIiOiIxMjMiLCJzY29wZXMiOltdfQ.fRK6x-yWsmVIcbbjA9E_EFrbg4ZNlD4gZCyVrFM974XsxgSvwQjBQzBiMcyxBQSmkRWb4z5ZuHDPrO5tKrRBtJximFNYpc4x2HHFpVip-lR5Y-d-_b5Fh9c65TFHCdd9f40z9Qp_hCeY7V262r3k56Zvxx5EOIcnsXh31l7WxDFm-a6rHkPQc8EML-HcpZY3KJfR5KjzpF1OQnca7eGDFxPvMBnlFMnSRIyfnOQWE-97opyC8i4azJRfs2T8TQarfnyCMkUIF_gVHdzz90vmOa5FIMWsfxlH5jhYmANv1CQPaoy-ixnroRujkvMiNRj_N8WyNUVyroD8gt-2Xu2L-9EnJvqNF6Y9RfOqDS_m0CfGRuZchrpGCaV5cOLA97UXSMG5zQRfrCZfw3hlqvYVLO9DqOhUT6mdnfQajjae5JVrWcbK11l5M82VWhztoQ9I7uRa7dgO9ubjP2_yVWbB89v75D1cu1JMa0Cq8S-fhLzDC9CSRYIykSQX5dR0jsRqLzsZOXkmAleGaIb1TvRqJj1tcfH_gAOcoBbumzMIRgd5QBpYZE7rp--NITK1M7NRZdK4szSav0qUaEViPGqYRyeHK3j6RC9065dimgVykJr2_V_jDU-YGCMZPv4m20gp8NladdqYD_eyUZe_Zgwx-dOLO0X5GuVB14fGju9Z3GA';
    </script>
    <script src="{{ asset('public/front-end/js/evengo-app.js') }}"></script>
    
  </body>
</html>
