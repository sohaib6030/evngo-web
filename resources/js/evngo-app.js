
import Vue from 'vue/dist/vue.esm.js';
require('./bootstrap');
import VueRouter from 'vue-router'
Vue.use(VueRouter)



import { Form, HasError, AlertError } from 'vform'
import PictureInput from 'vue-picture-input'
import objectToFormData from 'object-to-formdata';
window.objectToFormData = objectToFormData;

var _ = require('lodash');

// Import component
import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
import InfiniteLoading from 'vue-infinite-loading';
// import DatePick from 'vue-date-pick';
import 'vue-date-pick/dist/vueDatePick.css';
import VueTimepicker from 'vue2-timepicker'
// Vue.component('DatePick', DatePick);
import DatePicker from 'vue2-datepicker'
import dayjs from 'dayjs'
import vSelect from 'vue-select'

Vue.component('loading', Loading);
Vue.component('date-picker', DatePicker);
Vue.component('loading', Loading);
Vue.component('infinite-loading', InfiniteLoading);
Vue.component('PictureInput', PictureInput);
Vue.component('VueTimepicker', VueTimepicker);
Vue.component("vue-country-select", require("vue-country-select"));
Vue.component('v-select', vSelect)
import 'vue-select/dist/vue-select.css';
window.Form = Form;
window.dayjs = dayjs;
window.request_running = {};

import { VueMaskDirective } from 'v-mask'
Vue.directive('mask', VueMaskDirective);

Vue.mixin({
methods: {

  Liked(like){
    if(like == 'No'){
    	return false;
    }else{
    	return 'liked';
    }
  },

  subscribed_status(status){
    if(status == 'VIP'){
      if(window.auth_user_vip == 1){
        return 'Subscribed';
      }
    }else if(status == 'ServiceProvider'){
      if(window.service_provider == 1){
        return 'Subscribed';
      }
    }else if(status == 'Promoter'){
      if(window.promoter == 1){
        return 'Subscribed';
      }
    }

    return 'Subscribe';
  },

  formatDay(date){
    if(date){
      var a =  dayjs(date).format('DD');
      if(a != 'Invalid Date'){
        return a;
      }else{
        return 'N'
      }
    }
    
  },
  eventJoinStatus(status){
    if(status == "No"){
      return 'Join';
    }
    if(status == "Yes"){
      return 'Joined';
    }
  },

  formatMonth(date){
    if(date){
      var a =  dayjs(date).format('MMM');
      if(a != 'Invalid Date'){
        return a;
      }else{
        return 'A'
      }
    }

  },

  groupStatus(status){
    if(status == "Not Joined"){
      return 'Join';
    }else if(status == "Joined"){
      return 'Joined';
    }else if(status == "Pending"){
      return "Pending";
    }
  },

  formatTime(time){
   return dayjs(time).format('HH:mm');
  },

  LikedText(like){
    if(like == 'No'){
    	return 'Like';
    }else{
    	return 'Liked';
    }
  },

  checkVIP(vip){
    if(vip == 'Yess'){
      return true;
    }else{
      return false;
    }
  },

  LikeDislikePost(id,index){

            if(this.data[index].like == 'Yes'){
              var Type = 'UnLike';
            }else{
              var Type = 'Like';
            }


            if(window.request_running[index] == true){
              return null;
            }
              
            window.request_running[index] = true;
            console.log('clicked');
            axios.post(window.base_path+'/api/eventLikeDislike', {
              event_id:id,
              type: Type
            } )
              .then( (response) => {
                
                if(response.data.message == 'Like'){
                  this.data[index].like = 'Yes';
                  this.data[index].totalLike++;
                }else if(response.data.message == 'UnLike'){
                  this.data[index].like = 'No';
                  this.data[index].totalLike--;
                }
                window.request_running[index] = false;
              })
              .catch(function (error) {
                // handle error
                console.log(error);
              })
              .finally(function () {
                // always executed
              });
        }

}
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
Vue.component('points',require('./components/Points.vue').default);
Vue.component('scan-qr',require('./components/ScanQR.vue').default);
Vue.component('event-types',require('./components/EventTypes.vue').default);
Vue.component('group-types',require('./components/GroupTypes.vue').default);
Vue.component('request-types',require('./components/RequestTypes.vue').default);
Vue.component('search-result',require('./components/SearchResult.vue').default);
Vue.component('shared-with-me',require('./components/sharedWithMe.vue').default);
Vue.component('event-modal',require('./components/AddEvent.vue').default);
Vue.component('requests-counter',require('./components/countRequests.vue').default);
Vue.component('editevent-modal',require('./components/EditEvent.vue').default);
Vue.component('editgroup-modal',require('./components/EditGroup.vue').default);
Vue.component('groupmember-modal',require('./components/GroupMembers.vue').default);
Vue.component('invitemember-modal',require('./components/InviteMembers.vue').default);
Vue.component('group-modal',require('./components/AddGroup.vue').default);
Vue.component('service-modal',require('./components/AddService.vue').default);
Vue.component('subscribe-modal',require('./components/subscribeModel.vue').default);
Vue.component('share-event-modal',require('./components/shareEvent.vue').default);
Vue.component('events',require('./components/Events.vue').default);
Vue.component('event',require('./components/Event.vue').default);
Vue.component('groups',require('./components/Groups.vue').default);
Vue.component('user-requests',require('./components/Requests.vue').default);
Vue.component('invite-members-group',require('./components/InviteMembersGroup.vue').default);
Vue.component('attendance-modal',require('./components/Attendance-modal.vue').default);
Vue.component('buyticket-modal',require('./components/BuyEventTicket.vue').default);
Vue.component('revenuedetail-modal',require('./components/RevenueDetail.vue').default);
Vue.component('revenueticketdetail-modal',require('./components/RevenueTicketDetail.vue').default);


Vue.component('upcoming-events',require('./components/Sidebar/upcomingEvents.vue').default);
Vue.component('nearby-events',require('./components/Sidebar/nearbyEvents.vue').default);
Vue.component('popular-events',require('./components/Sidebar/popularEvents.vue').default);

const routes = [
  { path: '/group/view/:id/members', component: require('./components/members.vue').default, },
  { path: '/group/view/:id/', component: require('./components/Events.vue').default, },
  { path: '/group/view/:id/requests', component: require('./components/MembersRequest.vue').default, },

  { path: '/event/:id', component: require('./components/Event.vue').default, },
  // { path: '/user/requests', component: require('./components/Requests.vue').default, },
];

const router = new VueRouter({
  routes, // short for `routes: routes`
  mode:'history'
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.config.devtools = true;

const app = new Vue({
    el: '#app',
    router
});


