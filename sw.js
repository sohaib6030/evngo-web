// const staticCacheName = 'site-static';
// const assets = [
// 	// '/evngo.com.au/',
// 	'/evngo.com.au/public/front-end/css/bootstrap.min.css',
// 	'/evngo.com.au/public/front-end/css/snackbarlight.min.css',
// 	'/evngo.com.au/public/front-end/css/style.css',
// 	'/evngo.com.au/public/front-end/css/ionicons.min.css',
// 	'/evngo.com.au/public/front-end/css/font-awesome.min.css',
// 	'/evngo.com.au/public/front-end/fonts/MYRIADPRO-REGULAR.woff',
// 	'https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i'
// ];
// self.addEventListener('install', evg => {
// 	console.log('Service worker has been installed.');
// 	evg.waitUntil(
// 		caches.open(staticCacheName).then(cache => {
// 		console.log('caching assets');
// 		cache.addAll(assets);
// 	}));
// });

// // fetch Event
// self.addEventListener('fetch', evt => {
// 	evt.respondWith(
// 		caches.match(evt.request)).then(cacheRes => {
// 		// return cacheRes || fetch(evt.request);
// 		return cacheRes;
// 	});
// });


importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  'messagingSenderId': '861935465954'
});
